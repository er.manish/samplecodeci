<?php
	if ( ! defined ('BASEPATH')) {
    exit ('No direct script access allowed');
}
class Projects extends MX_Controller
{
    public function __construct ()
    {	
		parent::__construct ();
        $this->load->model ('Projects_model');
		$this->load->model ('dashboard/dashboard_model');
		$this->load->model ('post_project/Post_project_model');
        $this->load->library ('form_validation');
		$this->load->helper ('url');	
    }
	  
	/*
	 * @sid method used in assets/js/dashboard.js for updateing open bidding and expired project listing
	*/
	public function ajax_update_user_open_bidding_and_expired_project_listing() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		
		##################### fetch the expired projects from database and show on dashboard############
		$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('fixed_budget_projects_expired op');
		$this->db->where('op.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('user_dashboard_expired_projects_listing_limit'));
		$open_bidding_project_result = $this->db->get();
		$expired_project_data = $open_bidding_project_result->result_array();
		if(!empty($expired_project_data)){
			foreach($expired_project_data as $project_key=>$project_value){
				$expired_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'expired');
			}
		}
		$data['project_status'] = 'expired';
		$data["expired_project_data"] = $expired_project_data;
		$data['expired_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_expired')->count_all_results();
		$res['expired_project_data'] = $this->load->view('ajax_user_expired_project_list', $data, true);
		##################### fetch the open bidding projects from database and show on dashboard ############
		$open_bidding_project_data =array();
		$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
		$this->db->from('projects_open_bidding op');
		$this->db->where('op.project_owner_id',$user[0]->user_id);
		$this->db->where('op.project_expiration_date >= NOW()');
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('user_dashboard_open_bidding_projects_listing_limit'));
		$open_bidding_project_result = $this->db->get();
		
		$data["open_bidding_project_data"] = $open_bidding_project_data;
		$data['opeb_bidding_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
		$res['open_bidding_project_data'] = $this->load->view('ajax_user_open_bidding_project_list', $data, true);
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/*
	 * @sid method used in assets/js/dashboard.js for updateing draft and awaiting moderation view when user publish draft project
	*/
	public function ajax_update_user_draft_and_awaiting_moderation_project_view_dashboard() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		##################### fetch the draft projects from database and show on dashboard############
		$draft_project_data =array();
		$this->db->select('pd.project_id,pd.project_title,pd.project_description,pd.project_type,pd.min_budget,pd.max_budget,pd.confidential_dropdown_option_selected,pd.not_sure_dropdown_option_selected,pd.featured,pd.urgent,pd.sealed,pd.hidden,pd.project_save_date,pd.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_draft pd');
		$this->db->where('pd.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = pd.county_id', 'left');
		$this->db->join('localities', 'localities.id = pd.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = pd.postal_code_id', 'left');
		$this->db->order_by('pd.id','desc');
		$this->db->limit($this->config->item('user_dashboard_draft_projects_listing_limit'));
		$draft_project_result = $this->db->get();
		$draft_project_data = $draft_project_result->result_array();
		
		$data["draft_project_data"] = $draft_project_data;
		$data['draft_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_draft')->count_all_results();
		$res['draft_project_data'] = $this->load->view('ajax_user_draft_project_list', $data, true);
		##################### fetch the awaiting moderation projects from database and show on dashboard############
		$awaiting_moderation_project_data =array();
		$this->db->select('am.project_id,am.project_title,am.project_description,am.project_type,am.min_budget,am.max_budget,am.confidential_dropdown_option_selected,am.not_sure_dropdown_option_selected,am.featured,am.featured,am.urgent,am.sealed,am.hidden,am.project_submission_to_moderation_date,am.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_awaiting_moderation am');
		$this->db->where('am.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = am.county_id', 'left');
		$this->db->join('localities', 'localities.id = am.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = am.postal_code_id', 'left');
		$this->db->order_by('am.id','desc');
		$this->db->limit($this->config->item('user_dashboard_awaiting_moderation_projects_listing_limit'));
		$awaiting_moderation_project_result = $this->db->get();
		$awaiting_moderation_project_data = $awaiting_moderation_project_result->result_array();
		$data["awaiting_moderation_project_data"] = $awaiting_moderation_project_data;
		$data['awaiting_modearion_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_awaiting_moderation')->count_all_results();
		$res['awaiting_moderation_project_data'] = $this->load->view('ajax_user_awaiting_moderation_project_list', $data, true);
		##################### fetch the open bidding projects from database and show on dashboard ############
		$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date');
		$this->db->from('projects_open_bidding op');
		$this->db->where('op.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->order_by('op.id','desc');
		$open_bidding_project_result = $this->db->get();
		$open_bidding_project_data = $open_bidding_project_result->result_array();
		if(!empty($open_bidding_project_data)){
			foreach($open_bidding_project_data as $project_key=>$project_value){
				$open_bidding_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
			}
		}
		$data["open_bidding_project_data"] = $open_bidding_project_data;
		$res['open_bidding_project_data'] = $this->load->view('ajax_user_open_bidding_project_list', $data, true);
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/*
	 * @sid method used in assets/js/dashboard.js for updating draft view when user click on save project as draft
	*/
	public function ajax_update_user_draft_project_view_dashboard() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		##################### fetch the draft projects from database and show on dashboard############
		$this->db->select('pd.project_id,pd.project_title,pd.project_description,pd.project_type,pd.min_budget,pd.max_budget,pd.confidential_dropdown_option_selected,pd.not_sure_dropdown_option_selected,pd.featured,pd.urgent,pd.sealed,pd.hidden,pd.project_save_date,pd.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_draft pd');
		$this->db->where('pd.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = pd.county_id', 'left');
		$this->db->join('localities', 'localities.id = pd.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = pd.postal_code_id', 'left');
		$this->db->order_by('pd.id','desc');
		$this->db->limit($this->config->item('user_dashboard_draft_projects_listing_limit'));
		$draft_project_result = $this->db->get();
		$draft_project_data = $draft_project_result->result_array();
		$data["draft_project_data"] = $draft_project_data;
		$data['draft_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_draft')->count_all_results();
		$res['draft_project_data'] = $this->load->view('ajax_user_draft_project_list', $data, true);
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/*
	 * @sid method used in assets/js/dashboard.js for updating awaiting moderation or open for bidding view when user click on publish button
	*/
	public function ajax_update_user_awaiting_moderation_or_open_for_bidding_project_view_dashboard() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		##################### fetch the awaiting moderation projects from database and show on dashboard############
		$awaiting_moderation_project_data =array();
		$this->db->select('am.project_id,am.project_title,am.project_description,am.project_type,am.min_budget,am.max_budget,am.confidential_dropdown_option_selected,am.not_sure_dropdown_option_selected,am.featured,am.urgent,am.sealed,am.hidden,am.project_submission_to_moderation_date,am.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_awaiting_moderation am');
		$this->db->where('am.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = am.county_id', 'left');
		$this->db->join('localities', 'localities.id = am.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = am.postal_code_id', 'left');
		$this->db->order_by('am.id','desc');
		$this->db->limit($this->config->item('user_dashboard_awaiting_moderation_projects_listing_limit'));
		$awaiting_moderation_project_result = $this->db->get();
		$awaiting_moderation_project_data = $awaiting_moderation_project_result->result_array();
		$data["awaiting_moderation_project_data"] = $awaiting_moderation_project_data;
		$data['awaiting_modearion_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_awaiting_moderation')->count_all_results();
		$res['awaiting_moderation_project_data'] = $this->load->view('ajax_user_awaiting_moderation_project_list', $data, true);
		##################### fetch the open bidding projects from database and show on dashboard ############
		$open_bidding_project_data =array();
		$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
		$this->db->from('projects_open_bidding op');
		$this->db->where('op.project_owner_id',$user[0]->user_id);
		$this->db->where('op.project_expiration_date >= NOW()');
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('user_dashboard_open_bidding_projects_listing_limit'));
		$open_bidding_project_result = $this->db->get();
		$open_bidding_project_data = $open_bidding_project_result->result_array();
		$data["open_bidding_project_data"] = $open_bidding_project_data;
		$data['opeb_bidding_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
		$res['open_bidding_project_data'] = $this->load->view('ajax_user_open_bidding_project_list', $data, true);
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/**
	 * @sid method used in assets/js/dashboard.js to update latest project section on dashboard when project auto approved or manually approved by admin or directly move to open bidding stage based on configuration done in custom config file
	*/
	public function ajax_update_latest_project_dashboard_view() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		// update expired project upgrade status from open bidding table
		$this->Projects_model->update_expired_upgrade_status_open_bidding();
		$user = $this->session->userdata('user');
		##################### fetch the open bidding projects data from database to display under latest project section ##########
		$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_open_bidding op');
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->where('op.project_expiration_date >= NOW()');
		$this->db->where('(op.featured = "Y" OR op.urgent = "Y")');
		$this->db->where('op.project_owner_id !=', $user[0]->user_id);
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('dashboard_right_projects'));
		$data['open_bidding_project_right_side'] = $this->db->get()->result_array();

		$standard_project_cnt = $this->db->where(['featured' => 'N', 'urgent' => 'N', 'sealed' => 'N', 'hidden' => 'N'])->from('projects_open_bidding')->count_all_results();
		if($standard_project_cnt > 0) {
			$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('projects_open_bidding op');
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->where('op.project_expiration_date >= NOW()');
			$this->db->where('op.project_owner_id !=', $user[0]->user_id);
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('dashboard_left_projects'));
			$data['open_bidding_project_left_side'] = $this->db->get()->result_array();
		} else {
			$data['open_bidding_project_left_side'] = [];
		}
		
		
		$next_refresh_time = []; // this variable is used to manage user display activity log and next refresh time
		############################################ standard project refresh ###########################################################
		$standard_project_data = $this->Projects_model->manage_standard_project_refresh($data, $next_refresh_time, $user);
		$data = $standard_project_data['data'];
		$next_refresh_time = $standard_project_data['next_refresh_time'];
		
		
		############################################ end ################################################################################
		
		############################################ featured project refresh ###########################################################
		
		$featured_project_data_membership_included = $this->Projects_model->manage_featured_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user);
		$data = $featured_project_data_membership_included['data'];
		$next_refresh_time = $featured_project_data_membership_included['next_refresh_time'];
		
		$featured_project_data_bonus_based = $this->Projects_model->manage_featured_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user);
		$data = $featured_project_data_bonus_based['data'];
		$next_refresh_time = $featured_project_data_bonus_based['next_refresh_time'];
		
		$featured_project_data = $this->Projects_model->manage_featured_project_refresh_for_real_payment($data, $next_refresh_time, $user);
		$data = $featured_project_data['data'];
		$next_refresh_time = $featured_project_data['next_refresh_time'];
		
		
		############################################ end ################################################################################
		
		############################################ urgent project refresh ###########################################################
		
		$urgent_project_data_membership_included = $this->Projects_model->manage_urgent_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user);
		$data = $urgent_project_data_membership_included['data'];
		$next_refresh_time = $urgent_project_data_membership_included['next_refresh_time'];
		
		$urgent_project_data_bonus_based = $this->Projects_model->manage_urgent_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user);
		$data = $urgent_project_data_bonus_based['data'];
		$next_refresh_time = $urgent_project_data_bonus_based['next_refresh_time'];
		
		$urgent_project_data = $this->Projects_model->manage_urgent_project_refresh_for_real_payment($data, $next_refresh_time, $user);
		$data = $urgent_project_data['data'];
		$next_refresh_time = $urgent_project_data['next_refresh_time'];
		
		############################################ end ################################################################################

		############################################ sealed project refresh ###########################################################
		
		// $sealed_project_data_membership_included = $this->Projects_model->manage_sealed_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user);
		// $data = $sealed_project_data_membership_included['data'];
		// $next_refresh_time = $sealed_project_data_membership_included['next_refresh_time'];
		
		// $sealed_project_data_bonus_based = $this->Projects_model->manage_sealed_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user);
		// $data = $sealed_project_data_bonus_based['data'];
		// $next_refresh_time = $sealed_project_data_bonus_based['next_refresh_time'];
		
		// $sealed_project_data = $this->Projects_model->manage_sealed_project_refresh_for_real_payment($data, $next_refresh_time, $user);
		// $data = $sealed_project_data['data'];
		// $next_refresh_time = $sealed_project_data['next_refresh_time'];

		############################################ end ################################################################################
		
		########################################## Store data for user display activity log #############################################
		if(!empty($next_refresh_time)) {
			foreach($next_refresh_time as $val) {
				// standard and sealed project user disply activity message
				if($val['type'] == 'standard' || $val['type'] == 'sealed' ) {
					$msg = $this->config->item('standard_or_sealed_project_refresh_user_activity_log_displayed_message');
					$msg = str_replace('{project_title}', $val['project_title'], $msg);
					if(!empty($val['next_refresh_time'])) {
						$time_msg = $this->config->item('project_next_refresh_user_activity_log_displayed_message');
						$time_msg = str_replace('{next_refresh_time}', date(DATE_TIME_FORMAT, strtotime($val['next_refresh_time'])), $time_msg);
						$msg .= ' '.$time_msg;
					}
					user_display_log($msg, $val['profile']);
				} else if ($val['type'] == 'featured') { // featured project user disply activity message
					$msg = $this->config->item('featured_project_refresh_user_activity_log_displayed_message');
					$msg = str_replace('{project_title}', $val['project_title'], $msg);
					if(!empty($val['next_refresh_time'])) {
						$time_msg = $this->config->item('project_next_refresh_user_activity_log_displayed_message');
						$time_msg = str_replace('{next_refresh_time}', date(DATE_TIME_FORMAT, strtotime($val['next_refresh_time'])), $time_msg);
						$msg .= ' '.$time_msg;
					}
					user_display_log($msg, $val['profile']);
				} else if ($val['type'] == 'urgent') { // urgent project user disply activity message
					$msg = $this->config->item('urgent_project_refresh_user_activity_log_displayed_message');
					$msg = str_replace('{project_title}', $val['project_title'], $msg);
					if(!empty($val['next_refresh_time'])) {
						$time_msg = $this->config->item('project_next_refresh_user_activity_log_displayed_message');
						$time_msg = str_replace('{next_refresh_time}', date(DATE_TIME_FORMAT, strtotime($val['next_refresh_time'])), $time_msg);
						$msg .= ' '.$time_msg;
					}
					user_display_log($msg, $val['profile']);
				}  
			}
		}
		######################################### end ##################################################################################


		if(!empty($data['open_bidding_project_right_side'])){
			foreach($data['open_bidding_project_right_side'] as $project_key=>$project_value){
				$data['open_bidding_project_right_side'][$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
			}
		}
		if(!empty($data['open_bidding_project_left_side'])){
			foreach($data['open_bidding_project_left_side'] as $project_key=>$project_value){
				$data['open_bidding_project_left_side'][$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
			}
		}

		$res['status'] = 200;
		$res['data'] = $this->load->view('ajax_latest_project_user_dashboard', $data, true);
		echo json_encode($res);
		return;
	}
	/**
	 * @sid method used in assets/js/dashboard.js to handle cancel project and update open bidding and cancel project view on dashboard
	 */
	public function ajax_user_cancelled_open_bidding_project($project_id) {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		$open_bidding_project_data = $this->db->get_where('projects_open_bidding', ['project_id' => $project_id])->row_array();
		if(!empty($open_bidding_project_data)) {
			unset($open_bidding_project_data['id']);
			unset($open_bidding_project_data['views']);
			unset($open_bidding_project_data['revisions']);
			unset($open_bidding_project_data['featured']);
			unset($open_bidding_project_data['urgent']);
			if($open_bidding_project_data['project_type'] == 'fixed') { // insert into fixed budget project cancelled table
				unset($open_bidding_project_data['project_type']);
				$this->db->insert('fixed_budget_projects_cancelled', $open_bidding_project_data);
			}
			if($open_bidding_project_data['project_type'] == 'fulltime') { // insert into fixed budget project cancelled table
				unset($open_bidding_project_data['project_type']);
				$this->db->insert('fulltime_projects_cancelled', $open_bidding_project_data);
			}
			if($open_bidding_project_data['project_type'] == 'hourly') { // insert into fixed budget project cancelled table
				unset($open_bidding_project_data['project_type']);
				$this->db->insert('hourly_rate_based_projects_cancelled', $open_bidding_project_data);
			}

			// Remove project attachement based on project id
			$this->db->delete('projects_attachments', ['project_id' => $project_id]);

			// Set next refresh time to null for cancelled project
			$this->db->limit(1);
			$this->db->order_by('id', 'desc');
			$this->db->update('projects_refresh_sequence_tracking', ['project_next_refresh_time' => null], ['project_id' => $project_id]);
			$this->db->limit(1);
			$this->db->order_by('id', 'desc');
			$this->db->update('proj_refresh_sequence_tracking_membership_included_upgrades', ['project_next_refresh_time' => null], ['project_id' => $project_id]);
			$this->db->limit(1);
			$this->db->order_by('id', 'desc');
			$this->db->update('proj_refresh_sequence_track_bonus_based_purchased_upgrades', ['project_next_refresh_time' => null], ['project_id' => $project_id]);

			$config['ftp_hostname'] = $this->config->item('ftp_hostname');
			$config['ftp_username'] = $this->config->item('ftp_username');
			$config['ftp_password'] = $this->config->item('ftp_password');
			$config['ftp_port'] 	= $this->config->item('ftp_port');
			$config['debug']    = TRUE;
			$this->ftp->connect($config); 
			$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
			$projects_ftp_dir = $this->config->item('projects_ftp_dir');
			$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
			$profile_folder = $user[0]->profile_name;
			
			// remov entry from open bidding table
			$this->db->delete('projects_open_bidding', ['project_id' => $project_id]);
			if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id))) {
				$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id);
			}

			$this->ajax_update_open_bidding_or_expired_and_cancelled_project_dashboard_view('open');
		}
	}
	/**
		* @sid method used in assets/js/dashboard.js to handle cancel project and update open bidding and cancel project view on dashboard
	 */
	public function ajax_user_cancelled_expired_project($project_id) {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		$fixed_budget_expired_project_data = $this->db->get_where('fixed_budget_projects_expired', ['project_id' => $project_id])->row_array();
		if(!empty($fixed_budget_expired_project_data)){
			$expired_project_data = $fixed_budget_expired_project_data;
		}else{
			$hourly_rate_based_expired_project_data = $this->db->get_where('hourly_rate_based_projects_expired', ['project_id' => $project_id])->row_array();
			if(!empty($hourly_rate_based_expired_project_data)){
				$expired_project_data = $hourly_rate_based_expired_project_data;
			}else{
				$fulltime_expired_project_data = $this->db->get_where('fulltime_projects_expired', ['project_id' => $project_id])->row_array();
				if(!empty($fulltime_expired_project_data)){
					$expired_project_data = $fulltime_expired_project_data;
				}
			}
		}
		
		
		
		
		if(!empty($expired_project_data)) {
			unset($expired_project_data['id']);
			unset($expired_project_data['views']);
			unset($expired_project_data['revisions']);
			unset($expired_project_data['featured']);
			unset($expired_project_data['urgent']);
			unset($expired_project_data['project_type']);
			if($expired_project_data['project_type'] == 'fixed'){
				$this->db->insert('fixed_budget_projects_cancelled', $expired_project_data);
			}
			if($expired_project_data['project_type'] == 'hourly'){
				$this->db->insert('hourly_rate_based_projects_cancelled', $expired_project_data);
			}
			if($expired_project_data['project_type'] == 'fulltime'){
				$this->db->insert('fulltime_projects_cancelled', $expired_project_data);
			}
			// Remove project attachement based on project id
			$this->db->delete('projects_attachments', ['project_id' => $project_id]);

			$config['ftp_hostname'] = $this->config->item('ftp_hostname');
			$config['ftp_username'] = $this->config->item('ftp_username');
			$config['ftp_password'] = $this->config->item('ftp_password');
			$config['ftp_port'] 	= $this->config->item('ftp_port');
			$config['debug']    = TRUE;
			$this->ftp->connect($config); 
			$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
			$projects_ftp_dir = $this->config->item('projects_ftp_dir');
			$project_expired_dir = $this->config->item('project_expired_dir');
			$profile_folder = $user[0]->profile_name;
			
			// remov entry from open bidding table
			$this->db->delete('fixed_budget_projects_expired', ['project_id' => $project_id]);
			if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_expired_dir.$project_id))) {
				$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_expired_dir.$project_id);
			}

			$this->ajax_update_open_bidding_or_expired_and_cancelled_project_dashboard_view('expired');
		}
	}

	/**
	 * @sid method used in assets/js/dashboard.js to get newly entered project for latest project section on dashboard
	 */
	public function ajax_get_latest_project_dashboard_view() {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		// update expired project upgrade status from open bidding table
		$this->Projects_model->update_expired_upgrade_status_open_bidding();
		$user = $this->session->userdata('user');
		##################### fetch the open bidding projects data from database to display under latest project section ##########
		$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('projects_open_bidding op');
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->where('op.project_expiration_date >= NOW()');
		$this->db->where('(op.featured = "Y" OR op.urgent = "Y")');
		$this->db->where('op.project_owner_id !=', $user[0]->user_id);
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('dashboard_right_projects'));
		$data['open_bidding_project_right_side'] = $this->db->get()->result_array();
		
		$standard_project_cnt = $this->db->where(['featured' => 'N', 'urgent' => 'N', 'sealed' => 'N', 'hidden' => 'N'])->from('projects_open_bidding')->count_all_results();
		if($standard_project_cnt > 0) {
			$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('projects_open_bidding op');
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->where('op.project_expiration_date >= NOW()');
			$this->db->where('op.project_owner_id !=', $user[0]->user_id);
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('dashboard_left_projects'));
			$data['open_bidding_project_left_side'] = $this->db->get()->result_array();
		} else {
			$data['open_bidding_project_left_side'] = [];
		}
		
		if(!empty($data['open_bidding_project_right_side'])){
			foreach($data['open_bidding_project_right_side'] as $project_key=>$project_value){
				$data['open_bidding_project_right_side'][$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
			}
		}
		if(!empty($data['open_bidding_project_left_side'])){
			foreach($data['open_bidding_project_left_side'] as $project_key=>$project_value){
				$data['open_bidding_project_left_side'][$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
			}
		}

		$res['status'] = 200;
		$res['data'] = $this->load->view('ajax_latest_project_user_dashboard', $data, true);
		echo json_encode($res);
		return;
	}
	/**
	 * @sid method used in assets/js/dashboard.js to get listing either open bidding or expired project with cancel project
	*/
	public function ajax_update_open_bidding_or_expired_and_cancelled_project_dashboard_view($status) {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		if($status == 'expired') {
			##################### fetch the expired projects from database and show on dashboard############
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('fixed_budget_projects_expired op');
			$this->db->where('op.project_owner_id',$user[0]->user_id);
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('user_dashboard_expired_projects_listing_limit'));
			$open_bidding_project_result = $this->db->get();
			$expired_project_data = $open_bidding_project_result->result_array();
			if(!empty($expired_project_data)){
				foreach($expired_project_data as $project_key=>$project_value){
					$expired_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'expired');
				}
			}
			$data['project_status'] = 'expired';
			$data["expired_project_data"] = $expired_project_data;
			$data['expired_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_expired')->count_all_results();
			$res['expired_project_data'] = $this->load->view('ajax_user_expired_project_list', $data, true);
		} else {
			##################### fetch the open bidding projects from database and show on dashboard ############
			$open_bidding_project_data = array();
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
            $this->db->from('projects_open_bidding op');
            $this->db->where('op.project_owner_id',$user[0]->user_id);
            $this->db->where('op.project_expiration_date >= NOW()');
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('user_dashboard_open_bidding_projects_listing_limit'));
			$open_bidding_project_result = $this->db->get();
			$open_bidding_project_data = $open_bidding_project_result->result_array();		
			if(!empty($open_bidding_project_data)){
				foreach($open_bidding_project_data as $project_key=>$project_value){
					$open_bidding_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
				}
			}
			$data["open_bidding_project_data"] = $open_bidding_project_data;
			$data['opeb_bidding_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
			$res['open_bidding_project_data'] = $this->load->view('ajax_user_open_bidding_project_list', $data, true);
		}
		##################### fetch the cancelled projects from database and show on dashboard############
		$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
		$this->db->from('fixed_budget_projects_cancelled op');
		$this->db->where('op.project_owner_id',$user[0]->user_id);
		$this->db->join('counties', 'counties.id = op.county_id', 'left');
		$this->db->join('localities', 'localities.id = op.locality_id', 'left');
		$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
		$this->db->order_by('op.id','desc');
		$this->db->limit($this->config->item('user_dashboard_cancelled_projects_listing_limit'));
		$cancelled_project_result = $this->db->get();
		$cancelled_project_data = $cancelled_project_result->result_array();
		if(!empty($cancelled_project_data)){
			foreach($cancelled_project_data as $project_key=>$project_value){
				$cancelled_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'cancelled');
			}
		}
		$data['project_status'] = 'cancelled';
		$data["cancelled_project_data"] = $cancelled_project_data;
		$data['cancelled_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_cancelled')->count_all_results();
		##################### fetch the fixed budget cancelled by admin projects from database and show on dashboard############
		if($data['cancelled_project_cnt'] < $this->config->item('user_dashboard_cancelled_projects_listing_limit')) {
			$limit = $this->config->item('user_dashboard_cancelled_projects_listing_limit') - $data['cancelled_project_cnt'];
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('fixed_budget_projects_cancelled_by_admin op');
			$this->db->where('op.project_owner_id',$user[0]->user_id);
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($limit);
			$admin_cancelled_project_result = $this->db->get();
			$admin_cancelled_project_data = $admin_cancelled_project_result->result_array();
			if(!empty($admin_cancelled_project_data)){
				foreach($admin_cancelled_project_data as $project_key=>$project_value){
					$admin_cancelled_project_data[$project_key]['delete_by_admin'] = true;
					$admin_cancelled_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'cancelled');
				}
			}
			$data["cancelled_project_data"] = array_merge($data["cancelled_project_data"], $admin_cancelled_project_data);
			$data['cancelled_project_cnt'] += $limit;
		}
		$res['cancelled_project_data'] = $this->load->view('ajax_user_cancelled_project_list', $data, true);
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/**
	 * @sid method used in assets/js/dashboard.js to get listing of project based on status
	*/
	public function ajax_update_project_listing_dashboard_view_by_status($status) {
		if(!check_session_validity()) {
			echo json_encode(['status' => 404]);
			return;
		}
		$user = $this->session->userdata('user');
		if($status == 'awaiting_moderation') {
			##################### fetch the awaiting moderation projects from database and show on dashboard############
			$awaiting_moderation_project_data =array();
			$this->db->select('am.project_id,am.project_title,am.project_description,am.project_type,am.min_budget,am.max_budget,am.confidential_dropdown_option_selected,am.not_sure_dropdown_option_selected,am.featured,am.urgent,am.sealed,am.hidden,am.project_submission_to_moderation_date,am.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('projects_awaiting_moderation am');
			$this->db->where('am.project_owner_id',$user[0]->user_id);
			$this->db->join('counties', 'counties.id = am.county_id', 'left');
			$this->db->join('localities', 'localities.id = am.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = am.postal_code_id', 'left');
			$this->db->order_by('am.id','desc');
			$this->db->limit($this->config->item('user_dashboard_awaiting_moderation_projects_listing_limit'));
			$awaiting_moderation_project_result = $this->db->get();
			$awaiting_moderation_project_data = $awaiting_moderation_project_result->result_array();
			if(!empty($awaiting_moderation_project_data)){
				foreach($awaiting_moderation_project_data as $project_key=>$project_value){
					$awaiting_moderation_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'awaiting_moderation');
				}
			}
			$data["awaiting_moderation_project_data"] = $awaiting_moderation_project_data;
			$data['awaiting_modearion_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_awaiting_moderation')->count_all_results();
			$res['awaiting_moderation_project_data'] = $this->load->view('ajax_user_awaiting_moderation_project_list', $data, true);
		} else if($status == 'open') {
			##################### fetch the open bidding projects from database and show on dashboard ############
			$open_bidding_project_data = array();
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.confidential_dropdown_option_selected,op.not_sure_dropdown_option_selected,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
            $this->db->from('projects_open_bidding op');
            $this->db->where('op.project_owner_id',$user[0]->user_id);
            $this->db->where('op.project_expiration_date >= NOW()');
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('user_dashboard_open_bidding_projects_listing_limit'));
			$open_bidding_project_result = $this->db->get();
			$open_bidding_project_data = $open_bidding_project_result->result_array();		
			if(!empty($open_bidding_project_data)){
				foreach($open_bidding_project_data as $project_key=>$project_value){
					$open_bidding_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'open_for_bidding');
				}
			}
			$data["open_bidding_project_data"] = $open_bidding_project_data;
			$data['opeb_bidding_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
			$res['open_bidding_project_data'] = $this->load->view('ajax_user_open_bidding_project_list', $data, true);
		} else if($status == 'expired') {
			##################### fetch the expired projects from database and show on dashboard############
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('fixed_budget_projects_expired op');
			$this->db->where('op.project_owner_id',$user[0]->user_id);
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('user_dashboard_expired_projects_listing_limit'));
			$open_bidding_project_result = $this->db->get();
			$expired_project_data = $open_bidding_project_result->result_array();
			if(!empty($expired_project_data)){
				foreach($expired_project_data as $project_key=>$project_value){
					$expired_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'expired');
				}
			}
			$data['project_status'] = 'expired';
			$data["expired_project_data"] = $expired_project_data;
			$data['expired_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_expired')->count_all_results();
			$res['expired_project_data'] = $this->load->view('ajax_user_expired_project_list', $data, true);
		} else if($status == 'canceled') {
			##################### fetch the cancelled projects from database and show on dashboard############
			$this->db->select('op.project_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
			$this->db->from('fixed_budget_projects_cancelled op');
			$this->db->where('op.project_owner_id',$user[0]->user_id);
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->order_by('op.id','desc');
			$this->db->limit($this->config->item('user_dashboard_cancelled_projects_listing_limit'));
			$cancelled_project_result = $this->db->get();
			$cancelled_project_data = $cancelled_project_result->result_array();
			if(!empty($cancelled_project_data)){
				foreach($cancelled_project_data as $project_key=>$project_value){
					$cancelled_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'cancelled');
				}
			}
			$data['project_status'] = 'cancelled';
			$data["cancelled_project_data"] = $cancelled_project_data;
			$data['cancelled_project_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_cancelled')->count_all_results();
			##################### fetch the fixed budget cancelled by admin projects from database and show on dashboard############
			if($data['cancelled_project_cnt'] < $this->config->item('user_dashboard_cancelled_projects_listing_limit')) {
				$limit = $this->config->item('user_dashboard_cancelled_projects_listing_limit') - $data['cancelled_project_cnt'];
				$this->db->select('op.project_id,op.project_title,op.project_description,op.min_budget,op.max_budget,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
				$this->db->from('fixed_budget_projects_cancelled_by_admin op');
				$this->db->where('op.project_owner_id',$user[0]->user_id);
				$this->db->join('counties', 'counties.id = op.county_id', 'left');
				$this->db->join('localities', 'localities.id = op.locality_id', 'left');
				$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
				$this->db->order_by('op.id','desc');
				$this->db->limit($limit);
				$admin_cancelled_project_result = $this->db->get();
				$admin_cancelled_project_data = $admin_cancelled_project_result->result_array();
				if(!empty($admin_cancelled_project_data)){
					foreach($admin_cancelled_project_data as $project_key=>$project_value){
						$admin_cancelled_project_data[$project_key]['categories'] = 'fixed';
						$admin_cancelled_project_data[$project_key]['delete_by_admin'] = true;
						$admin_cancelled_project_data[$project_key]['categories'] = $this->Projects_model->get_project_categories($project_value['project_id'],'cancelled');
					}
				}
				$data["cancelled_project_data"] = array_merge($data["cancelled_project_data"], $admin_cancelled_project_data);
				$data['cancelled_project_cnt'] += $limit;
			}
			$res['cancelled_project_data'] = $this->load->view('ajax_user_cancelled_project_list', $data, true);
		}
		$res['status'] = 200;
		echo json_encode($res);
		return;
	}
	/**
	* This function is used to edit the draft project by login user.
	**/
    public function edit_draft_project()
    {
		if(check_session_validity()){ // check session exists or not if exist then it will update user session
		
			if(empty($this->input->get('id'))){
				show_404();
			}
			$user = $this->session->userdata('user');
			$check_project_exists = $this->db // count the number of record in temp_projects table
				->select ('id')
				->from ('projects_draft')
				->where('project_id',$this->input->get('id'))
				->where('project_owner_id', $user[0]->user_id)
				->get ()->num_rows ();
			if($check_project_exists == 0){
				redirect (VPATH . $this->config->item('dashboard_page_url'));
			}
			$project_id = $this->input->get('id');
			
			$data['current_page'] = 'edit_draft_project';
			########## meta information of post project pag ##########
			$data['project_parent_categories'] = $this->Post_project_model->get_project_parent_categories();
			
			if(!empty($project_id)){
				if(! $this->session->userdata('check_redirection_edit_draft_project')){ // if redirection is not set it will redirect to dashboard page
					redirect (VPATH . $this->config->item('dashboard_page_url'));
				}
				$this->session->set_userdata ('check_redirection_preview_draft_project', 1); // set redirection for  preview page of draft project so when user refresh the edit draft page he will redirect to dasboard
				$data['project_id'] = $project_id;
				######## fetch project detail from temp_projects table ########
				$this->Projects_model->check_update_invalid_combination_project_location('projects_draft','project_id',$project_id); // check valid combination of locality_id,county_id,postal_code_id If the combination is not valid it will update locality_id,county_id,postal_code_id  to 0.
				$this->db->select('*');
				$this->db->from('projects_draft');
				$this->db->where('project_id',$project_id);
				$this->db->where('project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
				$project_data = $project_result->result_array();
				if(empty($project_data)){
					redirect(VPATH.$this->config->item('dashboard_page_url'));
				}
				
				$data['project_data'] = $project_data;
				
				$user_detail = $this->db->get_where('user_details', array('user_id' => $user[0]->user_id))->row();
				$data['user_details'] = $user_detail;
				$user_membership_plan_details = $this->db->get_where('membership_plans', array('id' => $user_detail->current_membership_plan_id))->row();
				$data['user_membership_plan_details'] = $user_membership_plan_details;
				$data['count_user_featured_membership_included_upgrades_monthly'] = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user[0]->user_id);
				$data['count_user_urgent_membership_included_upgrades_monthly'] = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user[0]->user_id);
				
				
				########## fetch the categories of draft project and make the dynamic array start ###
				$this->db->select('*');
				$this->db->from('draft_projects_categories_listing_tracking');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_category_result = $this->db->get();
				$project_category_data = $project_category_result->result_array();
				
				$data['project_category_data'] = $project_category_data;
				
				########## fetch the  project attachments ###
				$data['project_attachment_array'] = $this->get_draft_project_attachments($project_id,$user[0]->profile_name);
			
				########## fetch the tags of draft project ###
				$this->db->select('*');
				$this->db->from('draft_projects_tags');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_tag_result = $this->db->get();
				$project_tag_data = $project_tag_result->result_array();
				
				$data['project_id'] = $project_id;
				$data['project_tag_data'] = $project_tag_data;
				
				########## fetch the temp project tags end ###
				$data['fixed_budget_projects_budget_range'] = $this->Post_project_model->get_fixed_budget_projects_budget_range();// drop down options for fixed budget project budget range
			
				$data['hourly_rate_based_budget_projects_budget_range'] = $this->Post_project_model->get_hourly_rate_based_projects_budget_range();// drop down options for hourly rate based project budget range
				
				$data['fulltime_project_salary_range'] = $this->Post_project_model->get_fulltime_projects_salaries_range();// drop down options for fulltime project salary range
				
				$data['counties'] = $this->dashboard_model->get_counties(); // drop down options of counties
				$data['localities'] = $this->dashboard_model->get_localities_selected_county($project_data[0]['county_id']);// drop down options of localities
				$data['postal_codes'] = $this->Post_project_model->get_project_post_codes($project_data[0]['locality_id']);// drop down options of localities
				########## fetch project information from temp_projects table with refrences table end #########
				
				########## fetch open bidding project information of logged in user ##########################
				$data['open_bidding_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
				########## fetch fixed budget expired project information of logged in user ##########################
				$expired_project_cnt = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_expired')->count_all_results();
				$data['open_bidding_cnt'] += $expired_project_cnt;
				
				################## get the user_details #################
				$data['user_details'] = $this->db->get_where('user_details', array('user_id' => $user[0]->user_id))->row();
				###############################################
				
				$count_project_categories = $this->db // count the number of categories of draft project
					->select ('id')
					->from ('draft_projects_categories_listing_tracking')
					->where('project_id',$project_id)
					->get ()->num_rows ();
					
				$count_project_attachments = $this->db // count the number of attachment of draft project
					->select ('id')
					->from ('draft_projects_attachments')
					->where('project_id',$project_id)
					->get ()->num_rows ();
					
				$count_project_tags = $this->db // count the number of tags of draft project
					->select ('id')
					->from ('draft_projects_tags')
					->where('project_id',$project_id)
					->get ()->num_rows ();	
					
				$count_project_postal_codes = $this->db
				->select ('id')
				->from ('postal_codes')
				->where ('locality_id', $project_data[0]['locality_id'])
				->get ()->num_rows ();

				$data['count_project_categories'] = $count_project_categories;
				$data['count_project_attachments'] = $count_project_attachments;
				$data['count_project_tags'] = $count_project_tags;
				$data['count_project_postal_codes'] = $count_project_postal_codes;
				########## meta information of edir draft project pag ##########
				
				$edit_draft_project_title_meta_tag = strip_tags($project_data[0]['project_title']);
				$edit_draft_project_title_meta_tag = substr($edit_draft_project_title_meta_tag,0,$this->config->item('project_title_meta_tag_character_limit'))." | ".SITE_TITLE_META_TAG;
				$edit_draft_project_preview_description_meta_tag = strip_tags($project_data[0]['project_description']);
				$edit_draft_project_preview_description_meta_tag = substr($edit_draft_project_preview_description_meta_tag,0,$this->config->item('project_description_meta_description_character_limit'));
				
				$this->session->unset_userdata ('check_redirection_edit_preview');
				$data['meta_tag'] = '<title>' . $edit_draft_project_title_meta_tag . '</title><meta name="description" content="' . $edit_draft_project_preview_description_meta_tag . '"/>';
				$this->session->unset_userdata ('check_redirection_edit_draft_project'); // unset the redirection so it prevent the page direct hit from browser
				$this->layout->view('edit_draft_project', '', $data, 'normal');
			}else{
				redirect (VPATH . $this->config->item('dashboard_page_url'));
			}
		}else{
			redirect (VPATH);
		}
    }
	
	/**
	*	This function is used to update the draft project by login user.
	**/
    public function update_draft_project()
    {
		if($this->input->is_ajax_request ()){
			if ($this->input->post () )
			{
				if(check_session_validity()){ 
					$project_id = $this->input->post ('project_id');
					$user = $this->session->userdata('user');
					$count_draft_project = $this->db // count the number of record in projects_draft table
					->select ('id')
					->from ('projects_draft')
					->where('project_id',$project_id)
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
					if($count_draft_project > 0){
						$post_data = $this->input->post ();
						$response = $this->Post_project_model->post_project_validation($post_data);;
						if($response['status'] == 'SUCCESS'){
							$project_locality_id = 0;$project_county_id = 0;$escrow_payment_method = 'N';
							$offline_payment_method = 'N';$upgrade_type_featured = 'N';$upgrade_type_urgent = 'N';
							$upgrade_type_sealed = 'N';$upgrade_type_hidden = 'N';$min_budget = 0;$max_budget = 0;
							$postal_code_id = 0;$not_sure_dropdown_option_selected = 'N';
							$confidential_dropdown_option_selected = 'N';$project_type = "fixed";$confidential_after_expiration_selected = 'N';
							if($this->input->post('escrow_payment_method') == 'Y'){
								$escrow_payment_method = $this->input->post('escrow_payment_method');
							}
							
							if($this->input->post ('offline_payment_method') == 'Y'){
								$offline_payment_method = $this->input->post ('offline_payment_method');
							}
							
							if(!empty($this->input->post ('project_budget'))){
								$project_budget = $this->input->post ('project_budget');
								if($project_budget == 'confidential_dropdown_option_selected'){
									$confidential_dropdown_option_selected = 'Y';
								}else if($project_budget == 'not_sure_dropdown_option_selected'){
									$not_sure_dropdown_option_selected = 'Y';
								}else{
									$project_budget_array = explode("_",$this->input->post ('project_budget'));
									$min_budget = $project_budget_array[0];
									$max_budget = $project_budget_array[1]; 
								}
							}
							if(!empty($this->input->post ('location_option'))){
								if(!empty($this->input->post ('project_locality_id'))){
								$project_locality_id = $this->input->post ('project_locality_id');
								}if(!empty($this->input->post ('project_county_id'))){
									$project_county_id = $this->input->post ('project_county_id');
								}
							
							}
							if(!empty($this->input->post ('upgrade_type_featured'))){
								$upgrade_type_featured = 'Y';
							}
							if(!empty($this->input->post ('upgrade_type_hidden'))){
								$upgrade_type_hidden = 'Y';
							}
							if(!empty($this->input->post ('upgrade_type_sealed'))){
								$upgrade_type_sealed = 'Y';
							}
							if(!empty($this->input->post ('upgrade_type_urgent'))){
								$upgrade_type_urgent = 'Y';
							}
							if(!empty($this->input->post ('confidential_after_expiration_selected'))){
								$confidential_after_expiration_selected = 'Y';
							}
							if(!empty($this->input->post('project_county_id')) && !empty($this->input->post ('project_locality_id'))){
							
								$postal_code_id = $this->input->post('project_postal_code_id');
								
							}
							if(!empty($this->input->post ('project_type_main')) && $this->input->post ('project_type_main') == 'post_project'){
								$project_type = $this->input->post('project_type');
								
							
							}else if(!empty($this->input->post ('project_type_main')) && $this->input->post ('project_type_main') == 'post_fulltime_position'){
								$project_type = 'fulltime';
							}
							$draft_project_data = array (
								'project_owner_id'=>$user[0]->user_id,
								'project_save_date'=>date('Y-m-d H:i:s'),
								'project_title'=>$this->input->post('project_title'),
								'project_description'=>$this->input->post('project_description'),
								'locality_id'=>$project_locality_id,
								'county_id'=>$project_county_id,
								'postal_code_id'=>$postal_code_id,
								'project_type'=>$project_type,
								'min_budget'=>$min_budget,
								'max_budget'=>$max_budget,
								'not_sure_dropdown_option_selected'=>$not_sure_dropdown_option_selected,
								'confidential_dropdown_option_selected'=>$confidential_dropdown_option_selected,
								'escrow_payment_method'=>$escrow_payment_method,
								'offline_payment_method'=>$offline_payment_method,
								'confidential_after_expiration_selected'=>$confidential_after_expiration_selected,
								'featured'=>$upgrade_type_featured,
								'urgent'=>$upgrade_type_urgent,
								'sealed'=>$upgrade_type_sealed,
								'hidden'=>$upgrade_type_hidden
								   
							);
							$this->db->where ('project_id', $project_id);
							$this->db->update ('projects_draft', $draft_project_data); // save data in projects_draft table from edit draft form
							
							$this->db->delete('draft_projects_tags', array('project_id' => $project_id));
							$this->db->delete('draft_projects_categories_listing_tracking', array('project_id' => $project_id));
							
							
							foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
							
								$project_category_id = 0;
								$project_parent_category_id = 0;
								if(!empty($project_category_value['project_parent_category'])){
									if(isset($project_category_value['project_child_category']) && !empty($project_category_value['project_child_category']))
									{
									
										$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);

										$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($project_category_value['project_parent_category'],$project_category_value['project_child_category']);	
										
										if($check_project_parent_category_exist){
											if($check_project_child_category_exist){
												
												$project_category_id = $project_category_value['project_child_category'];
												$project_parent_category_id = $project_category_value['project_parent_category'];
											
											}else{
											
												$project_category_id =  $project_category_value['project_parent_category'];
												$project_parent_category_id = 0;
												
											}
										}
								
									
									}else{
									
										$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
										if($check_project_parent_category_exist){
											
											$project_category_id =  $project_category_value['project_parent_category'];
											$project_parent_category_id = 0;
										
										}
									}
									
								}
								
								if(!empty($project_category_id) || !empty($project_parent_category_id)){
									$this->db->insert ('draft_projects_categories_listing_tracking', array(
										'project_id' => $project_id,
										'draft_project_category_id' => $project_category_id,
										'draft_project_parent_category_id' => $project_parent_category_id 
									));
								}
							}
						  
							if(!empty($this->input->post('project_tag'))){
								foreach($this->input->post('project_tag') as $project_tag_key){
									if(!empty($project_tag_key['tag_name'])){
										$this->db->insert ('draft_projects_tags', array('project_id' => $project_id,
										'draft_project_tag_name' => $project_tag_key['tag_name']));
										// save data in draft_projects_tags table from post project form
									}
								}	
							}
							$msg['status'] = 'SUCCESS';
							$msg['message'] = '';
							echo json_encode ($msg);
						
						}else{
						
							echo json_encode ($response);
						}
					}else{
						$msg['status'] = 404;
						$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
						echo json_encode ($msg);
						die;
					}
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
			
				}	
			}else{
				show_404();
			}
		}else{
		
			show_404();
		}
	}
	
	/**
	 * This function is used to show the preview of draft project .
	*/
	public function preview_draft_project(){
		if(check_session_validity()){ 
			if(empty($this->input->get('id'))){
				show_404();
			}
			$project_id = $this->input->get('id');
			$user = $this->session->userdata('user');
			$count_project = $this->db // count the number of record in projects_draft table
					->select ('id')
					->from ('projects_draft')
					->where('project_id',$project_id)
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
			
			if($count_project > 0){
				$this->session->set_userdata ('check_redirection_edit_draft_project', 1); // set redirection for edit draft page so when user refresh the edit draft page he will redirect to dasboard
				if(! $this->session->userdata('check_redirection_preview_draft_project')){ // if redirection is not set it will redirect to dashboard page
					redirect (VPATH . $this->config->item('dashboard_page_url'));
				}
				// set redirection for edit draft page so when user refresh the edit draft page he will redirect to dasboard
				########## fetch the project data from temporary tables ###
				$this->db->select('projects_draft.*,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
				$this->db->from('projects_draft');
				$this->db->join('counties', 'counties.id = projects_draft.county_id', 'left');
				$this->db->join('localities', 'localities.id = projects_draft.locality_id', 'left');
				$this->db->join('postal_codes', 'postal_codes.id = projects_draft.postal_code_id', 'left');
				$this->db->where('projects_draft.project_id',$project_id);
				$this->db->where('projects_draft.project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
				$project_data = $project_result->result_array();
				$data['project_id'] = $project_id;
				$data['project_data'] = $project_data;

				$user_detail = $this->db->get_where('user_details', ['user_id' => $user[0]->user_id])->row_array();
				$user_membership_plan_detail = $this->db->get_where('membership_plans', ['id' => $user_detail['current_membership_plan_id']])->row_array();
				
				
				// check the user account balance,bonus balance,account balance is sufficient for purchase upgrade
				$count_user_featured_membership_included_upgrades_monthly = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user[0]->user_id); // count user membership featured  upgrade

				$count_user_urgent_membership_included_upgrades_monthly = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user[0]->user_id);// count user membership urgent upgrade
				$upgraded_project_price = 0;
				if($project_data[0]['featured'] == 'Y'){
					if($user_membership_plan_detail['included_number_featured_upgrades'] == $count_user_featured_membership_included_upgrades_monthly){
						$upgraded_project_price += $this->config->item('project_upgrade_price_featured');
					}
				}
				if($project_data[0]['featured'] == 'Y'){
					if($user_membership_plan_detail['included_number_urgent_upgrades'] == $count_user_urgent_membership_included_upgrades_monthly){
						$upgraded_project_price += $this->config->item('project_upgrade_price_urgent');
					}
				}
				if($project_data[0]['featured'] == 'Y'){
					$upgraded_project_price += $this->config->item('project_upgrade_price_sealed');
				}
				if($project_data[0]['featured'] == 'Y'){
					$upgraded_project_price += $this->config->item('project_upgrade_price_hidden');
				}
				if(floatval($upgraded_project_price) > 0){
					$total_user_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'] + $user_detail['user_account_balance'];
					if(floatval($upgraded_project_price) > floatval($total_user_balance) ){
					
						$res = array(
								'status' => 400,
								'error' => $this->config->item('user_post_upgraded_project_insufficient_funds_error_message') // define in post_project_custom config
							);
						echo json_encode($res);
						die;
					}
				}
				
				
				########## fetch the login user data ###
				$this->db->select('account_type,first_name,last_name,company_name,account_validation_date');
				$this->db->from('users');
				$this->db->where('user_id',$project_data[0]['project_owner_id']);
				$user_result = $this->db->get();
				$user_data = $user_result->result_array();
				$data['user_data'] = $user_data;
				########## fetch the draft project tags ###
				$this->db->select('draft_projects_tags.draft_project_tag_name');
				$this->db->from('draft_projects_tags');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_tag_result = $this->db->get();
				$project_tag_data = $project_tag_result->result_array();
				$data['project_tag_data'] = $project_tag_data;
				$data['project_attachment_data'] = $this->get_draft_project_attachments($project_id,$user[0]->profile_name);
				
				########## fetch the draft project categories and make the dynamic array start ###
				$this->db->select('category_project.name as category_name,parent_category_project.name as parent_category_name');
				$this->db->from('draft_projects_categories_listing_tracking as category_tracking');
				$this->db->join('categories_projects as category_project', 'category_project.id = category_tracking.draft_project_category_id', 'left');
				$this->db->join('categories_projects as parent_category_project', 'parent_category_project.id = category_tracking.draft_project_parent_category_id', 'left');
				$this->db->where('category_tracking.project_id',$project_id);
				$this->db->order_by('category_project.name',"asc");
				$category_result = $this->db->get();
				$category_data = $category_result->result_array();
				if(!empty($category_data)){
					foreach($category_data as $key=>$value){
						if(!empty($value['parent_category_name'])){
							$category_data[$key]['category_order_value'] =  $value['parent_category_name'];
						}else{
							$category_data[$key]['category_order_value'] =  $value['category_name'];
						}
					}
					sortArrayBySpecificKeyValue('category_order_value',$category_data, 'asc');// sort category array by key:category_order_value value
				}
				
				$data['category_data'] = $category_data;
				$data['project_id'] = $project_id;
				$data['current_page'] = 'preview_draft_project';
				
				$draft_project_preview_title_meta_tag = strip_tags($project_data[0]['project_title']);
				$draft_project_preview_title_meta_tag = substr($draft_project_preview_title_meta_tag,0,$this->config->item('project_title_meta_tag_character_limit'))." | ".SITE_TITLE_META_TAG;
				$draft_project_preview_description_meta_tag = strip_tags($project_data[0]['project_description']);
				$draft_project_preview_description_meta_tag = substr($draft_project_preview_description_meta_tag,0,$this->config->item('project_description_meta_description_character_limit'));
				
				########## fetch open bidding project information of logged in user ##########################
				$data['open_bidding_cnt'] = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('projects_open_bidding')->count_all_results();
				########## fetch fixed budget expired project information of logged in user ##########################
				$expired_project_cnt = $this->db->where(['project_owner_id' => $user[0]->user_id])->from('fixed_budget_projects_expired')->count_all_results();
				$data['open_bidding_cnt'] += $expired_project_cnt;
				################## get the user_details #################
				$data['user_details'] = $this->db->get_where('user_details', array('user_id' => $user[0]->user_id))->row();
				$data['meta_tag'] = '<title>' . $draft_project_preview_title_meta_tag . '</title><meta name="description" content="' . $draft_project_preview_description_meta_tag . '"/>';
				//$this->layout->view ('project_preview', '', $data, 'include');
				$this->session->unset_userdata ('check_redirection_preview_draft_project'); // unset the redirection so it prevent the page direct hit from browser of preview page of draft project page
				$this->load->view('projects/preview_draft_project', $data);
			
			}else{
				redirect (VPATH . $this->config->item('dashboard_page_url'));
			}
		}else{
			redirect (VPATH);
		
		}
	
	}
	
	/**
	*	This function is used to move project from draft to waiting moderations status  by login user.
	**/
	public function publish_draft_project()
    {
		if( $this->input->is_ajax_request ()){
			if ($this->input->post ())
			{
				if(check_session_validity()){ // check session exists or not if exist then it will update user session
				$project_id = $this->input->post ('project_id');
					
					$page_type = $this->input->post ('page_type');
					$user = $this->session->userdata('user');
					$page_type_array = array('preview','form');
					$check_project_exists = $this->db // count the number of record in temp_projects table
					->select ('id')
					->from ('projects_draft')
					->where('project_id',$project_id)
					->where('project_owner_id', $user[0]->user_id)
					->get ()->num_rows ();
					if($check_project_exists > 0 )
					{
						
						if (in_array($page_type, $page_type_array))
						{
							
							######### apply validation for open projects ######
							$user_total_open_projects_count = $this->Projects_model->get_user_open_projects_count($user[0]->user_id);
							$user_detail = $this->db->get_where('user_details', array('user_id' => $user[0]->user_id))->row_array();
							
							if($user_detail['current_membership_plan_id'] == 1){
							
								if($user_total_open_projects_count >= $this->config->item('free_subscribers_max_number_of_open_projects'))
								{
									$res = array(
									'status' => 400,
											'location' => '',
											'error' => $this->config->item('free_user_account_open_project_limit_validation_post_project_message') // define in post_project_custom config
										);
									echo json_encode($res);
									die;
								
								
								}
							}
							if($user_detail['current_membership_plan_id'] == 4){
								if($user_total_open_projects_count >= $this->config->item('gold_subscribers_max_number_of_open_projects'))
								{
									$res = array(
											'status' => 400,
											'location' => '',
											'error' => $this->config->item('gold_user_account_open_project_limit_validation_post_project_message') // define in post_project_custom config
										);
									echo json_encode($res);
									die;
								
								}
							
							}
							
							$count_awaiting_moderation_project = $this->db // count the number of record in temp_projects table
							->select ('id')
							->from ('projects_awaiting_moderation')
							->where('project_id',$project_id)
							->where('project_owner_id',$user[0]->user_id)
							->get ()->num_rows ();
							
							$user_membership_plan_detail = $this->db->get_where('membership_plans', ['id' => $user_detail['current_membership_plan_id']])->row_array();
							if($count_awaiting_moderation_project == 0) { 
								
								if($page_type == 'preview') { // this block will execute when user click "publish project" button on draft preview page
									// move data from project drafts table to projects_awaiting_moderation table
									$this->db->select('*');
									$this->db->from('projects_draft');
									$this->db->where('projects_draft.project_id',$project_id);
									$this->db->where('projects_draft.project_owner_id',$user[0]->user_id);
									$project_result = $this->db->get();
									$project_data = $project_result->result_array();
									$awaiting_moderation_project_data = array (
										'project_id'=>$project_id,
										'project_owner_id'=>$user[0]->user_id,
										'project_submission_to_moderation_date'=>date('Y-m-d H:i:s'),
										'project_title'=>$project_data[0]['project_title'],
										'project_description'=>$project_data[0]['project_description'],
										'locality_id'=>$project_data[0]['locality_id'],
										'county_id'=>$project_data[0]['county_id'],
										'postal_code_id'=>$project_data[0]['postal_code_id'],
										'project_type'=>$project_data[0]['project_type'],
										'min_budget'=>$project_data[0]['min_budget'],
										'max_budget'=>$project_data[0]['max_budget'],
										'confidential_dropdown_option_selected'=>$project_data[0]['confidential_dropdown_option_selected'],
										'not_sure_dropdown_option_selected'=>$project_data[0]['not_sure_dropdown_option_selected'],
										'escrow_payment_method'=>$project_data[0]['escrow_payment_method'],
										'offline_payment_method'=>$project_data[0]['offline_payment_method'],
										'confidential_after_expiration_selected'=>$project_data[0]['confidential_after_expiration_selected'],
										'featured'=>$project_data[0]['featured'],
										'urgent'=>$project_data[0]['urgent'],
										'sealed'=>$project_data[0]['sealed'],
										'hidden'=>$project_data[0]['hidden']
									
									);
									
									$this->db->select('draft_project_category_id,draft_project_parent_category_id');
									$this->db->from('draft_projects_categories_listing_tracking');
									$this->db->where('project_id',$project_id);
									$this->db->order_by('id',"asc");
									$project_category_result = $this->db->get();
									$project_category_data = $project_category_result->result_array();
									
									###################### check that user selected category is valid or not(admin deactive/delete the category)
									$check_project_parent_category_status = false;
									if(!empty($project_category_data)){
										foreach($project_category_data as $category_key => $category_value){
											$project_parent_category_id = 0;
											if(!empty($category_value['draft_project_parent_category_id'])){
												$project_parent_category_id = $category_value['draft_project_parent_category_id'];
											
											}else{
												$project_parent_category_id = $category_value['draft_project_category_id'];
											
											}
											$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_parent_category_id);
											if($check_project_parent_category_exist){
												$check_project_parent_category_status = true;
												break;
											}
											
										}
									}
									if(!$check_project_parent_category_status){
										$res = [
											'status' => 400,
											'error' => $this->config->item('post_project_valid_category_not_existent_popup_message'),
											'location'=>''
										];
										echo json_encode($res);
										die;
									}
									
									
									
									
									// @sid code to set auto approval date into projects_awaiting_moderation
									$auto_approval_flag = false; // This flag is used to identify where the entry of temporary project table move either [awaiting moderation / open for bidding]
									
									// @sid this code block is used to update user account balance based on project upgrade 
									
									
									// check the user account balance,bonus balance,account balance is sufficient for purchase upgrade
									$count_user_featured_membership_included_upgrades_monthly = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user[0]->user_id); // count user membership featured  upgrade
					
									$count_user_urgent_membership_included_upgrades_monthly = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user[0]->user_id);// count user membership urgent upgrade
									$upgraded_project_price = 0;
									if($project_data[0]['featured'] == 'Y'){
										if($count_user_featured_membership_included_upgrades_monthly >=$user_membership_plan_detail['included_number_featured_upgrades']){
											$upgraded_project_price += $this->config->item('project_upgrade_price_featured');
										}
									}
									if($project_data[0]['urgent'] == 'Y'){
										if($count_user_urgent_membership_included_upgrades_monthly >=$user_membership_plan_detail['included_number_urgent_upgrades']){
											$upgraded_project_price += $this->config->item('project_upgrade_price_urgent');
										}
									}
									if($project_data[0]['sealed'] == 'Y'){
										$upgraded_project_price += $this->config->item('project_upgrade_price_sealed');
									}
									if($project_data[0]['hidden'] == 'Y'){
										$upgraded_project_price += $this->config->item('project_upgrade_price_hidden');
									}
									if(floatval($upgraded_project_price) > 0){
										$total_user_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'] + $user_detail['user_account_balance'];
										if(floatval($upgraded_project_price) > floatval($total_user_balance) ){
										
											$res = array(
													'status' => 400,
													'location' => '',
													'error' => $this->config->item('user_post_upgraded_project_insufficient_funds_error_message') // define in post_project_custom config
												);
											echo json_encode($res);
											die;
										}
									}
									if($project_data[0]['featured'] == 'Y' || $project_data[0]['urgent'] == 'Y' || $project_data[0]['sealed'] == 'Y' || $project_data[0]['hidden'] == 'Y') {
										if($user_detail['current_membership_plan_id'] == 1) {
											$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('free_user_upgraded_project_auto_approval_min'), $this->config->item('free_user_upgraded_project_auto_approval_max'));
											if($auto_approve_date != 0) {
												$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
											} else {
												$auto_approval_flag = true;
											}
										} else {
											$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('paying_subscriber_upgraded_project_auto_approval_min'), $this->config->item('paying_subscriber_upgraded_project_auto_approval_max'));
											if($auto_approve_date != 0) {
												$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
											} else {
												$auto_approval_flag = true;
											}
										}
									} else {
										if($user_detail['current_membership_plan_id'] == 1) {
											$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('free_user_standard_project_auto_approval_min'), $this->config->item('free_user_standard_project_auto_approval_max'));
											if($auto_approve_date != 0) {
												$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
											} else {
												$auto_approval_flag = true;
											}
										} else {
											$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('paying_subscriber_standard_project_auto_approval_min'), $this->config->item('paying_subscriber_standard_project_auto_approval_max'));
											if($auto_approve_date != 0) {
												$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
											} else {
												$auto_approval_flag = true;
											}
										}
									}
									if($auto_approval_flag) {
										$time_arr = explode(':', $this->config->item('standard_project_availability'));
										$project_expire_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
										$open_for_bidding_project_data = [
											'project_id'=>$project_id,
											'project_owner_id'=>$user[0]->user_id,
											'project_posting_date' => date('Y-m-d H:i:s'),
											'project_expiration_date' => $project_expire_date,
											'project_title'=>$project_data[0]['project_title'],
											'project_description'=>$project_data[0]['project_description'],
											'locality_id'=>$project_data[0]['locality_id'],
											'county_id'=>$project_data[0]['county_id'],
											'postal_code_id' => $project_data[0]['postal_code_id'],
											'project_type' => $project_data[0]['project_type'],
											'min_budget'=>$project_data[0]['min_budget'],
											'max_budget'=>$project_data[0]['max_budget'],
											'confidential_dropdown_option_selected'=>$project_data[0]['confidential_dropdown_option_selected'],
											'not_sure_dropdown_option_selected'=>$project_data[0]['not_sure_dropdown_option_selected'],
											'confidential_after_expiration_selected'=>$project_data[0]['confidential_after_expiration_selected'],
											'featured'=>$project_data[0]['featured'],
											'urgent'=>$project_data[0]['urgent'],
											'sealed'=>$project_data[0]['sealed'],
											'hidden'=>$project_data[0]['hidden'],
											'views' => 0,
											'revisions'=> 0
										];
										$this->db->insert ('projects_open_bidding', $open_for_bidding_project_data); // save data in fixed_budget_projects_open_bidding table from post project form
										
										// insert the data regrading purchase tracking and refresh sequence and manage the user bonus balance and account balance
										$this->Post_project_model->user_project_upgrade_purchase_refresh_sequence_tracking_save($open_for_bidding_project_data,$user[0]->user_id); 
										
										// move data from draft_projects_tags table to awaiting_moderation_projects_tags table
										$this->db->select('draft_project_tag_name');
										$this->db->from('draft_projects_tags');
										$this->db->where('project_id',$project_id);
										$this->db->order_by('id',"asc");
										$project_tag_result = $this->db->get();
										$project_tag_data = $project_tag_result->result_array();
										if(!empty($project_tag_data)){
											foreach($project_tag_data as $tag_key => $tag_value){
												$this->db->insert ('projects_tags', array('project_id'=>$project_id,'project_tag_name'=> $tag_value['draft_project_tag_name']));
											}
										
										}
										// move data from draft_projects_categories_listing_tracking table to projects_categories_listing_tracking table
										
										if(!empty($project_category_data)){
										
										
											foreach($project_category_data as $category_key => $category_value){
												$project_category_id = 0;
												$project_parent_category_id = 0;
												if(!empty($category_value['draft_project_parent_category_id'])){
												
													$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($category_value['draft_project_parent_category_id']);
					
													$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($category_value['draft_project_parent_category_id'],$category_value['draft_project_category_id']);
													if($check_project_parent_category_exist){
														if($check_project_child_category_exist){
															
															$project_category_id = $category_value['draft_project_category_id'];
															$project_parent_category_id = $category_value['draft_project_parent_category_id'];
														
														}else{
														
															$project_category_id =  $category_value['draft_project_parent_category_id'];
															$project_parent_category_id = 0;
															
														}
													}
												
												}else{
													$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($category_value['draft_project_category_id']);
													if($check_project_parent_category_exist){
													
														$project_category_id =  $category_value['draft_project_category_id'];
														$project_parent_category_id = 0;
													
													}
												
												}
											
												if(!empty($project_category_id) || !empty($project_parent_category_id)){
												$this->db->insert ('projects_categories_listing_tracking', array('project_id'=>$project_id,'project_category_id'=> $project_category_id,'project_parent_category_id'=>$project_parent_category_id));
												
												}
											}
										
										}
										// move data from draft_projects_attachments table to awaiting_moderation_projects_attachments table
										$this->db->select('draft_project_attachment_name');
										$this->db->from('draft_projects_attachments');
										$this->db->where('project_id',$project_id);
										$this->db->order_by('id',"asc");
										$project_attachment_result = $this->db->get();
										$project_attachment_data = $project_attachment_result->result_array();
										if(!empty($project_attachment_data)){
											
											$this->load->library('ftp');
											$config['ftp_hostname'] = $this->config->item('ftp_hostname');
											$config['ftp_username'] = $this->config->item('ftp_username');
											$config['ftp_password'] = $this->config->item('ftp_password');
											$config['ftp_port'] 	= $this->config->item('ftp_port');
											$config['debug']    = TRUE;
											$this->ftp->connect($config); 
											$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
											$projects_ftp_dir = $this->config->item('projects_ftp_dir');
											$project_draft_dir = $this->config->item('project_draft_dir');
											$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
											$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
											$profile_folder     = $user[0]->profile_name;
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
											
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir, 0777);// create awaiting_moderation directory in projects folder
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id , 0777); // create the directory by using  project id
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir , 0777); // create the owner attachment directory by using  project id
										
											foreach($project_attachment_data as $attachment_key => $attachment_value){
											
												if(!empty($attachment_value['draft_project_attachment_name'])){
													$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
													$file_size = $this->ftp->get_filesize($source_path);
													if($file_size != '-1'){
														$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir.$attachment_value['draft_project_attachment_name'];
														$this->ftp->move($source_path, $destination_path);
														$this->db->insert ('projects_attachments', array('project_id'=>$project_id,'project_attachment_name'=> $attachment_value['draft_project_attachment_name']));
													}
												}
											}
											$this->db->delete('draft_projects_attachments', array('project_id' => $project_id));
											$draft_project_attachment_list = $this->ftp->list_files($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);
											if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id))){
												$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR);// delete project directory 
											}
											$this->ftp->close();
										}
										// trigger socket event to update latest project section on user dashboard
										$url = SOCKET_URL."/updateLatestProjectOnUserDashboard/".$user[0]->user_id.'?authorization_key='.$this->config->item('url_authrization_key');
										$options = array(
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_SSL_VERIFYPEER => false
										);
										try {
											$ch = curl_init( $url );
											curl_setopt_array( $ch, $options );
											curl_exec( $ch );
											curl_close( $ch );
										} catch(Exception $e) {
										}
									
										
										if($project_data[0]['featured'] == 'N' && $project_data[0]['urgent'] == 'N' && $project_data[0]['sealed'] == 'N' && $project_data[0]['hidden'] == 'N' ) {
											// insert into projects_refresh_sequence_tracking table
											$time_arr = explode(':', $this->config->item('standard_project_refresh_sequence'));
											$refresh_sequence_data = [
												'project_id' => $project_id,
												'project_upgrade_purchase_tracking_id' => null,
												'project_last_refresh_time' => null,
												'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
											];
											$this->db->insert('projects_refresh_sequence_tracking', $refresh_sequence_data);
										}
										
										
										
									} else {
										$this->db->insert ('projects_awaiting_moderation', $awaiting_moderation_project_data);
										// move data from draft_projects_tags table to awaiting_moderation_projects_tags table
										
										// insert the data regrading purchase tracking and manage the user bonus balance and account balance
										$this->Post_project_model->user_project_upgrade_purchase_tracking_save($awaiting_moderation_project_data,$user[0]->user_id); 
										$this->db->select('draft_project_tag_name');
										$this->db->from('draft_projects_tags');
										$this->db->where('project_id',$project_id);
										$this->db->order_by('id',"asc");
										$project_tag_result = $this->db->get();
										$project_tag_data = $project_tag_result->result_array();
										if(!empty($project_tag_data)){
											foreach($project_tag_data as $tag_key => $tag_value){
												$this->db->insert ('awaiting_moderation_projects_tags', array('project_id'=>$project_id,'awaiting_moderation_project_tag_name'=> $tag_value['draft_project_tag_name']));
											}
										
										}
										// move data from draft_projects_categories_listing_tracking table to awaiting_moderation_projects_categories_listing_tracking table and check that category is valid or not
										if(!empty($project_category_data)){
										
										
											foreach($project_category_data as $category_key => $category_value){
												$project_category_id = 0;
												$project_parent_category_id = 0;
												if(!empty($category_value['draft_project_parent_category_id'])){
												
													$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($category_value['draft_project_parent_category_id']);
					
													$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($category_value['draft_project_parent_category_id'],$category_value['draft_project_category_id']);
													if($check_project_parent_category_exist){
														if($check_project_child_category_exist){
															
															$project_category_id = $category_value['draft_project_category_id'];
															$project_parent_category_id = $category_value['draft_project_parent_category_id'];
															
														}else{
														
															$project_category_id =  $category_value['draft_project_parent_category_id'];
															$project_parent_category_id = 0;
															
														}
													}
												
												}else{
													$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($category_value['draft_project_category_id']);
													if($check_project_parent_category_exist){
													
														$project_category_id =  $category_value['draft_project_category_id'];
														$project_parent_category_id = 0;
													
													}
												
												}
											
												if(!empty($project_category_id) || !empty($project_parent_category_id)){
												$this->db->insert ('awaiting_moderation_projects_categories_listing_tracking', array('project_id'=>$project_id,'awaiting_moderation_project_category_id'=> $project_category_id,'awaiting_moderation_project_parent_category_id'=>$project_parent_category_id));
												
												}
											}
										
										}
										
										
										// move data from draft_projects_attachments table to awaiting_moderation_projects_attachments table
										$this->db->select('draft_project_attachment_name');
										$this->db->from('draft_projects_attachments');
										$this->db->where('project_id',$project_id);
										$this->db->order_by('id',"asc");
										$project_attachment_result = $this->db->get();
										$project_attachment_data = $project_attachment_result->result_array();
										if(!empty($project_attachment_data)){
											
											$this->load->library('ftp');
											$config['ftp_hostname'] = $this->config->item('ftp_hostname');
											$config['ftp_username'] = $this->config->item('ftp_username');
											$config['ftp_password'] = $this->config->item('ftp_password');
											$config['ftp_port'] 	= $this->config->item('ftp_port');
											$config['debug']    = TRUE;
											$this->ftp->connect($config); 
											$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
											$projects_ftp_dir = $this->config->item('projects_ftp_dir');
											$project_draft_dir = $this->config->item('project_draft_dir');
											$project_awaiting_moderation_dir = $this->config->item('project_awaiting_moderation_dir');
											$profile_folder     = $user[0]->profile_name;
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
											
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir, 0777);// create awaiting_moderation directory in projects folder
											$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_id.DIRECTORY_SEPARATOR , 0777); // create the directory by using  project id
										
											foreach($project_attachment_data as $attachment_key => $attachment_value){
											
												if(!empty($attachment_value['draft_project_attachment_name'])){
													$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
													$file_size = $this->ftp->get_filesize($source_path);
													if($file_size != '-1'){
														$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
														$this->ftp->move($source_path, $destination_path);
														$this->db->insert ('awaiting_moderation_projects_attachments', array('project_id'=>$project_id,'awaiting_moderation_project_attachment_name'=> $attachment_value['draft_project_attachment_name']));
													}
												}
											}
											$this->db->delete('draft_projects_attachments', array('project_id' => $project_id));
											//$draft_project_attachment_list = $this->ftp->list_files($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);
											if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id))){
												$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);// delete project directory 

											}
											$this->ftp->close();
										}
									}
									
									########## Delete the record  from fixed budget draft table 
									$this->db->delete('draft_projects_tags', array('project_id' => $project_id));
									$this->db->delete('draft_projects_categories_listing_tracking', array('project_id' => $project_id));
									$this->db->delete('projects_draft', array('project_id' => $project_id));
									
			
									$msg['status'] = 'SUCCESS';
									$msg['message'] = '';
									echo json_encode ($msg);
							
								} else if($page_type == 'form') { // this block will execute when user click "publish project" button on edit draft page
									
									$post_data = $this->input->post ();
									$response = $this->Post_project_model->post_project_validation($post_data);
									
									if($response['status'] == 'SUCCESS'){
									
									
										###################### check that user selected category is valid or not(admin deactive/delete the category)
										$check_project_parent_category_status = false;
										if($this->input->post('project_category')){
											foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
												if(isset($project_category_value['project_parent_category']) && !empty($project_category_value['project_parent_category'])){
													$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
													if($check_project_parent_category_exist){
														$check_project_parent_category_status = true;
														break;
													}
												}
											}
										
										}
										if(!$check_project_parent_category_status){
											$res = [
												'status' => 400,
												'error' => $this->config->item('post_project_valid_category_not_existent_popup_message'),
												'location'=>''
											];
											echo json_encode($res);
											die;
										}
									
										$project_locality_id = 0;$project_county_id = 0;$escrow_payment_method = 'N';
										$offline_payment_method = 'N';$upgrade_type_featured = 'N';$upgrade_type_urgent = 'N';
										$upgrade_type_sealed = 'N';$upgrade_type_hidden = 'N';$min_budget = 0;$max_budget = 0;$not_sure_dropdown_option_selected = 'N';
										$confidential_dropdown_option_selected = 'N';$postal_code_id = 0;
										$confidential_after_expiration_selected = 'N';$project_type = "fixed";
										if($this->input->post('escrow_payment_method') == 'Y'){
											$escrow_payment_method = $this->input->post('escrow_payment_method');
											}
										
										if($this->input->post ('offline_payment_method') == 'Y'){
											$offline_payment_method = $this->input->post ('offline_payment_method');
										}
										if(!empty($this->input->post ('project_budget'))){
											$project_budget = $this->input->post ('project_budget');
											if($project_budget == 'confidential_dropdown_option_selected'){
												$confidential_dropdown_option_selected = 'Y';
											}else if($project_budget == 'not_sure_dropdown_option_selected'){
												$not_sure_dropdown_option_selected = 'Y';
											}else{
												$project_budget_array = explode("_",$this->input->post ('project_budget'));
												$min_budget = $project_budget_array[0];
												$max_budget = $project_budget_array[1]; 
											}
										}
										if(!empty($this->input->post ('location_option'))){
											if(!empty($this->input->post ('project_locality_id'))){
											$project_locality_id = $this->input->post ('project_locality_id');
											}if(!empty($this->input->post ('project_county_id'))){
												$project_county_id = $this->input->post ('project_county_id');
											}
										
										}
										if(!empty($this->input->post ('upgrade_type_featured'))){
											$upgrade_type_featured = 'Y';
										}
										if(!empty($this->input->post ('upgrade_type_hidden'))){
											$upgrade_type_hidden = 'Y';
										}
										if(!empty($this->input->post ('upgrade_type_sealed'))){
											$upgrade_type_sealed = 'Y';
										}
										if(!empty($this->input->post ('upgrade_type_urgent'))){
											$upgrade_type_urgent = 'Y';
										}
										if(!empty($this->input->post ('confidential_after_expiration_selected'))){
											$confidential_after_expiration_selected = 'Y';
										}
										if(!empty($this->input->post('project_county_id')) && !empty($this->input->post ('project_locality_id'))){
										
											$postal_code_id = $this->input->post('project_postal_code_id');
											
										}
										if(!empty($this->input->post ('project_type_main')) && $this->input->post ('project_type_main') == 'post_project'){
											$project_type = $this->input->post('project_type');
											
										
										}else if(!empty($this->input->post ('project_type_main')) && $this->input->post ('project_type_main') == 'post_fulltime_position'){
											$project_type = 'fulltime';
										}
										// move data from project drafts table to fixed_budget_projects_awaiting_moderation table
										/* $this->db->select('*');
										$this->db->from('projects_draft');
										$this->db->where('projects_draft.project_id',$project_id);
										$this->db->where('projects_draft.project_owner_id',$user[0]->user_id);
										$project_result = $this->db->get();
										$project_data = $project_result->result_array();*/
										$awaiting_moderation_project_data = array (
											'project_id'=>$project_id,
											'project_owner_id'=>$user[0]->user_id,
											'project_submission_to_moderation_date'=>date('Y-m-d H:i:s'),
											'project_title'=>$this->input->post('project_title'),
											'project_description'=>$this->input->post('project_description'),
											'locality_id'=>$project_locality_id,
											'county_id'=>$project_county_id,
											'postal_code_id'=>$postal_code_id,
											'project_type'=>$project_type,
											'min_budget'=>$min_budget,
											'max_budget'=>$max_budget,
											'not_sure_dropdown_option_selected'=>$not_sure_dropdown_option_selected,
											'confidential_dropdown_option_selected'=>$confidential_dropdown_option_selected,
											'escrow_payment_method'=>$escrow_payment_method,
											'offline_payment_method'=>$offline_payment_method,
											'confidential_after_expiration_selected'=>$confidential_after_expiration_selected,
											'featured'=>$upgrade_type_featured,
											'urgent'=>$upgrade_type_urgent,
											'sealed'=>$upgrade_type_sealed,
											'hidden'=>$upgrade_type_hidden
										
										); 
										// @sid code to set auto approval date into projects_awaiting_moderation
										$auto_approval_flag = false; // This flag is used to identify where the entry of temporary project table move either [awaiting moderation / open for bidding]
									
										// @sid this code block is used to update user account balance based on project upgrade 
										// check the user account balance,bonus balance,account balance is sufficient for purchase upgrade
										$count_user_featured_membership_included_upgrades_monthly = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user[0]->user_id); // count user membership featured  upgrade
						
										$count_user_urgent_membership_included_upgrades_monthly = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user[0]->user_id);// count user membership urgent upgrade
										$upgraded_project_price = 0;
										if($upgrade_type_featured == 'Y'){
											if($count_user_featured_membership_included_upgrades_monthly >= $user_membership_plan_detail['included_number_featured_upgrades']){
												$upgraded_project_price += $this->config->item('project_upgrade_price_featured');
											}
										}
										if($upgrade_type_urgent == 'Y'){
											if($count_user_urgent_membership_included_upgrades_monthly >= $user_membership_plan_detail['included_number_urgent_upgrades']){
												$upgraded_project_price += $this->config->item('project_upgrade_price_urgent');
											}
										}
										if($upgrade_type_sealed == 'Y'){
											$upgraded_project_price += $this->config->item('project_upgrade_price_sealed');
										}
										if($upgrade_type_hidden == 'Y'){
											$upgraded_project_price += $this->config->item('project_upgrade_price_hidden');
										}
										if(floatval($upgraded_project_price) > 0){
											$total_user_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'] + $user_detail['user_account_balance'];
											if(floatval($upgraded_project_price) > floatval($total_user_balance) ){
											
												$res = array(
														'status' => 400,
														'location' => '',
														'error' => $this->config->item('user_post_upgraded_project_insufficient_funds_error_message') // define in post_project_custom config
													);
												echo json_encode($res);
												die;
											}
										}
										
										if($upgrade_type_featured == 'Y' || $upgrade_type_urgent == 'Y' || $upgrade_type_sealed == 'Y' || $upgrade_type_hidden == 'Y') {
											if($user_detail['current_membership_plan_id'] == 1) {
												$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('free_user_upgraded_project_auto_approval_min'), $this->config->item('free_user_upgraded_project_auto_approval_max'));
												if($auto_approve_date != 0) {
													$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
												} else {
													$auto_approval_flag = true;
												}
											} else {
												$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('paying_subscriber_upgraded_project_auto_approval_min'), $this->config->item('paying_subscriber_upgraded_project_auto_approval_max'));
												if($auto_approve_date != 0) {
													$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
												} else {
													$auto_approval_flag = true;
												}
											}
										} else {
											if($user_detail['current_membership_plan_id'] == 1) {
												$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('free_user_standard_project_auto_approval_min'), $this->config->item('free_user_standard_project_auto_approval_max'));
												if($auto_approve_date != 0) {
													$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
												} else {
													$auto_approval_flag = true;
												}
											} else {
												$auto_approve_date = generate_random_project_autoapproval_time_between_min_max_values($this->config->item('paying_subscriber_standard_project_auto_approval_min'), $this->config->item('paying_subscriber_standard_project_auto_approval_max'));
												if($auto_approve_date != 0) {
													$awaiting_moderation_project_data['auto_approval_date'] = $auto_approve_date;
												} else {
													$auto_approval_flag = true;
												}
											}
										}
										
										if($auto_approval_flag) {
											$time_arr = explode(':', $this->config->item('standard_project_availability'));
											$project_expire_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
											$open_for_bidding_project_data = [
												'project_id'=>$project_id,
												'project_owner_id'=>$user[0]->user_id,
												'project_posting_date' => date('Y-m-d H:i:s'),
												'project_expiration_date' => $project_expire_date,
												'project_title'=>$this->input->post('project_title'),
												'project_description'=>$this->input->post('project_description'),
												'locality_id'=>$project_locality_id,
												'county_id'=>$project_county_id,
												'postal_code_id' => $postal_code_id,
												'project_type' => $this->input->post('project_type'),
												'min_budget'=>$min_budget,
												'max_budget'=>$max_budget,
												'not_sure_dropdown_option_selected'=>$not_sure_dropdown_option_selected,
												'confidential_dropdown_option_selected'=>$confidential_dropdown_option_selected,
												'featured'=>$upgrade_type_featured,
												'urgent'=>$upgrade_type_urgent,
												'sealed'=>$upgrade_type_sealed,
												'hidden'=>$upgrade_type_hidden,
												'confidential_after_expiration_selected'=>$confidential_after_expiration_selected,
												'views' => 0,
												'revisions'=> 0
											];
											$this->db->insert ('projects_open_bidding', $open_for_bidding_project_data); // save data in fixed_budget_projects_open_bidding table from post project form
											
											// insert the data regrading purchase tracking and refresh sequence and manage the user bonus balance and account balance
											$this->Post_project_model->user_project_upgrade_purchase_refresh_sequence_tracking_save($open_for_bidding_project_data,$user[0]->user_id); 
											
											
											// move data from draft_projects_tags table to awaiting_moderation_projects_tags table
											/* $this->db->select('draft_project_tag_name');
											$this->db->from('draft_projects_tags');
											$this->db->where('project_id',$project_id);
											$this->db->order_by('id',"asc");
											$project_tag_result = $this->db->get();
											$project_tag_data = $project_tag_result->result_array();
											if(!empty($project_tag_data)){
												foreach($project_tag_data as $tag_key => $tag_value){
													$this->db->insert ('projects_tags', array('project_id'=>$project_id,'project_tag_name'=> $tag_value['draft_project_tag_name']));
												}
											
											} */
											
											if(!empty($this->input->post('project_tag'))){
												foreach($this->input->post('project_tag') as $project_tag_key){
													if(!empty($project_tag_key['tag_name'])){
														$this->db->insert ('projects_tags', array('project_id' => $project_id,
														'project_tag_name' => $project_tag_key['tag_name']));
														// save data in projects_tags table from post project form
													}
												}	
											}
											
											
											
											foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
												$project_category_id = 0;
												$project_parent_category_id = 0;
											
												if(!empty($project_category_value['project_parent_category'])){
													if(isset($project_category_value['project_child_category']) && !empty($project_category_value['project_child_category']))
													{
														$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);

														$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($project_category_value['project_parent_category'],$project_category_value['project_child_category']);	
														if($check_project_parent_category_exist){
															if($check_project_child_category_exist){
																
																$project_category_id = $project_category_value['project_child_category'];
																$project_parent_category_id = $project_category_value['project_parent_category'];
															
															}else{
															
																$project_category_id =  $project_category_value['project_parent_category'];
																$project_parent_category_id = 0;
																
															}
														}
													}else{
														
														$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
														if($check_project_parent_category_exist){
														
															$project_category_id =  $project_category_value['project_parent_category'];
															$project_parent_category_id = 0;
														
														}
													}
												}
												if(!empty($project_category_id) || !empty($project_parent_category_id)){
													$this->db->insert ('projects_categories_listing_tracking', array(
														'project_id' => $project_id,
														'project_category_id' => $project_category_id,
														'project_parent_category_id' => $project_parent_category_id 
													));
													
												}
												// save data in projects_categories_listing_tracking table from post project form
												
											}
											
											
											// move data from draft_projects_attachments table to awaiting_moderation_projects_attachments table
											$this->db->select('draft_project_attachment_name');
											$this->db->from('draft_projects_attachments');
											$this->db->where('project_id',$project_id);
											$this->db->order_by('id',"asc");
											$project_attachment_result = $this->db->get();
											$project_attachment_data = $project_attachment_result->result_array();
											if(!empty($project_attachment_data)){
												
												$this->load->library('ftp');
												$config['ftp_hostname'] = $this->config->item('ftp_hostname');
												$config['ftp_username'] = $this->config->item('ftp_username');
												$config['ftp_password'] = $this->config->item('ftp_password');
												$config['ftp_port'] 	= $this->config->item('ftp_port');
												$config['debug']    = TRUE;
												$this->ftp->connect($config); 
												$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
												$projects_ftp_dir = $this->config->item('projects_ftp_dir');
												$project_draft_dir = $this->config->item('project_draft_dir');
												$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
												$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
												$profile_folder     = $user[0]->profile_name;
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
												
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir, 0777);// create awaiting_moderation directory in projects folder
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id , 0777); // create the directory by using  project id
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir , 0777); // create the owner attachment directory by using  project id
											
												foreach($project_attachment_data as $attachment_key => $attachment_value){
												
													if(!empty($attachment_value['draft_project_attachment_name'])){
														$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.$project_owner_attachments_dir.$attachment_value['draft_project_attachment_name'];
														$file_size = $this->ftp->get_filesize($source_path);
														if($file_size != '-1'){
															$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
															$this->ftp->move($source_path, $destination_path);
															$this->db->insert ('projects_attachments', array('project_id'=>$project_id,'project_attachment_name'=> $attachment_value['draft_project_attachment_name']));
														}
													}
												}
												$this->db->delete('draft_projects_attachments', array('project_id' => $project_id));
												$draft_project_attachment_list = $this->ftp->list_files($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);

												if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id))){
													$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR);// delete draft project directory 
												}
												$this->ftp->close();
											}
											// trigger socket event to update latest project section on user dashboard
											$url = SOCKET_URL."/updateLatestProjectOnUserDashboard/".$user[0]->user_id.'?authorization_key='.$this->config->item('url_authrization_key');
											$options = array(
												CURLOPT_RETURNTRANSFER => true,
												CURLOPT_SSL_VERIFYPEER => false
											);
											try {
												$ch = curl_init( $url );
												curl_setopt_array( $ch, $options );
												curl_exec( $ch );
												curl_close( $ch );
											} catch(Exception $e) {
											}		
											
											
											if($upgrade_type_featured == 'N' && $upgrade_type_urgent == 'N' && $upgrade_type_sealed == 'N' && $upgrade_type_hidden == 'N' ) {
												// insert into projects_refresh_sequence_tracking table
												$time_arr = explode(':', $this->config->item('standard_project_refresh_sequence'));
												$refresh_sequence_data = [
													'project_id' => $project_id,
													'project_upgrade_purchase_tracking_id' => null,
													'project_last_refresh_time' => null,
													'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
												];
												$this->db->insert('projects_refresh_sequence_tracking', $refresh_sequence_data);
											}
											
										} else {
											$this->db->insert ('projects_awaiting_moderation', $awaiting_moderation_project_data);
											// move data from draft_projects_tags table to awaiting_moderation_projects_tags table
											
											// insert the data regrading purchase tracking and manage the user bonus balance and account balance
											$this->Post_project_model->user_project_upgrade_purchase_tracking_save($awaiting_moderation_project_data,$user[0]->user_id); 
											/* $this->db->select('draft_project_tag_name');
											$this->db->from('draft_projects_tags');
											$this->db->where('project_id',$project_id);
											$this->db->order_by('id',"asc");
											$project_tag_result = $this->db->get();
											$project_tag_data = $project_tag_result->result_array();
											if(!empty($project_tag_data)){
												foreach($project_tag_data as $tag_key => $tag_value){
													$this->db->insert ('awaiting_moderation_projects_tags', array('project_id'=>$project_id,'awaiting_moderation_project_tag_name'=> $tag_value['draft_project_tag_name']));
												}
											
											} */
											
											if(!empty($this->input->post('project_tag'))){
												foreach($this->input->post('project_tag') as $project_tag_key){
													if(!empty($project_tag_key['tag_name'])){
														$this->db->insert ('awaiting_moderation_projects_tags', array('project_id' => $project_id,
														'awaiting_moderation_project_tag_name' => $project_tag_key['tag_name']));
														// save data in awaiting_moderation_project_tag_name table from post project form
													}
												}	
											}
											
											
											
											foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
												$project_category_id = 0;
												$project_parent_category_id = 0;
												if(!empty($project_category_value['project_parent_category'])){
													if(isset($project_category_value['project_child_category']) && !empty($project_category_value['project_child_category']))
													{
													
														$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);

														$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($project_category_value['project_parent_category'],$project_category_value['project_child_category']);
														if($check_project_parent_category_exist){
															if($check_project_child_category_exist){
																$project_category_id = $project_category_value['project_child_category'];
																$project_parent_category_id = $project_category_value['project_parent_category'];
															
															}else{
															
																$project_category_id =  $project_category_value['project_parent_category'];
																$project_parent_category_id = 0;
																
															}
														}
													}else{
													
														$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
														if($check_project_parent_category_exist){
															
															$project_category_id =  $project_category_value['project_parent_category'];
															$project_parent_category_id = 0;
														
														}
													}
													
													if(!empty($project_category_id) || !empty($project_parent_category_id)){
														$this->db->insert ('awaiting_moderation_projects_categories_listing_tracking', array(
															'project_id' => $project_id,
															'awaiting_moderation_project_category_id' => $project_category_id,
															'awaiting_moderation_project_parent_category_id' => $project_parent_category_id
														));
													}
													// save data in awaiting_moderation_projects_categories_listing_tracking table from post project form
												}	
											}
											$this->db->select('draft_project_attachment_name');
											$this->db->from('draft_projects_attachments');
											$this->db->where('project_id',$project_id);
											$this->db->order_by('id',"asc");
											$project_attachment_result = $this->db->get();
											$project_attachment_data = $project_attachment_result->result_array();
											if(!empty($project_attachment_data)){
												
												$this->load->library('ftp');
												$config['ftp_hostname'] = $this->config->item('ftp_hostname');
												$config['ftp_username'] = $this->config->item('ftp_username');
												$config['ftp_password'] = $this->config->item('ftp_password');
												$config['ftp_port'] 	= $this->config->item('ftp_port');
												$config['debug']    = TRUE;
												$this->ftp->connect($config); 
												$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
												$projects_ftp_dir = $this->config->item('projects_ftp_dir');
												$project_draft_dir = $this->config->item('project_draft_dir');
												$project_awaiting_moderation_dir = $this->config->item('project_awaiting_moderation_dir');
												$profile_folder     = $user[0]->profile_name;
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
												
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir, 0777);// create awaiting_moderation directory in projects folder
												$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_id.DIRECTORY_SEPARATOR , 0777); // create the directory by using  project id
											
												foreach($project_attachment_data as $attachment_key => $attachment_value){
												
													if(!empty($attachment_value['draft_project_attachment_name'])){
														$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
														$file_size = $this->ftp->get_filesize($source_path);
														if($file_size != '-1'){
															$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_value['draft_project_attachment_name'];
															$this->ftp->move($source_path, $destination_path);
															$this->db->insert ('awaiting_moderation_projects_attachments', array('project_id'=>$project_id,'awaiting_moderation_project_attachment_name'=> $attachment_value['draft_project_attachment_name']));
														}
													}
												}
												$this->db->delete('draft_projects_attachments', array('project_id' => $project_id));
												//$draft_project_attachment_list = $this->ftp->list_files($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);
												if(!empty($this->ftp->check_ftp_directory_exist($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id))){
													$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);// delete project directory 

												}
												$this->ftp->close();
											}
										}
											
										########## Delete the record  from fixed budget draft table 
										$this->db->delete('draft_projects_tags', array('project_id' => $project_id));
										$this->db->delete('draft_projects_categories_listing_tracking', array('project_id' => $project_id));
										$this->db->delete('projects_draft', array('project_id' => $project_id));
										$msg['status'] = 'SUCCESS';
										$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
										$msg['message'] = '';
										echo json_encode ($msg);
									}else{
										echo json_encode ($response);
									}
								
								} else {
									$msg['status'] = 404;
									$msg['message'] = '';
									$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
									echo json_encode ($msg);
								}
									
							} else {
								
								$msg['status'] = 404;
								$msg['message'] = '';
								$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
								echo json_encode ($msg);
							}
						}
					} else {
						$msg['status'] = 404;
						$msg['message'] = '';
						$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
						echo json_encode ($msg);
					}
					
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
				}	
			}else{
			
				show_404();
			
			}
		}else{
		
			show_404();
		}
	}
	
	/**
	 * This function is used for load the edit view of project.
	*/
	
	public function edit_project(){
		if(check_session_validity()){ 
		
			if(empty($this->input->get('id'))){
				show_404();
			}
			$user = $this->session->userdata('user');
			$check_project_exists = $this->db // count the project from projects_open_bidding table
				->select ('id')
				->from ('projects_open_bidding')
				->where('project_id',$this->input->get('id'))
				->where('project_owner_id', $user[0]->user_id)
				->get ()->num_rows ();
			
			if($check_project_exists == 0){
				redirect (VPATH . $this->config->item('dashboard_page_url'));
			}
			$project_id = $this->input->get('id');
			$data['project_parent_categories'] = $this->Post_project_model->get_project_parent_categories();
			
		
			
			$this->Projects_model->check_update_invalid_combination_project_location('projects_open_bidding','project_id',$project_id); // check valid combination of locality_id,county_id,postal_code_id If the combination is not valid it will update locality_id,county_id,postal_code_id  to 0.
			
			
			
			$this->db->select('projects_open_bidding.*,projects_additional_information.additional_information,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
			$this->db->from('projects_open_bidding');
			$this->db->join('projects_additional_information', 'projects_additional_information.project_id = projects_open_bidding.project_id', 'left');
			/* $this->db->where('projects_open_bidding.project_id',$project_id);
			$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id); */
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
			$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			$this->db->where('projects_open_bidding.project_id',$project_id);
			$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
			$project_result = $this->db->get();
			$project_data = $project_result->result_array();
			
			/* echo "<pre>";
			print_r($project_data);
			die; */
			$data['project_data'] = $project_data;
			########## fetch the categories of draft project and make the dynamic array start ###
			$this->db->select('*');
			$this->db->from('projects_categories_listing_tracking');
			$this->db->where('project_id',$project_id);
			$this->db->order_by('id',"asc");
			$project_category_result = $this->db->get();
			$project_category_data = $project_category_result->result_array();
			$data['project_category_data'] = $project_category_data;
			########## fetch the  project attachments ###
			$data['project_attachment_array'] = $this->get_project_attachments($project_id,$user[0]->profile_name,'open_for_bidding');
			########## fetch the tags of draft project ###
			$this->db->select('*');
			$this->db->from('projects_tags');
			$this->db->where('project_id',$project_id);
			$this->db->order_by('id',"asc");
			$project_tag_result = $this->db->get();
			$project_tag_data = $project_tag_result->result_array();
			
			$data['project_id'] = $project_id;
			$data['project_tag_data'] = $project_tag_data;
			$data['fixed_budget_projects_budget_range'] = $this->Post_project_model->get_fixed_budget_projects_budget_range();// drop down options for fixed budget project budget range
			
			$data['hourly_rate_based_budget_projects_budget_range'] = $this->Post_project_model->get_hourly_rate_based_projects_budget_range();// drop down options for hourly rate based project budget range
			
			$data['fulltime_project_salary_range'] = $this->Post_project_model->get_fulltime_projects_salaries_range();// drop down options for fulltime project salary range
			
			$data['counties'] = $this->dashboard_model->get_counties(); // drop down options of counties
			$data['localities'] = $this->dashboard_model->get_localities_selected_county($project_data[0]['county_id']);// drop down options of localities
			$data['postal_codes'] = $this->Post_project_model->get_project_post_codes($project_data[0]['locality_id']);// drop down options of localities
			
			################## get the user_details #################
				$data['user_details'] = $this->db->get_where('user_details', array('user_id' => $user[0]->user_id))->row();
			###############################################
			
			$count_project_categories = $this->db // count the number of categories of draft project
				->select ('id')
				->from ('projects_categories_listing_tracking')
				->where('project_id',$project_id)
				->get ()->num_rows ();
				
			$count_project_attachments = $this->db // count the number of attachment of draft project
				->select ('id')
				->from ('projects_attachments')
				->where('project_id',$project_id)
				->get ()->num_rows ();
				
			$count_project_tags = $this->db // count the number of tags of draft project
				->select ('id')
				->from ('projects_tags')
				->where('project_id',$project_id)
				->get ()->num_rows ();	
				
			$count_project_postal_codes = $this->db
			->select ('id')
			->from ('postal_codes')
			->where ('locality_id', $project_data[0]['locality_id'])
			->get ()->num_rows ();

			$data['count_project_categories'] = $count_project_categories;
			$data['count_project_attachments'] = $count_project_attachments;
			$data['count_project_tags'] = $count_project_tags;
			$data['count_project_postal_codes'] = $count_project_postal_codes;
			$data['project_id'] = $project_id;
			$data['current_page'] = 'edit_project';
			
			
			$edit_project_page_title_meta_tag = strip_tags($project_data[0]['project_title']);
			$edit_project_page_title_meta_tag = substr($edit_project_page_title_meta_tag,0,$this->config->item('project_title_meta_tag_character_limit'))." | ".SITE_TITLE_META_TAG;
			$edit_project_page_description_meta_tag = strip_tags($project_data[0]['project_description']);
			$edit_project_page_description_meta_tag = substr($edit_project_page_description_meta_tag,0,$this->config->item('project_description_meta_description_character_limit'));
			
			$data['meta_tag'] = '<title>' . $edit_project_page_title_meta_tag . '</title><meta name="description" content="' . $edit_project_page_description_meta_tag . '"/>';
			$this->layout->view('edit_project', '', $data, 'normal');
		}else{
			redirect (VPATH);
		
		}
		}
	
	/**
	This function is used for update the project into the database.
	*/
	public  function update_project(){
		if($this->input->is_ajax_request ()){
			if ($this->input->post ())
			{
				if(check_session_validity()){ 
					$i = 0;
					$project_id = $this->input->post ('project_id');
					$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_type');
					$this->db->from('projects_open_bidding');
					
					$this->db->where('project_id',$project_id);
					$project_result = $this->db->get();
					$project_data = $project_result->result_array();

					
					if(!$this->input->post('project_category')){
		
						$msg['status'] = 'FAILED';
						$msg['errors'][$i]['id'] = 'project_parent_category_0';
						$msg['errors'][$i]['message'] = $this->config->item('parent_category_validation_post_project_message');
						$msg['errors'][$i]['error_class'] = 'required';
						$i ++;
					
					}else{
						$project_category = $this->input->post('project_category');
						if(array_key_exists(500,$project_category ) && empty($project_category[500]['project_parent_category'])){
							$msg['status'] = 'FAILED';
							$msg['errors'][$i]['id'] = 'project_parent_category_0';
							$msg['errors'][$i]['message'] = $this->config->item('parent_category_validation_post_project_message');
							$msg['errors'][$i]['error_class'] = 'required';
							$i ++;
						
						}
					
					}
					if(!empty(trim($this->input->post ('project_additional_information'))) && strlen($this->input->post ('project_additional_information')) < $this->config->item('project_additional_information_minimum_length_character_limit_post_project')){
						
						$msg['status'] = 'FAILED';
						$msg['errors'][$i]['id'] = 'project_additional_information';
						if($project_data[0]['project_type'] == 'fulltime'){
							$msg['errors'][$i]['message'] = $this->config->item('fulltime_position_additional_information_characters_min_length_validation_post_project_message');
						}else{
							$msg['errors'][$i]['message'] = $this->config->item('project_additional_information_characters_min_length_validation_message');
						}
						$msg['errors'][$i]['error_class'] = 'min_length';
						$i ++;
					}if(!empty($this->input->post ('location_option'))){
					
					
						if(empty($this->input->post ('project_county_id'))){
							$msg['status'] = 'FAILED';
							$msg['errors'][$i]['id'] = 'project_county_id';
							$msg['errors'][$i]['message'] = $this->config->item('project_county_validation_post_project_message');
								$msg['errors'][$i]['error_class'] = 'required';
							$i ++;
						}
						if(!empty($this->input->post('project_county_id')) && empty($this->input->post ('project_locality_id'))){
							$msg['status'] = 'FAILED';
							$msg['errors'][$i]['id'] = 'project_locality_id';
							$msg['errors'][$i]['message'] = $this->config->item('project_locality_validation_post_project_message');
							$msg['errors'][$i]['error_class'] = 'required';
								$i ++;
						}
						if(!empty($this->input->post('project_county_id')) && !empty($this->input->post ('project_locality_id')) && empty($this->input->post ('project_postal_code_id'))){
							if(!empty($this->get_project_post_codes($this->input->post ('project_locality_id')))){
								$msg['status'] = 'FAILED';
								$msg['errors'][$i]['id'] = 'project_postal_code_id';
								$msg['errors'][$i]['message'] = $this->config->item('project_postal_code_validation_post_project_message');
								$msg['errors'][$i]['error_class'] = 'required';
								$i ++;
							}
							
						}
						
					}
					if($i == 0){
						
						
						$user = $this->session->userdata('user');
						$count_project = $this->db // count the number of record in projects_open_bidding table
						->select ('id')
						->from ('projects_open_bidding')
						->where('project_id',$project_id)
						->where('project_owner_id',$user[0]->user_id)
						->get ()->num_rows ();
						if($count_project > 0){
						
						
						###################### check that user selected category is valid or not(admin deactive/delete the category)
						$check_project_parent_category_status = false;
						if($this->input->post('project_category')){
							foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
								if(isset($project_category_value['project_parent_category']) && !empty($project_category_value['project_parent_category'])){
									$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
									if($check_project_parent_category_exist){
										$check_project_parent_category_status = true;
										break;
									}
								}
							}
						
						}
						if(!$check_project_parent_category_status){
							$res = [
								'status' => 400,
								'error' => $this->config->item('post_project_valid_category_not_existent_popup_message'),
								'location'=>''
							];
							echo json_encode($res);
							die;
						}
						
							$upgraded_project_price = 0;
							if($this->input->post ('upgrade_type_featured')){
								$upgraded_project_price += $this->config->item('project_upgrade_price_featured');
							}
							if($this->input->post ('upgrade_type_urgent')){
								$upgraded_project_price += $this->config->item('project_upgrade_price_urgent');
							}
							if(floatval($upgraded_project_price) > 0){
								$user_detail = $this->db->get_where('user_details', ['user_id' => $user[0]->user_id])->row_array();
								$total_user_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'] + $user_detail['user_account_balance'];
								if(floatval($upgraded_project_price) > floatval($total_user_balance) ){
								
									$res = array(
											'status' => 400,
											'location'=>'',
											'error' => $this->config->item('user_post_upgraded_project_insufficient_funds_error_message') // define in post_project_custom config
										);
									echo json_encode($res);
									die;
								}
							}
							
							
							$project_locality_id = 0;$project_county_id = 0;
							$postal_code_id = 0;
							
							$this->db->select('projects_open_bidding.*,projects_additional_information.additional_information,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date');
							$this->db->from('projects_open_bidding');
							$this->db->join('projects_additional_information', 'projects_additional_information.project_id = projects_open_bidding.project_id', 'left');
							
							$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
							$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
							$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
							$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
							$this->db->where('projects_open_bidding.project_id',$project_id);
							$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
							$project_result = $this->db->get();
							$project_data = $project_result->result_array();
							
							
							$featured_upgrade_end_date_timestamp = $project_data[0]['featured_upgrade_end_date'] != NULL ? strtotime ($project_data[0]['featured_upgrade_end_date']) : 0; // latest featured upgrade date
							$bounus_featured_upgrade_end_date_timestamp = $project_data[0]['bounus_featured_upgrade_end_date'] != NULL ? strtotime ($project_data[0]['bounus_featured_upgrade_end_date']) : 0; // latest featured upgrade date
							
							$urgent_upgrade_end_date_timestamp = $project_data[0]['urgent_upgrade_end_date'] != NULL ? strtotime ($project_data[0]['urgent_upgrade_end_date']) : 0;
							$bonus_urgent_upgrade_end_date_timestamp = $project_data[0]['bonus_urgent_upgrade_end_date'] != NULL ? strtotime ($project_data[0]['bonus_urgent_upgrade_end_date']) : 0;
							$featured_upgrade_end_date_max = 0;
							$urgent_upgrade_end_date_max = 0;
							$user_selected_upgrades = array();
							$user_selected_upgrades['project_id'] = $project_id;
							$expiration_upgrade_date_array[] = $project_data[0]['project_expiration_date'];
							
							
							if($this->input->post ('upgrade_type_featured')){
							
								$time_arr = explode(':', $this->config->item('project_upgrade_availability_featured'));
								if(empty($featured_upgrade_end_date_timestamp) && empty($bounus_featured_upgrade_end_date_timestamp)){
										$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
								}else{
									
									if($featured_upgrade_end_date_timestamp >= $bounus_featured_upgrade_end_date_timestamp && $featured_upgrade_end_date_timestamp != 0){
										$featured_upgrade_end_date_max = $project_data[0]['featured_upgrade_end_date'];
									
									}elseif($bounus_featured_upgrade_end_date_timestamp >= $featured_upgrade_end_date_timestamp && $bounus_featured_upgrade_end_date_timestamp !=0){
										$featured_upgrade_end_date_max = $project_data[0]['bounus_featured_upgrade_end_date'];
									}
									$upgrade_end_date = date('Y-m-d H:i:s',strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',strtotime($featured_upgrade_end_date_max)));
									
								}
								$this->db->update('projects_open_bidding', ['featured' => 'Y'], ['project_id' => $project_id]);
								$expiration_upgrade_date_array[] = $upgrade_end_date;
								$user_selected_upgrades['featured'] = 'Y';
								$user_selected_upgrades['featured_upgrade_end_date'] = $upgrade_end_date;
							
							}
							//if($count_featured_upgrade_project_row == 0 && $this->input->post ('upgrade_type_urgent')){
							if($this->input->post ('upgrade_type_urgent')){
								
								$time_arr = explode(':', $this->config->item('project_upgrade_availability_urgent'));
								if(empty($urgent_upgrade_end_date_timestamp) && empty($bounus_urgent_upgrade_end_date_timestamp)){
										$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
								}else{
									
									if($urgent_upgrade_end_date_timestamp >= $bounus_urgent_upgrade_end_date_timestamp && $urgent_upgrade_end_date_timestamp != 0){
										$urgent_upgrade_end_date_max = $project_data[0]['urgent_upgrade_end_date'];
									
									}elseif($bounus_urgent_upgrade_end_date_timestamp >= $urgent_upgrade_end_date_timestamp && $bounus_urgent_upgrade_end_date_timestamp !=0){
										$urgent_upgrade_end_date_max = $project_data[0]['bounus_urgent_upgrade_end_date'];
									}
									$upgrade_end_date = date('Y-m-d H:i:s',strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',strtotime($urgent_upgrade_end_date_max)));
									
								}
								$expiration_upgrade_date_array[] = $upgrade_end_date;
								$this->db->update('projects_open_bidding', ['urgent' => 'Y'], ['project_id' => $project_id]);
								$user_selected_upgrades['urgent'] = 'Y';
								$user_selected_upgrades['urgent_upgrade_end_date'] = $upgrade_end_date;
							}
							if($this->input->post ('upgrade_type_featured') || $this->input->post ('upgrade_type_urgent') && !empty($expiration_upgrade_date_array)){
						
								$max = max(array_map('strtotime', $expiration_upgrade_date_array));
								$this->db->update('projects_open_bidding', ['project_expiration_date'=>date('Y-m-d H:i:s', $max)], ['project_id' => $project_id]);
								
								$this->Projects_model->user_project_upgrade_purchase_refresh_sequence_tracking_membership_exclude_save($user_selected_upgrades,$user[0]->user_id);// track purchasing and refresh sequence;
								
							}
							
							if($this->input->post('escrow_payment_method') == 'Y'){
								$escrow_payment_method = $this->input->post('escrow_payment_method');
							}
							
							if($this->input->post ('offline_payment_method') == 'Y'){
								$offline_payment_method = $this->input->post ('offline_payment_method');
							}
							if(!empty($this->input->post ('location_option'))){
								if(!empty($this->input->post ('project_locality_id'))){
								$project_locality_id = $this->input->post ('project_locality_id');
								}if(!empty($this->input->post ('project_county_id'))){
									$project_county_id = $this->input->post ('project_county_id');
								}
							
							}
							if(!empty($this->input->post('project_county_id')) && !empty($this->input->post ('project_locality_id'))){
							
								$postal_code_id = $this->input->post('project_postal_code_id');
								
							}
							$open_for_bidding_project_data = array (
								
								'locality_id'=>$project_locality_id,
								'county_id'=>$project_county_id,
								'postal_code_id'=>$postal_code_id,
								'escrow_payment_method'=>$escrow_payment_method,
								'offline_payment_method'=>$offline_payment_method
							);
							$this->db->where ('project_id', $project_id);
							$this->db->update ('projects_open_bidding', $open_for_bidding_project_data); // save data in projects_draft table from edit draft form
							
							$this->db->delete('projects_tags', array('project_id' => $project_id));
							$this->db->delete('projects_categories_listing_tracking', array('project_id' => $project_id));
							
							
							/* foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
								if(!empty($project_category_value['project_parent_category'])){
									if(isset($project_category_value['project_child_category']) && !empty($project_category_value['project_child_category']))
									{
										$project_category_data = array(
											'project_id' => $project_id,
											'project_category_id' => $project_category_value['project_child_category'],
											'project_parent_category_id' => $project_category_value['project_parent_category']
										);
									}else{
										$project_category_data = array(
											'project_id' => $project_id,
											'project_category_id' => $project_category_value['project_parent_category'],
											'project_parent_category_id' => 0
										);
									}
									$this->db->insert ('projects_categories_listing_tracking', $project_category_data);
									// save data in draft_projects_categories_listing_tracking table from post project form
								}	
							} */
							
							foreach($this->input->post('project_category') as $project_category_key=>$project_category_value){
							
								$project_category_id = 0;
								$project_parent_category_id = 0;
								if(!empty($project_category_value['project_parent_category'])){
									if(isset($project_category_value['project_child_category']) && !empty($project_category_value['project_child_category']))
									{
									
										$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);

										$check_project_child_category_exist = $this->Post_project_model->check_project_child_category_exist($project_category_value['project_parent_category'],$project_category_value['project_child_category']);	
										
										if($check_project_parent_category_exist){
											if($check_project_child_category_exist){
												
												$project_category_id = $project_category_value['project_child_category'];
												$project_parent_category_id = $project_category_value['project_parent_category'];
											
											}else{
											
												$project_category_id =  $project_category_value['project_parent_category'];
												$project_parent_category_id = 0;
												
											}
										}
								
									
									}else{
									
										$check_project_parent_category_exist = $this->Post_project_model->check_project_parent_category_exist($project_category_value['project_parent_category']);
										if($check_project_parent_category_exist){
											
											$project_category_id =  $project_category_value['project_parent_category'];
											$project_parent_category_id = 0;
										
										}
									}
									
								}
								
								if(!empty($project_category_id) || !empty($project_parent_category_id)){
									$this->db->insert ('projects_categories_listing_tracking', array(
										'project_id' => $project_id,
										'project_category_id' => $project_category_id,
										'project_parent_category_id' => $project_parent_category_id 
									));
								}
							}
						  
							if(!empty($this->input->post('project_tag'))){
								foreach($this->input->post('project_tag') as $project_tag_key){
									if(!empty($project_tag_key['tag_name'])){
										$this->db->insert ('projects_tags', array('project_id' => $project_id,
										'project_tag_name' => $project_tag_key['tag_name']));
										// save data in draft_projects_tags table from post project form
									}
								}	
							}
							
							$this->db->select('*');
							$this->db->from('projects_additional_information');
							$this->db->where('project_id',$project_id);
							$project_additional_information_result = $this->db->get();
							$project_additional_information_data = $project_additional_information_result->result_array();
							if(empty($project_additional_information_data)){
							
								$this->db->insert ('projects_additional_information', array('project_id'=>$project_id,
								'additional_information_add_date'=> date('Y-m-d H:i:s'),'additional_information'=>$this->input->post ('project_additional_information')));
							
							}else{
							
								$this->db->where ('project_id', $project_id);
								$this->db->update ('projects_additional_information',array(
								'additional_information_add_date'=> date('Y-m-d H:i:s'),'additional_information'=>$this->input->post ('project_additional_information')));
							}
							$msg['status'] = 'SUCCESS';
							$msg['message'] = '';
							echo json_encode ($msg);
						}else{
							$msg['status'] = '404';
							$msg['message'] = '';
							echo json_encode ($msg);
						}
					}else{
						echo json_encode ($msg);
						die;
					}
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
			
				}		
			}else{
				show_404();
			}
		}else{
		
			show_404();
		}
	
	}
	
	
	/**
	* This function is used for detail page of projects with different different status(awaiting moderation,open for bidding etc).
	*/
    public function project_detail ()
    {
        if(empty($this->input->get('id'))){
            redirect (VPATH . $this->config->item('dashboard_page_url'));
        }
		$user = $this->session->userdata('user');
        $project_id = $this->input->get('id');
        $project_status_table_array = $this->Projects_model->get_project_status_table_name($project_id);
		
        if(!empty($project_status_table_array['table_name'])){
			$data['project_id'] = $project_id;
				
			if($project_status_table_array['table_name'] == 'projects_awaiting_moderation'){
				if(!$this->session->userdata ('user')){
					redirect(VPATH.$this->config->item('signin_page_url'));
				}
				$user = $this->session->userdata ('user');
				$count_awaiting_moderation_project = $this->db 
				->select ('id')
				->from ('projects_awaiting_moderation')
				->where('project_id',$project_id)
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_awaiting_moderation_project == 0){
					redirect (VPATH . $this->config->item('dashboard_page_url'));
				}
				
				$this->Projects_model->check_update_invalid_combination_project_location('projects_awaiting_moderation','project_id',$project_id); // check valid combination of locality_id,county_id,postal_code_id If the combination is not valid it will update locality_id,county_id,postal_code_id  to 0.
				
				// fetch the project infromation from projects_awaiting_moderation table
				$this->db->select('projects_awaiting_moderation.*,users.account_type,users.first_name,users.last_name,users.company_name,users.profile_name,users.account_validation_date,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
				$this->db->from('projects_awaiting_moderation');
				$this->db->join('users', 'users.user_id = projects_awaiting_moderation.project_owner_id', 'left');
				$this->db->join('counties', 'counties.id = projects_awaiting_moderation.county_id', 'left');
				$this->db->join('localities', 'localities.id = projects_awaiting_moderation.locality_id', 'left');
				$this->db->join('postal_codes', 'postal_codes.id = projects_awaiting_moderation.postal_code_id', 'left');
				$this->db->where('projects_awaiting_moderation.project_id',$project_id);
				$this->db->where('projects_awaiting_moderation.project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
				$project_data = $project_result->result_array();

			}else if($project_status_table_array['table_name'] == 'projects_open_bidding'){
				
				$count_open_for_bidding_project = $this->db 
				->select ('id')
				->from ('projects_open_bidding')
				->where('project_id',$project_id)
				->get ()->num_rows ();
				if($count_open_for_bidding_project == 0){
					redirect (VPATH . $this->config->item('dashboard_page_url'));
				}
				$this->Projects_model->check_update_invalid_combination_project_location('projects_open_bidding','project_id',$project_id); // check valid combination of locality_id,county_id,postal_code_id If the combination is not valid it will update locality_id,county_id,postal_code_id  to 0.
				// fetch the project infromation from projects_open_bidding table
				$this->db->select('projects_open_bidding.*,users.account_type,users.first_name,users.last_name,users.company_name,users.profile_name,users.account_validation_date,counties.name as county_name, localities.name as locality_name, postal_codes.postal_code, projects_additional_information.additional_information_add_date, projects_additional_information.additional_information,fpuucpt.project_cover_picture_name,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
				$this->db->select('users.sync_linkedin, users.sync_facebook');
				$this->db->from('projects_open_bidding');
				$this->db->join('users', 'users.user_id = projects_open_bidding.project_owner_id', 'left');
				$this->db->join('projects_additional_information', 'projects_additional_information.project_id = projects_open_bidding.project_id', 'left');
				$this->db->join('counties', 'counties.id = projects_open_bidding.county_id', 'left');
				$this->db->join('localities', 'localities.id = projects_open_bidding.locality_id', 'left');
				$this->db->join('postal_codes', 'postal_codes.id = projects_open_bidding.postal_code_id', 'left');
				$this->db->join('featured_projects_users_upload_cover_pictures_tracking fpuucpt', 'fpuucpt.project_id = projects_open_bidding.project_id', 'left');
				
				$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_id = "'.$project_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
			
				$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_id = "'.$project_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				
				$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_id = "'.$project_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				
				$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_id = "'.$project_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				
				$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_id = "'.$project_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				
				$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_id = "'.$project_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				$this->db->where('projects_open_bidding.project_id',$project_id);
				$project_result = $this->db->get();
				$project_data = $project_result->result_array();
				$featured_max = 0;
				if(!empty($project_data[0]['featured_upgrade_end_date'])){
				$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
				}
				if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
					$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
					}
				if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
					$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
				}
				if(!empty($expiration_featured_upgrade_date_array)){
					$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
				}
				
				$this->Projects_model->get_featured_project_upgrade_expiration_status($project_data[0]['project_id']);
				
				$data['upgrade_cover_picture_exist_status'] = false;
				if(!empty($project_data[0]['project_cover_picture_name'])){
				
					$this->load->library('ftp');
					$config['ftp_hostname'] = $this->config->item('ftp_hostname');
					$config['ftp_username'] = $this->config->item('ftp_username');
					$config['ftp_password'] = $this->config->item('ftp_password');
					$config['ftp_port'] 	= $this->config->item('ftp_port');
					$config['debug']    = TRUE;
					$this->ftp->connect($config); 
					######## connectivity of remote server end #######
					
					$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
					$projects_ftp_dir = $this->config->item('projects_ftp_dir');
					$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
					$featured_upgrade_cover_picture = $this->config->item('featured_upgrade_cover_picture');
					$profile_folder     = $project_data[0]['profile_name'];
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$featured_upgrade_cover_picture.$project_data[0]['project_cover_picture_name'];
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1'){
						$data['upgrade_cover_picture_exist_status'] = true;
					}
				}
			}else if($project_status_table_array['table_name'] == 'fixed_budget_projects_expired'){
				$count_fixed_budget_expired_project = $this->db 
				->select ('id')
				->from ('fixed_budget_projects_expired')
				->where('project_id',$project_id)
				->get ()->num_rows ();
				if($count_fixed_budget_expired_project == 0){
						redirect (VPATH . $this->config->item('dashboard_page_url'));
				}
				// fetch the project infromation from projects_open_bidding table
				$this->db->select('fixed_budget_projects_expired.project_id,users.account_type,users.first_name,users.last_name,users.company_name,users.profile_name,users.account_validation_date');
				$this->db->from('fixed_budget_projects_expired');
				$this->db->join('users', 'users.user_id = fixed_budget_projects_expired.project_owner_id', 'left');
				$this->db->where('fixed_budget_projects_expired.project_id',$project_id);
				$project_result = $this->db->get();
				$project_data = $project_result->result_array();
				$this->Projects_model->delete_featured_project_upgrade_record_cover_picture($project_data[0]['profile_name'],$project_data[0]['project_id']);
			}
			$data['project_attachment_data'] = $this->get_project_attachments($project_id,$project_data[0]['profile_name'],$project_status_table_array['project_status']);
				
			$data['project_tag_data'] = $this->Projects_model->get_project_tags($project_id,$project_status_table_array['project_status']);
			$data['project_category_data'] = $this->Projects_model->get_project_categories($project_id,$project_status_table_array['project_status']);
			
			

			$data['project_id'] = $project_id;
			$data['project_data'] = $project_data;
			
			######################## meta tag and meta description ##################
			$project_title_meta_tag = strip_tags($project_data[0]['project_title']);
			$project_title_meta_tag = substr($project_title_meta_tag,0,$this->config->item('project_title_meta_tag_character_limit'))." | ".SITE_TITLE_META_TAG;
			$project_description_meta_tag = strip_tags($project_data[0]['project_description']);
			$project_description_meta_tag = substr($project_description_meta_tag,0,$this->config->item('project_description_meta_description_character_limit'));
			$data['meta_tag'] = '<title>' . $project_title_meta_tag . '</title><meta name="description" content="' . $project_description_meta_tag . '"/>';
			$data['current_page'] = 'project_detail';
			
			$_SESSION['share_title_short'] = $project_title_meta_tag;
			$_SESSION['share_description'] = substr(strip_tags($project_data[0]['project_description']), 0, $this->config->item('facebook_and_linkedin_share_project_description_character_limit'))." | ".SITE_TITLE_META_TAG;
			// $_SESSION['share_description'] = $project_description_meta_tag;
			$_SESSION['share_url'] = base_url().$this->config->item('project_detail_page_url').'?id='.$project_data[0]['project_id'];
			
			// load the view according to project status//
			if($project_status_table_array['table_name'] == 'projects_awaiting_moderation'){
				$this->load->view('projects/awaiting_moderation_project_detail', $data);
			}else if($project_status_table_array['table_name'] == 'projects_open_bidding'){
				$this->load->view('projects/project_detail', $data);
			}
			
		}else{
				redirect (VPATH . $this->config->item('dashboard_page_url'));
		}
		
	}
	
	/* this is used to set the redirect for edit draft page*/
	public function set_redirection_edit_draft_project(){
		if(!$this->session->userdata ('user')){
			//redirect(VPATH.$this->config->item('dashboard_page_url'));
			show_404();
		}
		if(empty($this->input->get('id'))){
			//redirect (VPATH . $this->config->item('dashboard_page_url'));
			show_404();
		}
		$project_id = $this->input->get ('id');
		$user = $this->session->userdata('user');
		$check_project_exists = $this->db // count the number of record in temp_projects table
			->select ('id')
			->from ('projects_draft')
			->where('project_id',$this->input->get('id'))
			->where('project_owner_id', $user[0]->user_id)
			->get ()->num_rows ();
		if($check_project_exists == 0){
			redirect (VPATH . $this->config->item('dashboard_page_url'));
		}	
		$this->session->set_userdata ('check_redirection_edit_draft_project', 1); // set redirection for edit draft page so when user refresh the edit draft page he will redirect to dasboard
		$url = VPATH . $this->config->item('edit_draft_project_page_url').'?id='.$project_id ;
		redirect ($url);
	}
	
	/**
		This function is used to delete the draft project from database by login user.
	**/
	public function delete_draft_project(){
		if($this->input->is_ajax_request ()){
			if(check_session_validity()){ // check session exists or not if exist then it will update user session
				if ($this->input->post ()){
				
					$project_id = $this->input->post ('project_id');
					$user = $this->session->userdata ('user');
					
					$count_draft_project = $this->db // count the number of record in projects_draft table
					->select ('id')
					->from ('projects_draft')
					->where('project_id',$project_id)
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
					if($count_draft_project == 0 )
					{
						$msg['status'] = 400;
						$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
						echo json_encode($msg);
						die;
					}
					
					
					
					
					
					
					$count_project_attachments = $this->db // count the number of attachments of draft project
					->select ('id')
					->from ('draft_projects_attachments')
					->where('project_id',$project_id)
					->get ()->num_rows ();
					if($count_project_attachments > 0){
						$this->load->library('ftp');
						$config['ftp_hostname'] = $this->config->item('ftp_hostname');
						$config['ftp_username'] = $this->config->item('ftp_username');
						$config['ftp_password'] = $this->config->item('ftp_password');
						$config['ftp_port'] 	= $this->config->item('ftp_port');
						$config['debug']    = TRUE;
						$this->ftp->connect($config); 
						$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
						$projects_ftp_dir = $this->config->item('projects_ftp_dir');
						$project_draft_dir = $this->config->item('project_draft_dir');
						$profile_folder     = $user[0]->profile_name;
						//$draft_project_attachment_list = $this->ftp->list_files($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id);
						if(!empty($this->ftp->check_ftp_directory_exist(users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id))){
							$this->ftp->delete_dir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id); // delete draft project directory 
						}
						$this->ftp->close();
					}
					$this->db->delete('draft_projects_attachments', array('project_id' => $project_id));
					$this->db->delete('draft_projects_tags', array('project_id' => $project_id));
					$this->db->delete('draft_projects_categories_listing_tracking', array('project_id' => $project_id));
					$this->db->delete('projects_draft', array('project_id' => $project_id));
					$msg['status'] = 'SUCCESS';
					$msg['message'] = '';

				}else{
					show_404();
				}
				echo json_encode ($msg);
				die;
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			
			}
		}else{
			show_404();
		}
	}
	
	
	/**
	This function is used to delete the project category from draft_projects_categories_listing_tracking table.
	*/
	public function delete_draft_project_category ()
    {
		if($this->input->is_ajax_request ()){
			if(empty($this->input->post ('category_project_id'))){
				show_404();
			}
			if(check_session_validity()){ 
				$user = $this->session->userdata ('user');
				$category_project_id = $this->input->post ('category_project_id');
				$remove_category_id = $this->input->post ('remove_category_id');
				$category_project_array  = explode('_',$category_project_id);
				
				$count_draft_project = $this->db // count the number of record in projects_draft table
				->select ('id')
				->from ('projects_draft')
				->where('project_id',$category_project_array[1])
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_draft_project == 0 )
				{
					$msg['status'] = 400;
					$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
					echo json_encode($msg);
					die;
				}
				
				
				$this->db->delete('draft_projects_categories_listing_tracking', array('id' => $category_project_array[0]));
				$count_project_categories = $this->db // count the number of record in temp_projects table
				->select ('id')
				->from ('draft_projects_categories_listing_tracking')
				->where('project_id',$category_project_array[1])
				->get ()->num_rows ();
					$msg['add_category_button_show_status']  = '1';
				if( $count_project_categories >= $this->config->item('number_project_category_post_project')){
					$msg['add_category_button_show_status']  = '0';
				}
				$msg['remove_category_id'] = $remove_category_id;
				$msg['status'] = 'SUCCESS';
				$msg['message'] = '';
				echo json_encode ($msg);
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			
			}
		}else{
			show_404();
		}
    }
	
	/**
	This function is used to delete the project tag from draft_projects_tags table.
	*/
	public function delete_draft_project_tag ()
    {
		if($this->input->is_ajax_request ()){
			if(empty($this->input->post ('project_tag_id'))){
				show_404();
			}
			if(check_session_validity()){ 
				$user = $this->session->userdata ('user');
				$project_tag_id = $this->input->post ('project_tag_id');
				$project_tag_array = explode("_",$project_tag_id);
				$count_draft_project = $this->db // count the number of record in projects_draft table
				->select ('id')
				->from ('projects_draft')
				->where('project_id',$project_tag_array[3])
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_draft_project == 0 )
				{
					$msg['status'] = 400;
					$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
					echo json_encode($msg);
					die;
				}
				$this->db->delete('draft_projects_tags', array('id' => $project_tag_array[2]));
				$msg['status'] = 'SUCCESS';
				$msg['message'] = '';
				echo json_encode ($msg);die;
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
		}else{
			show_404();
		}
    }
	
	/**
		This function is used to upload draft project attachment.
	*/
	public function upload_draft_project_attachment ()
    {
		if($this->input->is_ajax_request ()){
			if(check_session_validity()){
				$user = $this->session->userdata ('user');
				$project_id = $this->uri->segment('3');
				$count_draft_project = $this->db // count the number of record in projects_draft table
				->select ('id')
				->from ('projects_draft')
				->where('project_id',$project_id)
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_draft_project > 0 )
				{
					$no_draft_project_attachment_uploaded_user = $this->db
					->select ('id')
					->from ('draft_projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					
					$this->db->where('project_id', $project_id);
					
					$project_attachment_maximum_size_limit	 = $this->config->item('project_attachment_maximum_size_limit');
					
					$project_attachment_maximum_size_limit = ($project_attachment_maximum_size_limit * 1048576);
					if(!empty($_FILES['file']['tmp_name'])){
						$file_array = $_FILES['file'];
						if($file_array['size'] > $project_attachment_maximum_size_limit){
							$msg['status'] = 'FAILED';
							$msg['message'] = $this->config->item('project_attachment_maximum_size_validation_post_project_message');
							echo json_encode ($msg);die;
						}elseif($no_draft_project_attachment_uploaded_user >= $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
							$msg['status'] = 'FAILED';
							$msg['message'] = 'you are not allowed to upload files';
							echo json_encode ($msg);die;
						
						}else{
							
							######## connectivity of remote server start#########
							$this->load->library('ftp');
							$config['ftp_hostname'] = $this->config->item('ftp_hostname');
							$config['ftp_username'] = $this->config->item('ftp_username');
							$config['ftp_password'] = $this->config->item('ftp_password');
							$config['ftp_port'] 	= $this->config->item('ftp_port');
							$config['debug']    = TRUE;
							$this->ftp->connect($config); 
							######## connectivity of remote server end #######
							
							$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
							$projects_ftp_dir = $this->config->item('projects_ftp_dir');
							$project_draft_dir = $this->config->item('project_draft_dir');
							$profile_folder     = $user[0]->profile_name;
							$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
							
							if(!empty($project_id )){
							
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir, 0777);// create temporary directory in projects folder
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR , 0777); // create the directory by using temporary project id
								$temp 		= 	explode(".", $file_array["name"]);
								$extension 	= 	end($temp);
								$attachment_name 	= 	$this->config->item('project_attachment_name')."_".rand(0,1000).$project_id.'.'.$extension;// name of attachment
								
								if(move_uploaded_file($file_array['tmp_name'],$this->config->item('temp_dir').$attachment_name)){
									
									$source_path = FCPATH .$this->config->item('temp_dir'). $attachment_name;
									
									$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_name;
									
									$this->ftp->upload($source_path,$destination_path , 'auto', 0777); // upload the attachment into temporary folder of projects 
									unlink(FCPATH .$this->config->item('project_draft_dir'). $attachment_name);
									$projects_attachments_data = array('project_id'=>$project_id,'draft_project_attachment_name'=>$attachment_name);
									$this->db->insert ('draft_projects_attachments', $projects_attachments_data);
									$last_insert_id = $this->db->insert_id();
									
									$no_draft_project_attachment_uploaded_user = $this->db
									->select ('id')
									->from ('draft_projects_attachments')
									->where ('project_id', $project_id)
									->get ()->num_rows (); // check the number of attachment of user into database
									
									$msg['status'] = 'OK';
									$msg['message'] = 'uploded';
									$msg['filename'] = $attachment_name;
									$msg['size'] = number_format($file_array['size']/1024). 'KB';
									$msg['id'] = $last_insert_id;
									$msg['encrypt_id'] = Cryptor::doEncrypt($last_insert_id);
									
									$upload_button_status = '0';
									if($no_draft_project_attachment_uploaded_user < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
										$upload_button_status = '1';
									}
									$msg['upload_button_status'] = $upload_button_status;
								}
							}
							$this->ftp->close();
						}	
						
					}else{
						$msg['status'] = 'FAILED';
						$msg['message'] = 'file is empty';
					}
					echo json_encode ($msg);die;	
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
					echo json_encode($msg);
					die;
				}
				
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
			
		}else{
			show_404();
		}
    }
	
	/**
	This function is used to remove the attachment of draft projects.
	*/
	public function delete_draft_project_attachment ()
    {
        if ($this->input->is_ajax_request ())
        {
			if(empty($this->input->post ('project_attachment_id'))){
				show_404();
			}
			if(check_session_validity()){ 
				$project_id = $this->input->post ('project_id');
				$user = $this->session->userdata ('user');
				$count_draft_project = $this->db // count the number of record in projects_draft table
				->select ('id')
				->from ('projects_draft')
				->where('project_id',$project_id)
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_draft_project == 0 )
				{
					$msg['status'] = 400;
					$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
					echo json_encode($msg);
					die;
				}
			
				$user = $this->session->userdata ('user');
				
				######## connectivity of remote server start#########
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				######## connectivity of remote server end #######
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				$project_draft_dir = $this->config->item('project_draft_dir');
				$profile_folder     = $user[0]->profile_name;
				
				$project_attachment_name = $this->input->post ('project_attachment_name');
				$project_attachment_id = $this->input->post ('project_attachment_id');
				
				$this->db->select('*');
				$this->db->from('draft_projects_attachments');
				$this->db->where('id',$project_attachment_id);
				$project_attachment_result = $this->db->get();
				$project_attachment_data = $project_attachment_result->result_array();
				if(!empty($project_attachment_data)){
					
					
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id .DIRECTORY_SEPARATOR .$project_attachment_data[0]['draft_project_attachment_name'];
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR.$project_attachment_data[0]['draft_project_attachment_name']);
					}
					
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id .DIRECTORY_SEPARATOR .$project_attachment_name;
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR.$project_attachment_name);
					}
					
					$this->db->delete('draft_projects_attachments', array('id' => $project_attachment_id));
					
					$no_draft_project_attachment_uploaded_user = $this->db
					->select ('id')
					->from ('draft_projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					$msg['status'] = 'SUCCESS';
					$msg['message'] = '';
					$upload_button_status = '0';
					if($no_draft_project_attachment_uploaded_user < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
						$upload_button_status = '1';
					}
					$msg['upload_button_status'] = $upload_button_status;
					
				
				
				}else{
				
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id .DIRECTORY_SEPARATOR .$project_attachment_name;
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR.$project_attachment_name);
					}
					$no_project_attachment_uploaded_user_temp = $this->db
					->select ('id')
					->from ('draft_projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					$msg['status'] = 'SUCCESS';
					$msg['message'] = '';
					$upload_button_status = '0';
					if($no_project_attachment_uploaded_user_temp < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
						$upload_button_status = '1';
					}
					$msg['upload_button_status'] = $upload_button_status;
				}
				
				$this->ftp->close();
				echo json_encode ($msg);
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			
			}
        }else{
			show_404();
		}
		
    }
	
	
	/**
	This function is used to return the list of valid draft project attachments if any attachment is not exist in disk then this will remove the entry from table also.
	*/
	public function get_draft_project_attachments($project_id,$user_profile_name){
		if(!empty($project_id) && !empty($user_profile_name)){
			########## fetch the draft project attachments ###
			$this->db->select('*');
			$this->db->from('draft_projects_attachments');
			$this->db->where('project_id',$project_id);
			$this->db->order_by('id',"asc");
			$project_attachment_result = $this->db->get();
			$project_attachment_data = $project_attachment_result->result_array();
			
			$project_attachment_array = array();
			if(!empty($project_attachment_data)){
			
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				$project_draft_dir = $this->config->item('project_draft_dir');
				$profile_folder     = $user_profile_name;
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				foreach($project_attachment_data as $attachment_key){
					$source_path =  $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_key['draft_project_attachment_name'];
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1'){
						$project_attachment['id'] = $attachment_key['id'];
						$project_attachment['project_id'] = $attachment_key['project_id'];
						$project_attachment['project_attachment_name'] = $attachment_key['draft_project_attachment_name'];
						$project_attachment['size']  = number_format($file_size/1024). 'KB';
						$project_attachment_array[] = $project_attachment;
					}else{
						$this->db->delete('draft_projects_attachments', array('id' => $attachment_key['id'])); 
					}
				}
				$this->ftp->close();
			}
			return $project_attachment_array;
		}else{
		
			show_404();
		}
	}
	
	/**
	This function is user the reset the the draft project entiries from database
	*/
	public function reset_draft_project_data ()
    {
		if($this->input->is_ajax_request () ){
			if ($this->input->post ())
			{
				if(check_session_validity()){ // check session exists or not if exist then it will update user session
					$project_id = $this->input->post ('project_id');
					$this->db->select('*');
					$this->db->from('draft_projects_attachments');
					$this->db->where('project_id',$project_id);
					$project_attachment_result = $this->db->get();
					$project_attachment_data = $project_attachment_result->result_array();
					
					if(!empty($project_attachment_data)){
						$user = $this->session->userdata ('user');
						######## connectivity of remote server start#########
						$this->load->library('ftp');
						$config['ftp_hostname'] = $this->config->item('ftp_hostname');
						$config['ftp_username'] = $this->config->item('ftp_username');
						$config['ftp_password'] = $this->config->item('ftp_password');
						$config['ftp_port'] 	= $this->config->item('ftp_port');
						$config['debug']    = TRUE;
						$this->ftp->connect($config); 
						######## connectivity of remote server end #######
						$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
						$projects_ftp_dir = $this->config->item('projects_ftp_dir');
						$project_draft_dir = $this->config->item('project_draft_dir');
						$profile_folder     = $user[0]->profile_name;
						foreach($project_attachment_data as $key=>$value){
							$file_size = $this->ftp->get_filesize($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$value['project_id'].DIRECTORY_SEPARATOR.$value['draft_project_attachment_name']);
							if($file_size != '-1'){
								$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$value['project_id'].DIRECTORY_SEPARATOR.$value['draft_project_attachment_name']);
								$this->db->delete('draft_projects_attachments', array('id' => $value['id'])); 
							}
						}
						$this->ftp->close();
					}
					$this->db->delete('draft_projects_tags', array('project_id' => $project_id));
					
					$project_id = $this->input->post('project_id');
					$draft_project_data = array (
						'project_title'=>'',
						'project_description'=>'',
						'locality_id'=>0,
						'county_id'=>0,
						'postal_code_id'=>0,
						'escrow_payment_method'=>'N',
						'offline_payment_method'=>'N',
						'confidential_dropdown_option_selected'=>'N',
						'not_sure_dropdown_option_selected'=>'N',
						'confidential_after_expiration_selected'=>'N',
						'min_budget'=>'',
						'max_budget'=>'',
						'featured'=>'N',
						'urgent'=>'N',
						'sealed'=>'N',
						'hidden'=>'N'
					);
					
				   $this->db->where ('project_id', $project_id);
				   $this->db->update ('projects_draft', $draft_project_data);
				   $this->db->delete('draft_projects_tags', array('project_id' => $project_id));
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
				}
				
			}else{
				show_404();
			}
		}else{
			show_404();
		}
    }
	
	
	/**
	This function is used to check that  attachment exists or not in either in draft folder or into database
	*/
	public function check_draft_project_attachment_exists ()
    {
		if($this->input->is_ajax_request () ){
			if(check_session_validity()){ 
				$encrypt_attachment_id = $this->input->post ('attachment_id');
				$decrypt_attachment_id = Cryptor::doDecrypt($this->input->post ('attachment_id'));
				$user = $this->session->userdata ('user');
				$project_attachment_detail = $this->db->get_where('draft_projects_attachments', array('id' => $decrypt_attachment_id))->result_array();
				
				if(!empty($project_attachment_detail)){
					$count_draft_project = $this->db // count the number of record in projects_draft table
					->select ('id')
					->from ('projects_draft')
					->where('project_id',$project_attachment_detail[0]['project_id'])
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
					if($count_draft_project == 0 ){
						$msg['status'] = 'FAILED';
						$msg['message'] = $this->config->item('project_attachment_not_exist_validation_post_project_message');
						$msg['location'] = '';
						echo json_encode($msg);
						die;
					
					}
					
					$this->load->library('ftp');
					$config['ftp_hostname'] = $this->config->item('ftp_hostname');
					$config['ftp_username'] = $this->config->item('ftp_username');
					$config['ftp_password'] = $this->config->item('ftp_password');
					$config['ftp_port'] 	= $this->config->item('ftp_port');
					$config['debug']    = TRUE;
					$this->ftp->connect($config); 
					$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
					$projects_ftp_dir = $this->config->item('projects_ftp_dir');
					$project_draft_dir = $this->config->item('project_draft_dir');
					$profile_folder     = $user[0]->profile_name;
					$project_attachment_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_attachment_detail[0]['project_id'] .DIRECTORY_SEPARATOR .$project_attachment_detail[0]['draft_project_attachment_name'];
					$file_size = $this->ftp->get_filesize($project_attachment_path);
					if($file_size != '-1')
					{
						
						$msg['status'] = 'SUCCESS';
						$msg['message'] = '';
						$msg['location'] = VPATH . 'projects/download_draft_project_attachment/'.$encrypt_attachment_id;
					
						
					}else{
						$msg['status'] = 'FAILED';
						$msg['message'] = $this->config->item('project_attachment_not_exist_validation_post_project_message');
						$msg['location'] = '';
					}
					$this->ftp->close();
				}else{
					$msg['status'] = 'FAILED';
					$msg['message'] = $this->config->item('project_attachment_not_exist_validation_post_project_message');
					$msg['location'] = '';
					
				}
				echo json_encode ($msg);die;
			}else{
			
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
			
		}else{
		
			show_404();
		}
    }
	
	
	/**
	This function is used to download project attachment from temp_projects_attachments table.
	*/
	public function download_draft_project_attachment ()
    {
		
		$this->load->helper('download');
		if($this->session->userdata ('user')){
			if(!empty($this->uri->segment(3))){
				$attachment_id = Cryptor::doDecrypt($this->uri->segment(3));
				$user = $this->session->userdata ('user');
				######## connectivity of remote server start#########
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				######## connectivity of remote server end #######
				
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				$project_draft_dir = $this->config->item('project_draft_dir');
				$profile_folder     = $user[0]->profile_name;
				$project_attachment_detail = $this->db->get_where('draft_projects_attachments', array('id' => $attachment_id))->row();
				
				$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_draft_dir.$project_attachment_detail->project_id .DIRECTORY_SEPARATOR .$project_attachment_detail->draft_project_attachment_name;
				$file_size = $this->ftp->get_filesize($source_path);
				if($file_size != '-1')
				{
					$destination_path =  FCPATH .$this->config->item('temp_dir').$project_attachment_detail-> 	draft_project_attachment_name 	;
					$this->ftp->download($source_path,$destination_path, 'auto', 0777);
					$this->ftp->close();
					$data = file_get_contents ($this->config->item('temp_dir').$project_attachment_detail-> 	draft_project_attachment_name 	 );// read the content of file
					unlink($this->config->item('temp_dir').$project_attachment_detail->draft_project_attachment_name);
					force_download ($project_attachment_detail->draft_project_attachment_name,$data);
				}else{
				
					show_404();
				}
			}else{
				show_404();
			}
		}else{
			show_404();
		}
    }

	
	/**
	This function is used to check that attachment exists or not in either in folder or into database
	*/
	public function check_project_attachment_exists ()
    {
		if($this->input->is_ajax_request ()){
			if(check_session_validity()){ 
				$encrypt_attachment_id = $this->input->post ('attachment_id');
				$decrypt_attachment_id = Cryptor::doDecrypt($this->input->post('attachment_id'));
				$project_status = $this->input->post ('project_status');
				$user = $this->session->userdata ('user');
				if($project_status == 'awaiting_moderation'){
				
					$project_attachment_detail = $this->db->get_where('awaiting_moderation_projects_attachments', array('id' => $decrypt_attachment_id))->result_array();
				}else if($project_status == 'open_for_bidding'){
				
					$project_attachment_detail = $this->db->get_where('projects_attachments', array('id' => $decrypt_attachment_id))->result_array();
				}
			
				if(!empty($project_attachment_detail)){ 
					
					$this->load->library('ftp');
					$config['ftp_hostname'] = $this->config->item('ftp_hostname');
					$config['ftp_username'] = $this->config->item('ftp_username');
					$config['ftp_password'] = $this->config->item('ftp_password');
					$config['ftp_port'] 	= $this->config->item('ftp_port');
					$config['debug']    = TRUE;
					$this->ftp->connect($config); 
					$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
					$projects_ftp_dir = $this->config->item('projects_ftp_dir');
					$profile_folder     = $user[0]->profile_name;
					if($project_status == 'awaiting_moderation'){
						$project_awaiting_moderation_dir = $this->config->item('project_awaiting_moderation_dir');
						$project_attachment_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_attachment_detail[0]['project_id'] .DIRECTORY_SEPARATOR .$project_attachment_detail[0]['awaiting_moderation_project_attachment_name'];
						
					}
					else if($project_status == 'open_for_bidding'){
						$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
						$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
						$project_attachment_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_attachment_detail[0]['project_id'] .$project_owner_attachments_dir.$project_attachment_detail[0]['project_attachment_name'];
					}
					$file_size = $this->ftp->get_filesize($project_attachment_path);
					if($file_size != '-1')
					{
						
						$msg['status'] = 'SUCCESS';
						$msg['message'] = '';
						$msg['location'] = VPATH . 'projects/download_project_attachment/'.$encrypt_attachment_id . '/'.$project_status;
					
						
					}else{
						$msg['status'] = 'FAILED';
						$msg['message'] = $this->config->item('project_attachment_not_exist_validation_post_project_message');
						$msg['location'] = '';
					}
					$this->ftp->close(); 
				}else{
					$msg['status'] = 'FAILED';
					$msg['message'] = $this->config->item('project_attachment_not_exist_validation_post_project_message');
					$msg['location'] = '';
					
				} 
				echo json_encode ($msg);die;
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH . $this->config->item('dashboard_page_url');;
				echo json_encode($msg);
				die;
			
			}
			
		}else{
			show_404();
		}
    }
	
	
	/**
	This function is used to download project attachment.
	*/
	public function download_project_attachment ()
    {
		
		$this->load->helper('download');
		if($this->session->userdata ('user')){
			if(!empty($this->uri->segment(3)) && !empty($this->uri->segment(4))){
				$attachment_id = Cryptor::doDecrypt($this->uri->segment(3));
				$project_status = $this->uri->segment(4);
				$user = $this->session->userdata ('user');
				######## connectivity of remote server start#########
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				######## connectivity of remote server end #######
				
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				
				$profile_folder     = $user[0]->profile_name;
				$project_attachment_name = '';
				if($project_status == 'awaiting_moderation'){
					$project_awaiting_moderation_dir = $this->config->item('project_awaiting_moderation_dir');
					$project_attachment_detail = $this->db->get_where('awaiting_moderation_projects_attachments', array('id' => $attachment_id))->row();
					$project_attachment_name = $project_attachment_detail->awaiting_moderation_project_attachment_name;
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_attachment_detail->project_id .DIRECTORY_SEPARATOR .$project_attachment_name;
					
				}else if($project_status == 'open_for_bidding'){
				
					$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
					$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
					$project_attachment_detail = $this->db->get_where('projects_attachments', array('id' => $attachment_id))->row();
					$project_attachment_name = $project_attachment_detail->project_attachment_name;
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_attachment_detail->project_id.$project_owner_attachments_dir.$project_attachment_name;
					
				}
				
				
				$file_size = $this->ftp->get_filesize($source_path);
				if($file_size != '-1')
				{
					$destination_path =  FCPATH .$this->config->item('temp_dir').$project_attachment_name;
					$this->ftp->download($source_path,$destination_path, 'auto', 0777);
					$this->ftp->close();
					$data = file_get_contents ($this->config->item('temp_dir').$project_attachment_name);// read the content of file
					unlink($this->config->item('temp_dir').$project_attachment_name);
					force_download ($project_attachment_name,$data);
				}else{
				
					show_404();
				}
			}else{
				show_404();
			}
		}else{
			show_404();
		}
    }
	
	
	/**
	This function is used to return the list of valid project attachments if any attachment is not exist in disk then this will remove the entry from table also.
	*/
	public function get_project_attachments($project_id,$user_profile_name,$project_status){
		if(!empty($project_id) && !empty($user_profile_name) && !empty($project_status)){
			########## fetch the draft project attachments ###
			if($project_status == 'awaiting_moderation')
			{
				$this->db->select('*');
				$this->db->from('awaiting_moderation_projects_attachments');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_attachment_result = $this->db->get();
				$project_attachment_data = $project_attachment_result->result_array();
				
			
			}elseif($project_status == 'open_for_bidding'){
				$this->db->select('*');
				$this->db->from('projects_attachments');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_attachment_result = $this->db->get();
				$project_attachment_data = $project_attachment_result->result_array();
			}
			
			$project_attachment_array = array();
			if(!empty($project_attachment_data)){
			
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				
				$profile_folder     = $user_profile_name;
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				foreach($project_attachment_data as $attachment_key){
					
					if($project_status == 'awaiting_moderation'){
						$project_awaiting_moderation_dir = $this->config->item('project_awaiting_moderation_dir');
						$source_path =  $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_awaiting_moderation_dir.$project_id.DIRECTORY_SEPARATOR .$attachment_key['awaiting_moderation_project_attachment_name'];
					}else if($project_status == 'open_for_bidding'){
					
						$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
						$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
						$source_path =  $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir .$attachment_key['project_attachment_name'];
						$users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir .$attachment_key['project_attachment_name'];
					}
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1'){
						$project_attachment['id'] = $attachment_key['id'];
						$project_attachment['project_id'] = $attachment_key['project_id'];
						if($project_status == 'awaiting_moderation'){
							$project_attachment['project_attachment_name'] = $attachment_key['awaiting_moderation_project_attachment_name'];
						}else if($project_status == 'open_for_bidding'){
							$project_attachment['project_attachment_name'] = $attachment_key['project_attachment_name'];
						}
						$project_attachment['size']  = number_format($file_size/1024). 'KB';
						$project_attachment_array[] = $project_attachment;
					}else{
						/* if($project_status == 'awaiting_moderation'){
							$this->db->delete('awaiting_moderation_projects_attachments', array('id' => $attachment_key['id'])); 
						}else if($project_status == 'open_for_bidding'){
							$this->db->delete('projects_attachments', array('id' => $attachment_key['id'])); 
						} */
					}
				}
				$this->ftp->close();
			}
			return $project_attachment_array;
		}else{
			show_404();
		}
	}
	
	
	/**
		This function is used to upload project attachment whose available for open for bidding.
	*/
	public function upload_project_attachment ()
    {
		if($this->input->is_ajax_request ()){
		
			if(!$this->uri->segment('3')){ // if project temp id not coming it will show 404 page
			show_404();
			}
			if(check_session_validity()){
				$user = $this->session->userdata ('user');
				$project_id = $this->uri->segment('3');
				$count_open_for_bidding_project = $this->db // count the number of record in projects_open_bidding table
				->select ('id')
				->from ('projects_open_bidding')
				->where('project_id',$project_id)
				->where('project_owner_id',$user[0]->user_id)
				->get ()->num_rows ();
				if($count_open_for_bidding_project > 0 )
				{
					$no_project_attachment_uploaded_user = $this->db
					->select ('id')
					->from ('projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					
					$this->db->where('project_id', $project_id);
					
					$project_attachment_maximum_size_limit	 = $this->config->item('project_attachment_maximum_size_limit');
					
					$project_attachment_maximum_size_limit = ($project_attachment_maximum_size_limit * 1048576);
					if(!empty($_FILES['file']['tmp_name'])){
						$file_array = $_FILES['file'];
						if($file_array['size'] > $project_attachment_maximum_size_limit){
							$msg['status'] = 'FAILED';
							$msg['message'] = $this->config->item('project_attachment_maximum_size_validation_post_project_message');
							echo json_encode ($msg);die;
						}elseif($no_project_attachment_uploaded_user >= $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
							$msg['status'] = 'FAILED';
							$msg['message'] = 'you are not allowed to upload files';
							echo json_encode ($msg);die;
						
						}else{
							
							######## connectivity of remote server start#########
							$this->load->library('ftp');
							$config['ftp_hostname'] = $this->config->item('ftp_hostname');
							$config['ftp_username'] = $this->config->item('ftp_username');
							$config['ftp_password'] = $this->config->item('ftp_password');
							$config['ftp_port'] 	= $this->config->item('ftp_port');
							$config['debug']    = TRUE;
							$this->ftp->connect($config); 
							######## connectivity of remote server end #######
							
							$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
							$projects_ftp_dir = $this->config->item('projects_ftp_dir');
							$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
							$project_owner_attachments_dir = $this->config->item('project_owner_attachments_dir');
							$profile_folder     = $user[0]->profile_name;
							$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
							
							if(!empty($project_id )){
							
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir, 0777);// create projects directory if not exists
					
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir, 0777);// create directory for open for bidding project
								
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id, 0777); // create the project directory if it is not exists
								$this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir , 0777); // create the project attachment directory
								$temp 		= 	explode(".", $file_array["name"]);
								$extension 	= 	end($temp);
								$attachment_name 	= 	$this->config->item('project_attachment_name')."_".rand(0,1000).$project_id.'.'.$extension;// name of attachment
								
								if(move_uploaded_file($file_array['tmp_name'],$this->config->item('temp_dir').$attachment_name)){
									
									$source_path = FCPATH .$this->config->item('temp_dir'). $attachment_name;
									
									$destination_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir .$attachment_name;
									
									$this->ftp->upload($source_path,$destination_path , 'auto', 0777); // upload the attachment into temporary folder of projects 
									unlink(FCPATH .$this->config->item('temp_dir'). $attachment_name);
									$projects_attachments_data = array('project_id'=>$project_id,'project_attachment_name'=>$attachment_name);
									$this->db->insert ('projects_attachments', $projects_attachments_data);
									$last_insert_id = $this->db->insert_id();
									
									$no_project_attachment_uploaded_user = $this->db
									->select ('id')
									->from ('projects_attachments')
									->where ('project_id', $project_id)
									->get ()->num_rows (); // check the number of attachment of user into database
									
									$msg['status'] = 'OK';
									$msg['message'] = 'uploded';
									$msg['filename'] = $attachment_name;
									$msg['size'] = number_format($file_array['size']/1024). 'KB';
									$msg['id'] = $last_insert_id;
									$msg['encrypt_id'] = Cryptor::doEncrypt($last_insert_id);
									
									$upload_button_status = '0';
									if($no_project_attachment_uploaded_user < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
										$upload_button_status = '1';
									}
									$msg['upload_button_status'] = $upload_button_status;
								}
							}
							$this->ftp->close();
						}	
						
					}else{
						$msg['status'] = 'FAILED';
						$msg['message'] = 'file is empty';
					}
						
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
					$msg['message'] = '';
				}
				echo json_encode ($msg);die;
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
			
		}else{
		
			show_404();
		}
    }
	
	
	
	/**
	This function is used to remove the attachment of  projects which are availabe for open for bidding.
	*/
	public function delete_project_attachment ()
    {
		if($this->input->is_ajax_request ()){
			if(empty($this->input->post ('project_attachment_id'))){
			
				show_404();
			}
			if(check_session_validity()){ 
				
				$user = $this->session->userdata ('user');
				######## connectivity of remote server start#########
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config); 
				######## connectivity of remote server end #######
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				$project_open_for_bidding_dir = $config['project_open_for_bidding_dir'];
				$project_owner_attachments_dir = $config['project_owner_attachments_dir'];
				$profile_folder     = $user[0]->profile_name;
				
				$project_attachment_name = $this->input->post ('project_attachment_name');
				$project_attachment_id = $this->input->post ('project_attachment_id');
				$project_id = $this->input->post ('project_id');
				$this->db->select('*');
				$this->db->from('projects_attachments');
				$this->db->where('id',$project_attachment_id);
				$project_attachment_result = $this->db->get();
				$project_attachment_data = $project_attachment_result->result_array();
				if(!empty($project_attachment_data)){
					
					
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_attachment_data[0]['project_id'].$project_owner_attachments_dir.$project_attachment_data[0]['project_attachment_name'];
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir.$project_attachment_data[0]['project_attachment_name']);
					}
					
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir.$project_attachment_name;
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_attachment_data[0]['project_id'].$project_owner_attachments_dir.$project_attachment_name);
					}
					
					$this->db->delete('projects_attachments', array('id' => $project_attachment_id));
					
					$no_project_attachment_uploaded_user = $this->db
					->select ('id')
					->from ('projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					$msg['status'] = 'SUCCESS';
					$msg['message'] = '';
					$upload_button_status = '0';
					if($no_project_attachment_uploaded_user < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
						$upload_button_status = '1';
					}
					$msg['upload_button_status'] = $upload_button_status;
					
				
				
				}else{
				
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$project_owner_attachments_dir.$project_attachment_name;
					$file_size = $this->ftp->get_filesize($source_path);
					if($file_size != '-1')
					{
						$this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_attachment_data[0]['project_id'].$project_owner_attachments_dir.$project_attachment_name);
					}
					$no_project_attachment_uploaded_user = $this->db
					->select ('id')
					->from ('projects_attachments')
					->where ('project_id', $project_id)
					->get ()->num_rows ();// check the number of attachment of user into database
					$msg['status'] = 'SUCCESS';
					$msg['message'] = '';
					$upload_button_status = '0';
					if($no_project_attachment_uploaded_user < $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
						$upload_button_status = '1';
					}
					$msg['upload_button_status'] = $upload_button_status;
				}
				
				$this->ftp->close();
				echo json_encode ($msg);die;
				
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
			
		}else{
			show_404();
		}
    }
	
	
	/**
	This function is used to delete the project tag from projects_tags table.
	*/
	public function delete_project_tag ()
    {
		if($this->input->is_ajax_request ()){
			if (empty($this->input->post ('project_tag_id')))
			{
				show_404();
			}
			if(check_session_validity()){ 
				$project_tag_id = $this->input->post ('project_tag_id');
				$project_tag_array = explode("_",$project_tag_id);
				$this->db->delete('projects_tags', array('id' => $project_tag_array[2]));
				$msg['status'] = 'SUCCESS';
				$msg['message'] = '';
				echo json_encode ($msg);
				die;
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
		}else{
			show_404();
		}
    }
	
	/**
	This function is used to delete the project category from projects_categories_listing_tracking table.
	*/
	public function delete_project_category ()
    {
		if($this->input->is_ajax_request ()){
			if (empty($this->input->post ('category_project_id')))
			{
				show_404();
			}	
			if(check_session_validity()){ 	
				$category_project_id = $this->input->post ('category_project_id');
				$remove_category_id = $this->input->post ('remove_category_id');
				$category_project_array  = explode('_',$category_project_id);
				$this->db->delete('projects_categories_listing_tracking', array('id' => $category_project_array[0]));
				$count_project_categories = $this->db // count the number of record in temp_projects table
				->select ('id')
				->from ('projects_categories_listing_tracking')
				->where('project_id',$category_project_array[1])
				->get ()->num_rows ();
					$msg['add_category_button_show_status']  = '1';
				if( $count_project_categories >= $this->config->item('number_project_category_post_project')){
					$msg['add_category_button_show_status']  = '0';
				}
				$msg['remove_category_id'] = $remove_category_id;
				$msg['status'] = 'SUCCESS';
				$msg['message'] = '';
				echo json_encode ($msg);
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
		}else{
			show_404();
		}
    }
	
	
	// create the html of edit upgrade/prolong availability  popup for open for bidding project
	public function ajax_project_upgrade_popup_body(){
	
		if($this->input->is_ajax_request ()){
			
			if (!empty($this->input->post ('project_id')))
			{
				if(check_session_validity()){ // check session exists or not if exist then it will update user session
					$project_id = $this->input->post ('project_id');
					$user = $this->session->userdata ('user');
					$check_project_exists = $this->db // check project exists in projects_open_bidding table
					->select ('id')
					->from ('projects_open_bidding')
					->where('project_id',$project_id)
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
					if($check_project_exists == 0){
						$msg['status'] = 400;
						$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
						echo json_encode($msg);
						die;
					}
					if($this->input->post ('action_type') && in_array($this->input->post ('action_type'),array('prolong_availability_urgent','upgrade_as_urgent_project','prolong_availability_featured','upgrade_as_featured_project','upgrade_project'))){
						$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_owner_id,projects_open_bidding.project_expiration_date,projects_open_bidding.featured,projects_open_bidding.urgent,projects_open_bidding.sealed,projects_open_bidding.hidden,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
						$this->db->from('projects_open_bidding');
						
						
						$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
						$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
						
						$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
						
						$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
						
						$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
						
						$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
						
						$this->db->where('projects_open_bidding.project_id',$project_id);
						$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
						
						$project_result = $this->db->get();
						$project_data = $project_result->result_array();
						
						
						$data['project_data'] = $project_data;
						$data['action_type'] = $this->input->post ('action_type');
						$res['project_upgrade_popup_body'] = $this->load->view('ajax_project_upgrade_popup_body', $data, true);
						$res['status'] = 200;
						echo json_encode($res);
						die;
					}else{
						$res['status'] = 400;
						$msg['location'] = VPATH . $this->config->item('dashboard_page_url');
						echo json_encode($res);
						die;
					}
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
				}
			}else{
				show_404();
			}
		}else{
			show_404();
		}
	
	}
	
	// update the project upgrades into database(edit upgrade/prolong availability) for open for bidding project
	public function ajax_update_project_upgrade(){
	
		if($this->input->is_ajax_request ()){
			
			if (!empty($this->input->post ('project_id')))
			{
				if(check_session_validity()){ // check session exists or not if exist then it will update user session
					$expiration_upgrade_date_array = array();
					$project_id = $this->input->post ('project_id');
					$user = $this->session->userdata ('user');
					
					$check_project_exists = $this->db // check project exists in projects_open_bidding table
					->select ('id')
					->from ('projects_open_bidding')
					->where('project_id',$project_id)
					->where('project_owner_id',$user[0]->user_id)
					->get ()->num_rows ();
					if($check_project_exists == 0){
						$res['status'] = 400;
						echo json_encode($res);
						return;
					}
					$upgraded_project_price = 0;
					if($this->input->post ('upgrade_type_featured')){
						$upgraded_project_price += $this->config->item('project_upgrade_price_featured');
					}
					if($this->input->post ('upgrade_type_urgent')){
						$upgraded_project_price += $this->config->item('project_upgrade_price_urgent');
					}
					if(floatval($upgraded_project_price) > 0){
						$user_detail = $this->db->get_where('user_details', ['user_id' => $user[0]->user_id])->row_array();
						$total_user_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'] + $user_detail['user_account_balance'];
						if(floatval($upgraded_project_price) > floatval($total_user_balance) ){
						
							$res = array(
									'status' => 400,
									'location' => '',
									'error' => $this->config->item('user_post_upgraded_project_insufficient_funds_error_message') // define in post_project_custom config
								);
							echo json_encode($res);
							die;
						}
					}
					$is_featured = '0';
				
					$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_owner_id,projects_open_bidding.project_expiration_date,projects_open_bidding.featured,projects_open_bidding.urgent,projects_open_bidding.sealed,projects_open_bidding.hidden,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
					$this->db->from('projects_open_bidding');
					
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'"  and project_id = "'.$project_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
				
					$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->where('projects_open_bidding.project_id',$project_id);
					$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
					
					$project_result = $this->db->get();
					$project_data = $project_result->result_array();
					
					
					
					$featured_max = 0;
					$urgent_max = 0;
					$expiration_featured_upgrade_date_array = array();
					$expiration_urgent_upgrade_date_array = array();
					$user_selected_upgrades = array();
					$user_selected_upgrades['project_id'] = $project_id;
					
					if(!empty($project_data[0]['featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
					}
					if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
					}
					if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
					}
					if(!empty($expiration_featured_upgrade_date_array)){
						$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
					}

					##########

					if(!empty($project_data[0]['urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['urgent_upgrade_end_date'];
					}
					if(!empty($project_data[0]['bounus_urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['bounus_urgent_upgrade_end_date'];
					}
					if(!empty($project_data[0]['membership_include_urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['membership_include_urgent_upgrade_end_date'];
					}
					if(!empty($expiration_urgent_upgrade_date_array)){
						$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
					}
					
					$expiration_upgrade_date_array[] = $project_data[0]['project_expiration_date'];
					if($this->input->post ('upgrade_type_featured')){
					
						$time_arr = explode(':', $this->config->item('project_upgrade_availability_featured'));
						if(empty($featured_max)){
							$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
						}else{
							
							$upgrade_end_date = date('Y-m-d H:i:s',strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',$featured_max));
						}
						$this->db->update('projects_open_bidding', ['featured' => 'Y'], ['project_id' => $project_id]);
						$expiration_upgrade_date_array[] = $upgrade_end_date;
						$is_featured = '1';
						$user_selected_upgrades['featured'] = 'Y';
						$user_selected_upgrades['featured_upgrade_end_date'] = $upgrade_end_date;
					
					}
					//if($count_featured_upgrade_project_row == 0 && $this->input->post ('upgrade_type_urgent')){
					if($this->input->post ('upgrade_type_urgent')){
						$time_arr = explode(':', $this->config->item('project_upgrade_availability_urgent'));
						if(empty($urgent_max)){
							$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds')); 
						}else{
							$upgrade_end_date = date('Y-m-d H:i:s',strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',$urgent_max));
						}
					
						$expiration_upgrade_date_array[] = $upgrade_end_date;
						$this->db->update('projects_open_bidding', ['urgent' => 'Y'], ['project_id' => $project_id]);
						$user_selected_upgrades['urgent'] = 'Y';
						$user_selected_upgrades['urgent_upgrade_end_date'] = $upgrade_end_date;
					}
					if($this->input->post ('upgrade_type_featured') || $this->input->post ('upgrade_type_urgent') && !empty($expiration_upgrade_date_array)){
						
						$max = max(array_map('strtotime', $expiration_upgrade_date_array));
						$this->db->update('projects_open_bidding', ['project_expiration_date'=>date('Y-m-d H:i:s', $max)], ['project_id' => $project_id]);
						$this->Projects_model->user_project_upgrade_purchase_refresh_sequence_tracking_membership_exclude_save($user_selected_upgrades,$user[0]->user_id,$expiration_upgrade_date_array);// track purchasing and refresh sequence;
					}
					/* fetch the information of project upgrade start */
					$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_owner_id,projects_open_bidding.project_expiration_date,projects_open_bidding.featured,projects_open_bidding.urgent,projects_open_bidding.sealed,projects_open_bidding.hidden,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date');
					$this->db->from('projects_open_bidding');
				
					$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'"  group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
				
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'"  group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent" and project_owner_id = "'.$user[0]->user_id.'" and project_id = "'.$project_id.'" group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
					
					$this->db->where('projects_open_bidding.project_id',$project_id);
					$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
					
					
					$project_result = $this->db->get();
					$project_data = $project_result->result_array();
					
					$featured_max = 0;
					$urgent_max = 0;
					$expiration_featured_upgrade_date_array = array();
					$expiration_urgent_upgrade_date_array = array();

					if(!empty($project_data[0]['featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
					}
					if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
					}
					/* if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
						$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
					} */
					if(!empty($expiration_featured_upgrade_date_array)){
						$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
					}

					##########

					if(!empty($project_data[0]['urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['urgent_upgrade_end_date'];
					}
					if(!empty($project_data[0]['bounus_urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['bounus_urgent_upgrade_end_date'];
					}
					/* if(!empty($project_data[0]['membership_include_urgent_upgrade_end_date'])){
						$expiration_urgent_upgrade_date_array[] = $project_data[0]['membership_include_urgent_upgrade_end_date'];
					} */
					if(!empty($expiration_urgent_upgrade_date_array)){
						$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
					}

					
					
					$open_for_bidding_project_upgrades_badges_html = '';
					$open_for_bidding_project_upgrades_prolong_availability_information_html = '';
					$open_for_bidding_project_actions_html = '';
					if($project_data[0]['featured'] == 'Y' && $featured_max != 0 && $featured_max > time() ){
						$open_for_bidding_project_upgrades_badges_html .= '<button type="button" class="btn">Featured</button>';
					}if($project_data[0]['urgent'] == 'Y' && $urgent_max != 0 && $featured_max > time() ){
						$open_for_bidding_project_upgrades_badges_html .= '<button type="button" class="btn urgent">Urgent</button>';
					}if($project_data[0]['sealed'] == 'Y'){
						$open_for_bidding_project_upgrades_badges_html .= '<button type="button" class="btn">Sealed</button>';
					}
					if($project_data[0]['featured'] == 'Y' && $featured_max != 0 && $featured_max > time() ){
						$open_for_bidding_project_upgrades_prolong_availability_information_html .= '<li><label><span>Featured Upgrade </span><small>'.$this->config->item('project_details_page_expires_on').'&nbsp;'.date(DATE_TIME_FORMAT,$featured_max).'</small></label><button type="button" data-attr= "'.$project_id.'" class="btn upgrade_project" data-action-type="prolong_availability_featured">Prolong Availability</button></li>';
					}
					if($project_data[0]['urgent'] == 'Y' &&  $urgent_max != 0 && $urgent_max > time()){
						$open_for_bidding_project_upgrades_prolong_availability_information_html .= '<li><label><span>Urgent Upgrade </span><small>'.$this->config->item('project_details_page_expires_on').'&nbsp;'.date(DATE_TIME_FORMAT,$urgent_max).'</small></label><button type="button" data-attr= "'.$project_id.'" class="btn upgrade_project" data-action-type="prolong_availability_urgent">Prolong Availability</button></li>';
					}
					
					$open_for_bidding_project_actions_html .= '<a class="dropdown-item edit_project" data-attr= "'.$project_id.'" style="cursor:pointer">Edit Project</a>';
					if($project_data[0]['featured'] == 'N' && $project_data[0]['urgent'] == 'N' && empty($featured_max) && empty($urgent_max)){
					
						$open_for_bidding_project_actions_html.= '<a id="upgrade_project_'.$project_id.'" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "'.$project_id.'"data-action-type="upgrade_project">Upgrade Project</a>';
					
					}elseif($project_data[0]['featured'] == 'N' && $project_data[0]['urgent'] == 'Y' && empty($featured_max)){
						$open_for_bidding_project_actions_html.= '<a id="upgrade_project_'.$project_id.'" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "'.$project_id.'"data-action-type="upgrade_as_featured_project">Upgrade As Featured Project</a>';
					}elseif($project_data[0]['featured'] == 'Y' && $project_data[0]['urgent'] == 'N' &&  empty($urgent_max)){
						$open_for_bidding_project_actions_html.= '<a id="upgrade_project_'.$project_id.'" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "'.$project_id.'"data-action-type="upgrade_as_urgent_project">Upgrade As Urgent Project</a>';
					} 
					
					/* fetch the information of project upgrade end */
					$res['project_id'] = $project_id;
					$res['open_for_bidding_project_upgrades_badges_html'] = $open_for_bidding_project_upgrades_badges_html;
					$res['open_for_bidding_project_upgrades_prolong_availability_information_html'] = $open_for_bidding_project_upgrades_prolong_availability_information_html;
					$res['open_for_bidding_project_actions_html'] = $open_for_bidding_project_actions_html;
					$res['is_featured'] = $is_featured;
					$res['status'] = 200;
					echo json_encode($res);
					return;
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
				}
				
			}else{
				show_404();
			}
		}else{
			show_404();
		}
	
	}
	
	
	public function show_project_upgrade_balance_message(){
		if($this->input->is_ajax_request ()){
			$upgrade_message = '';
			if(!empty($this->input->post())){
				if(check_session_validity()){ // check session exists or not if exist then it will update user session
					$user = $this->session->userdata('user');
					$user_id = $user[0]->user_id;
					
					$count_user_featured_membership_included_upgrades_monthly = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user_id); // count user membership featured  upgrade
					
					$count_user_urgent_membership_included_upgrades_monthly = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user_id);// count user membership urgent upgrade
					
					$user_detail = $this->db->get_where('user_details', ['user_id' => $user[0]->user_id])->row_array();
					$user_membership_plan_detail = $this->db->get_where('membership_plans', ['id' => $user_detail['current_membership_plan_id']])->row_array();
					$total_user_upgrade_balance = false;
					
					if(!empty($this->input->post ('upgrade_type_featured'))){
					 
						$total_user_upgrade_balance = true;
					}
					if(!empty($this->input->post ('upgrade_type_urgent'))){
						
						$total_user_upgrade_balance = true;
						
					}
					if($total_user_upgrade_balance){
						$total_bonus_balance = $user_detail['bonus_balance'] + $user_detail['signup_bonus_balance'];
						
						if(floatval($total_bonus_balance) > 0 ){
							$upgrade_message .= '<div class="form-group col-md-12 disclaimer"><div class="custom_checkbox"><input class="checked_input" value="1" name=""  type="checkbox" checked><small class="checkmark"></small></div>payments related to "Projects Upgrades" are final, and there are no refunds given for these purchases</div>';
							$upgrade_message .= '<div class="form-group col-md-12 bonus_balance">
							<div class="custom_checkbox">
								<input class="checked_input" value="1" name=""  type="checkbox" checked>
								<small class="checkmark"></small>
							 </div>
							 I agree to use my bonus balance to pay for this project upgrade purchase (your current available bonus balance is '.number_format($total_bonus_balance,2);'KÄ)</div>';
						
						}else{
							$upgrade_message .= '<div class="form-group col-md-12 disclaimer"><div class="custom_checkbox"><input class="checked_input" value="1" name=""  type="checkbox" checked><small class="checkmark"></small></div>payments related to "Projects Upgrades" are final, and there are no refunds given for these purchases</div>';
						
						}
					}
					$msg['status'] = 'SUCCESS';
					$msg['upgrade_message'] = $upgrade_message;
					echo json_encode ($msg);die;
				}else{
					$msg['status'] = 400;
					$msg['location'] = VPATH;
					echo json_encode($msg);
					die;
				}
				
			}else{
				$msg['status'] = 'SUCCESS';
				$msg['upgrade_message'] = $upgrade_message;
				echo json_encode ($msg);die;
			}
		}else{
			show_404();
		}
	}
	//--- Cover Picture Upload Function Area Start -----
	public function resize_image($file, $w, $h, $crop=FALSE) {
        //list($width, $height) = getimagesize($file);
        $src = imagecreatefromstring($file);
        if (!$src) return false;
        $width = imagesx($src);
        $height = imagesy($src);

        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
              $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
              $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
              $newwidth = $h*$r;
              $newheight = $h;
            } else {
              $newheight = $w/$r;
              $newwidth = $w;
            }
        }
        //$src = imagecreatefrompng($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        // Buffering
          ob_start();
          imagepng($dst);
          $data = ob_get_contents();
          ob_end_clean();
          return $data;
    }
    
	
	
    public function ftp_upload_file($data=array()) {
        $this->load->library('ftp');
        $config['ftp_hostname'] = $this->config->item('ftp_hostname');
        $config['ftp_username'] = $this->config->item('ftp_username');
        $config['ftp_password'] = $this->config->item('ftp_password');
        $config['ftp_port'] 	= $this->config->item('ftp_port');
        $config['debug']    = TRUE;
        $this->ftp->connect($config);
        
        $users_ftp_dir 	= $this->config->item('users_ftp_dir');
        $profile_folder = $data['dir_profile'];
        $projects_folder = $data['dir_projects'];
        $open_for_bidding_folder = $data['dir_open_for_bidding'];
        $dir_project_id_folder = $data['dir_project_id'];
        $upload_folder 	= $data['dirname'];

        //echo $users_ftp_dir.$profile_folder.$upload_folder.$data['org_image_name'];exit;
		
		$users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$upload_folder.$data['org_image_name'];
		
		
		
        $this->ftp->download($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$upload_folder.$data['org_image_name'], FCPATH .$this->config->item('temp_dir') . $data['org_image_name'], 'auto', 0777);

        $this->ftp->close();
    }
    public function ftp_server_check($data=array()) {
        $this->load->library('ftp');
        $config['ftp_hostname'] = $this->config->item('ftp_hostname');
        $config['ftp_username'] = $this->config->item('ftp_username');
        $config['ftp_password'] = $this->config->item('ftp_password');
        $config['ftp_port'] 	= $this->config->item('ftp_port');
        $config['debug']    = TRUE;
        $this->ftp->connect($config);

        $users_ftp_dir 	= $this->config->item('users_ftp_dir');
        $profile_folder = $data['dir_profile'];
        $projects_folder = $data['dir_projects'];
        $open_for_bidding_folder = $data['dir_open_for_bidding'];
        $dir_project_id_folder = $data['dir_project_id'];
        $upload_folder 	= $data['dir'];
        //	make profile folder
        $this->ftp->mkdir($users_ftp_dir.$profile_folder, 0777);
		//	make projects folder
        $this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_folder, 0777);
		//	make open_for_bidding_folder folder
        $this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder, 0777);
		//	make dir_project_id folder
        $this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder, 0777);
        // make sub-folder 
        foreach($upload_folder as $dir){
            $this->ftp->mkdir($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$dir, 0777);
        }
        // delete cover picture if exists
        if(isset($data['cover_picture_file']) && $data['cover_picture_file']!='') {
            $this->ftp->delete_file($users_ftp_dir.$data['cover_picture_file']);
        }

        //picture upload
        if(isset($data['image_name']) && $data['image_name']!='') {
            //echo FCPATH .$this->config->item('temp_dir'). $data['image_name'].'<br>'.$users_ftp_dir.$profile_folder.$data['dirname'].$data['image_name'];exit;
            $this->ftp->upload(FCPATH .$this->config->item('temp_dir'). $data['image_name'], $users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$data['dirname'].$data['image_name'], 'auto', 0777);
            unlink(FCPATH .$this->config->item('temp_dir'). $data['image_name']);

            //echo $users_ftp_dir.$data['cover_picture_org_file'];exit;
            if(isset($data['cover_picture_file']) && $data['cover_picture_file']!='') {
				$this->ftp->rename($users_ftp_dir.$data['cover_picture_org_file'], $users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$data['dirname'].$data['org_image_name']);
				$im	= $data['cover_picture_org_file'];
				$ima = explode('/', $im);
				$imc	= count($ima)-1;
				unlink(FCPATH .$this->config->item('temp_dir') . $ima[$imc]);
            } else {
                $this->ftp->upload(FCPATH .$this->config->item('temp_dir') . $data['org_image_name'], $users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$data['dirname'].$data['org_image_name'], 'auto', 0777);
                unlink(FCPATH .$this->config->item('temp_dir') . $data['org_image_name']);
            }
        }

        $this->ftp->close();
    }
    public function ftp_file_del($data=array()) {
        $this->load->library('ftp');
        $config['ftp_hostname'] = $this->config->item('ftp_hostname');
        $config['ftp_username'] = $this->config->item('ftp_username');
        $config['ftp_password'] = $this->config->item('ftp_password');
        $config['ftp_port'] 	= $this->config->item('ftp_port');
        $config['debug']    = TRUE;
        $this->ftp->connect($config);
		
	$users_ftp_dir              = $this->config->item('users_ftp_dir');
        $profile_folder             = $data['dir_profile'];
        $projects_folder            = $data['dir_projects'];
        $open_for_bidding_folder    = $data['dir_open_for_bidding'];
        $dir_project_id_folder      = $data['dir_project_id'];
        $dirname                    = $data['dirname'];
	
        $image_name     = $data['image_name'];
        $original_image_name = $data['org_image_name'];

        //echo $users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$dirname.$image_name;
        // delete cover picture if exists
        if(isset($image_name) && $image_name!='') {
            $this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$dirname.$image_name);
            $this->ftp->delete_file($users_ftp_dir.$profile_folder.$projects_folder.$open_for_bidding_folder.$dir_project_id_folder.$dirname.$original_image_name);
        }

        $this->ftp->close();
        
    }
	
    public function fetch_featured_project_cropped_cover_picture() {
		
		if(check_session_validity()){ 
			$user = $this->session->userdata ('user');
			$project_id = $this->input->post ('id');
			
			$this->load->library('ftp');
			$config['ftp_hostname'] = $this->config->item('ftp_hostname');
			$config['ftp_username'] = $this->config->item('ftp_username');
			$config['ftp_password'] = $this->config->item('ftp_password');
			$config['ftp_port'] 	= $this->config->item('ftp_port');
			$config['debug']    = TRUE;
			$this->ftp->connect($config);
			
			$this->db->select('projects_open_bidding.project_id,users.profile_name');
			$this->db->from('projects_open_bidding');
			$this->db->join('users', 'users.user_id = projects_open_bidding.project_owner_id', 'left');
			$this->db->where('projects_open_bidding.project_id',$project_id);
			$project_result = $this->db->get();
			$project_data = $project_result->result_array();
			$profile_folder     = $project_data[0]['profile_name'];
			
			$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
			$projects_ftp_dir = $this->config->item('projects_ftp_dir');
			$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
			$featured_upgrade_cover_picture = $this->config->item('featured_upgrade_cover_picture');
			$im	= $this->input->post("image");
			$ima = explode('/', $im);
			$imc	= count($ima)-1;
			//print_r($ima);exit;
			$imag = explode('.', $ima[$imc]);
			$original_image_name = $imag[0] . "_original.png";
			$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$featured_upgrade_cover_picture.$original_image_name;
			
			$file_size = $this->ftp->get_filesize($source_path);
			if($file_size == '-1')
			{
				$msg['status'] = 400;
				$msg['error'] = $this->config->item('invalid_featured_project_cover_picture_validation_message');
				$msg['error_type'] = 'invalid_featured_project_cover_picture';
				echo json_encode($msg);
				die;
				
			}
			$destination_path = FCPATH .$this->config->item('temp_dir') . $original_image_name;
			$this->ftp->download($source_path,$destination_path, 'auto', 0777);
			$this->ftp->close();
			$msg['status'] = 200;
			$msg['cover_picture'] = URL .$this->config->item('temp_dir') . $original_image_name;
			echo json_encode($msg);
			die;
			
		}else{
			show_404();
		}
    }
	
    public function save_featured_project_cropped_cover_picture_edit() {
		if($this->input->is_ajax_request ()){
			$msg['location'] = '';
			$project_id = $this->input->post ('id');
			if(empty($this->input->post ('id'))){
			
				show_404();
			}
			if(check_session_validity()){ 
				$user = $this->session->userdata ('user');
				
				
				$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_expiration_date,users.profile_name');
				$this->db->from('projects_open_bidding');
				$this->db->join('users', 'users.user_id = projects_open_bidding.project_owner_id', 'left');
				$this->db->where('projects_open_bidding.project_id',$project_id);
				$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
                $project_data = $project_result->result_array();
				$project_expiration_date = $project_data[0]['project_expiration_date']!= NULL ? strtotime ($project_data[0]['project_expiration_date']) : 0;
				
				if($project_expiration_date < time()){
					
					$msg['status'] = 400;
					$msg['error'] = $this->config->item('project_expiration_validation_message');
					echo json_encode($msg);
					die;
				}
				
				$expiration_status = $this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				if($expiration_status){
				
					$this->load->library('ftp');
					$config['ftp_hostname'] = $this->config->item('ftp_hostname');
					$config['ftp_username'] = $this->config->item('ftp_username');
					$config['ftp_password'] = $this->config->item('ftp_password');
					$config['ftp_port'] 	= $this->config->item('ftp_port');
					$config['debug']    = TRUE;
					$this->ftp->connect($config);
			
					$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
					$projects_ftp_dir = $this->config->item('projects_ftp_dir');
					$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
					$featured_upgrade_cover_picture = $this->config->item('featured_upgrade_cover_picture');
					$profile_folder     = $project_data[0]['profile_name'];
					
				
				
					$im = base64_decode($this->input->post("image"));
					$original = base64_decode($this->input->post("original"));

					$tt = "cover_" . time();
					$image_name = $tt .'.png';
					$original_image_name = $tt . "_original.png";

					$ftp['image_name'] = $image_name;
					$ftp['org_image_name'] = $original_image_name;
					
					$ftp['dir_project_id'] = $project_id;
					$ftp['dir_profile'] = $user[0]->profile_name;
					$ftp['dir_projects'] = $this->config->item('projects_ftp_dir');
					$ftp['dir_open_for_bidding'] = $this->config->item('project_open_for_bidding_dir');
					$ftp['dir'][] = $this->config->item('featured_upgrade_cover_picture');
					$ftp['dirname'] = $this->config->item('featured_upgrade_cover_picture');

					$picdata = $this->db->get_where('featured_projects_users_upload_cover_pictures_tracking', array('project_id' => $project_id))->row();
					
					$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$featured_upgrade_cover_picture.$picdata->project_cover_picture_name;
					
					$file_size = $this->ftp->get_filesize($source_path);
					
					if($picdata->project_cover_picture_name!='' && $file_size != '-1') {
						$ftp['cover_picture_file'] = $user[0]->profile_name . $this->config->item('projects_ftp_dir') . $this->config->item('project_open_for_bidding_dir') . $project_id. $this->config->item('featured_upgrade_cover_picture') .$picdata->project_cover_picture_name;
						$ex = explode('.',$picdata->project_cover_picture_name);
						$ftp['cover_picture_org_file'] = $user[0]->profile_name . $this->config->item('projects_ftp_dir') . $this->config->item('project_open_for_bidding_dir') . $project_id. $this->config->item('featured_upgrade_cover_picture') .$ex[0].'_original.png';
					}

					// upload cropped image to server 
					$this->load->helper("file");

					$newIamge = $this->resize_image($im, $this->input->post("width"), 500);
					file_put_contents(FCPATH .$this->config->item('temp_dir') . $image_name, $newIamge); 
					file_put_contents(FCPATH .$this->config->item('temp_dir') . $original_image_name, $original);
					$this->ftp_server_check($ftp);
					$msg['domain'] = $this->config->item('ftp_domainname');
					$msg['u_dir'] = $this->config->item('users_ftp_dir');
					$msg['status'] = 200;
					$msg['cover_picture'] = $user[0]->profile_name . $this->config->item('projects_ftp_dir') . $this->config->item('project_open_for_bidding_dir') . $project_id. $this->config->item('featured_upgrade_cover_picture') . $image_name;
					
				}else{
					$msg['status'] = 400;
					$msg['error'] = 'your upgrade is expired';
				}
				echo json_encode($msg);
				die;	
			}else{
				$this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}	
		}else{
			show_404();
		
		}
    }
	
    public function save_featured_project_cropped_cover_picture() {
		if($this->input->is_ajax_request ()){
		
			if(empty($this->input->post ('id'))){
				show_404();
			}
			$project_id = $this->input->post ('id');
			$msg['location'] = '';
			if(check_session_validity()){ 
				$user = $this->session->userdata ('user');
				
				
				$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_expiration_date');
				$this->db->from('projects_open_bidding');
				$this->db->where('projects_open_bidding.project_id',$project_id);
				$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
                $project_data = $project_result->result_array();
				$project_expiration_date = $project_data[0]['project_expiration_date']!= NULL ? strtotime ($project_data[0]['project_expiration_date']) : 0;
				
				if($project_expiration_date < time()){
					
					$msg['status'] = 400;
					$msg['error'] = $this->config->item('project_expiration_validation_message');
					echo json_encode($msg);
					die;
				}
				
				
				$expiration_status = $this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				if($expiration_status){
				
					$im = base64_decode($this->input->post("image"));
					$original = base64_decode($this->input->post("original"));
					
					$tt = "cover_" . time();
					$image_name = $tt .'.png';
					$original_image_name = $tt . "_original.png"; 
					$ftp['image_name'] = $image_name;
					$ftp['org_image_name'] = $original_image_name;
					
					$ftp['dir_project_id'] = $project_id;
					$ftp['dir_profile'] = $user[0]->profile_name;
					$ftp['dir_projects'] = $this->config->item('projects_ftp_dir');
					$ftp['dir_open_for_bidding'] = $this->config->item('project_open_for_bidding_dir');
					$ftp['dir'][] = $this->config->item('featured_upgrade_cover_picture');
					$ftp['dirname'] = $this->config->item('featured_upgrade_cover_picture');
					
					//users/user_profile_namek/projects/open_for_bidding/project_id/featured_upgrade_cover_picture

					$picdata = $this->db->get_where('featured_projects_users_upload_cover_pictures_tracking', array('project_id' => $project_id))->row();
					if($picdata->project_cover_picture_name!='') {
						$ftp['cover_picture_file'] = $picdata->project_cover_picture_name;
						$ex = explode('.',$picdata->project_cover_picture_name);
						$ftp['cover_picture_org_file'] = $ex[0].'_original.png';
					}
				
					$this->load->helper("file");
					$newIamge = $this->resize_image($im, $this->input->post("width"), 500);
					file_put_contents(FCPATH .$this->config->item('temp_dir') . $image_name, $newIamge); 
					file_put_contents(FCPATH .$this->config->item('temp_dir') . $original_image_name, $original);

					$this->ftp_server_check($ftp);
					
					//start added remove existing file by diib
					$msg['status'] = 200;
					$msg['domain'] = $this->config->item('ftp_domainname');
					$msg['u_dir'] = $this->config->item('users_ftp_dir');
					$msg['cover_picture'] = $user[0]->profile_name . $this->config->item('projects_ftp_dir') . $this->config->item('project_open_for_bidding_dir') . $project_id. $this->config->item('featured_upgrade_cover_picture') . $image_name;;
				}else{
					$msg['status'] = 400;
					$msg['error'] = $this->config->item('featured_upgrade_type_expired_validation_message');
				}
				echo json_encode($msg);
				die;
			}else{
				$this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}	
		}else{
			show_404();
		}
    }
	
    public function ajax_featured_project_cropped_cover_picture_edit ()
    {
		if($this->input->is_ajax_request ()){
			if(check_session_validity()){ 
				error_reporting(E_ALL);
				ini_set('display_errors', 1);
				
				$project_id = $this->input->post ('id');
				
				$user = $this->session->userdata ('user');
				
				$field = $this->input->post ('field');
				$new_data = $this->input->post ('new_data');
				if($new_data!='') {
					$ex                     = explode('/',$new_data);
					$exp                    = count($ex)-1;
					$new_data             = $ex[$exp];
				}
					
				$cover_name = $this->input->post ('name');
				$updateArr = array (
					"project_id" => $project_id,
					$field => $new_data,
					"project_cover_picture_upload_date" => date('Y-m-d H:i:s')
				);
				
				//start added remove existing file by diib
				$this->load->helper("file");
				
				//for cover picture
			   
				$picdetails = $this->db->get_where('featured_projects_users_upload_cover_pictures_tracking', array('project_id' => $project_id))->row();
				//if($picdetails->project_cover_picture_name!='') {
				if(!empty($picdetails)) {
					/*$ex                     = explode('/',$picdetails->project_cover_picture_name);
					$exp                    = count($ex)-1;
					$image_name             = $ex[$exp];*/
					$image_name             = $picdetails->project_cover_picture_name;
					$expl                   = explode('.',$image_name);
					$original_image_name    = $expl[0].'_original.png';

					$ftp['image_name']      = $image_name;
					$ftp['org_image_name']  = $original_image_name;

					$ftp['dir_project_id'] = $project_id;
					$ftp['dir_profile'] = $user[0]->profile_name;
					$ftp['dir_projects'] = $this->config->item('projects_ftp_dir');
					$ftp['dir_open_for_bidding'] = $this->config->item('project_open_for_bidding_dir');
					$ftp['dirname'] = $this->config->item('featured_upgrade_cover_picture');

					
					if($new_data=='') {
						$this->ftp_file_del($ftp);
						$this->db->delete ('featured_projects_users_upload_cover_pictures_tracking', array ("project_id" => $project_id)); 
					} else {
						$this->db->update ('featured_projects_users_upload_cover_pictures_tracking', $updateArr, array ("project_id" => $project_id));  
					}
				} else if($new_data !=''){
					$this->db->insert ('featured_projects_users_upload_cover_pictures_tracking', $updateArr);  
				}
				echo $this->db->last_query();
			}else{
				show_404();
			}
				
		}else{
			show_404();
		
		}
       
    }
	
	//--- Cover Picture Upload Function Area End -----
	
	// This function is used two reset the project upgrade amount
	public function reset_project_upgrade_badge_amount_container(){
		if($this->input->is_ajax_request ()){
		
			if(empty($this->input->post('project_id'))){
				show_404();
			}
			$project_id = $this->input->post('project_id');
			if(check_session_validity()){ // check session exists or not if exist then it will update user session
			
				$this->db->where('project_id', $project_id);
				$project_data = $this->db->get('projects_draft')->row_array();
				
				if(empty($project_data)) { // if project not exists it will redirect to post project step1 page
					$res = [
						'status' => 400,
						'location'=>VPATH.$this->config->item('dasboard_page_url')
					];
					echo json_encode($res);
					die;
				}
				$user = $this->session->userdata('user');
				$user_id = $user[0]->user_id;
				
				$user_detail = $this->db->get_where('user_details', ['user_id' => $user_id])->row_array();
				$user_membership_plan_detail = $this->db->get_where('membership_plans', ['id' => $user_detail['current_membership_plan_id']])->row_array();
				
				
				$upgrade_type_featured_amount_html = '';
				$upgrade_type_urgent_amount_html = '';
				
				$count_user_featured_membership_included_upgrades_monthly = $this->Post_project_model->count_user_featured_membership_included_upgrades_monthly($user_id); // count user membership featured  upgrade
					
				$count_user_urgent_membership_included_upgrades_monthly = $this->Post_project_model->count_user_urgent_membership_included_upgrades_monthly($user_id);// count user membership urgent upgrade
				
				if($count_user_featured_membership_included_upgrades_monthly <$user_membership_plan_detail['included_number_featured_upgrades']){
				
					$upgrade_type_featured_amount_html = '<span><span id="upgrade_type_featured_amount" data-attr="0">Free</span></span>';
				
				}else{
				
					$upgrade_type_featured_amount_html = '<span><span id="upgrade_type_featured_amount" data-attr="'.number_format($this->config->item('project_upgrade_price_featured')).'">'.number_format($this->config->item('project_upgrade_price_featured')).'</span> Kč</span>';
				
				}
				if($count_user_urgent_membership_included_upgrades_monthly <$user_membership_plan_detail['included_number_urgent_upgrades']){
				
					$upgrade_type_urgent_amount_html = '<span><span id="upgrade_type_urgent_amount" data-attr="0">Free</span></span>';
				
				}else{
				
					$upgrade_type_urgent_amount_html = '<span><span id="upgrade_type_urgent_amount" data-attr="'.number_format($this->config->item('project_upgrade_price_urgent')).'">'.number_format($this->config->item('project_upgrade_price_urgent')).'</span> Kč</span>';
				
				}
					
				$msg['upgrade_type_featured_amount_html'] = $upgrade_type_featured_amount_html;
				$msg['upgrade_type_urgent_amount_html'] = $upgrade_type_urgent_amount_html;
				$msg['status'] = 'SUCCESS';
				$msg['upgrade_message'] = '';
				echo json_encode ($msg);
			}else{
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
			
		}else{
			show_404();
		}
	}
	
	
	public function check_user_project_expiration_featured_upgrade_expiration()
	{
		if($this->input->is_ajax_request ()){
			$msg['location'] = '';
			$project_id = $this->input->post ('id');
			if(empty($this->input->post ('id'))){
				show_404();
			}
			if(check_session_validity()){ 
				
				$user = $this->session->userdata ('user');
				
				$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_expiration_date');
				$this->db->from('projects_open_bidding');
				$this->db->where('projects_open_bidding.project_id',$project_id);
				$this->db->where('projects_open_bidding.project_owner_id',$user[0]->user_id);
				$project_result = $this->db->get();
                $project_data = $project_result->result_array();
				$project_expiration_date = $project_data[0]['project_expiration_date']!= NULL ? strtotime ($project_data[0]['project_expiration_date']) : 0;
				
				if($project_expiration_date < time()){
					
					$msg['status'] = 400;
					$msg['error'] = $this->config->item('project_expiration_validation_message');
					echo json_encode($msg);
					die;
				}
				$expiration_status = $this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				if($expiration_status){
					$msg['status'] = 200;
				}else{
					$msg['status'] = 400;
					$msg['error'] = $this->config->item('featured_upgrade_type_expired_validation_message');
				}
				echo json_encode($msg);
				die;
		
			}else{
				$this->Projects_model->get_featured_project_upgrade_expiration_status($project_id);
				$msg['status'] = 400;
				$msg['location'] = VPATH;
				echo json_encode($msg);
				die;
			}
		
		}else{
			show_404();
		}
	
	}
	
}