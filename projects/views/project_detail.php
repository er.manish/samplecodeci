<?php echo $this->load->view('header.php'); 
$user = $this->session->userdata('user');
$CI = & get_instance ();
$CI->load->library('Cryptor');
$payment_method = '';
$location = '';
if($project_data[0]['escrow_payment_method'] == 'Y'){
	$payment_method = 'via Escrow system';
	}
if($project_data[0]['offline_payment_method'] == 'Y'){
	$payment_method = 'via Offline system';
}
if(!empty($project_data[0]['county_name'])){
	if(!empty($project_data[0]['locality_name'])){
		$location .= '&nbsp'.$project_data[0]['locality_name'];
	}
	if(!empty($project_data[0]['postal_code'])){
		$location .= '&nbsp'.$project_data[0]['postal_code'] .',&nbsp';
	}else{
		$location .= ',&nbsp';
	}
	$location .= $project_data[0]['county_name'];
}
$featured_max = 0;
$project_expiration_date = $project_data[0]['project_expiration_date']!= NULL ? strtotime ($project_data[0]['project_expiration_date']) : 0;
if(!empty($project_data[0]['featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
}
if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
}
if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
}
if(!empty($expiration_featured_upgrade_date_array)){
	$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
}

?>
<div class="dashTop">
	<!-- Upload Image Cover Section Start -->
	<!-- <div class="uploadImg">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
				<div id="topContainer" class="bgposition" style="background-image: url('<?php echo URL ?>assets/images/post_project/banner.jpg');"></div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div> -->
	<?php
	$featured_upgrade_cover_picture_show_css = "display:none;";	
	//$upgrade_cover_picture_exist_status = false;	
	$upgrade_cover_picture = "";
	$upgrade_cover_picture_css = "";
	
	if($project_data[0]['featured'] == 'Y' && $featured_max >= time() && $project_expiration_date >= time() ){
		$featured_upgrade_cover_picture_show_css = "display:block;";	
	}
	
	if($upgrade_cover_picture_exist_status){
		//$upgrade_cover_picture_exist_status = true;
		$upgrade_cover_picture = 'https://'.$this->config->item('ftp_domainname').$this->config->item('users_ftp_dir').$project_data[0]['profile_name'].$this->config->item('projects_ftp_dir').$this->config->item('project_open_for_bidding_dir').$this->input->get(id).$this->config->item('featured_upgrade_cover_picture').$project_data[0]['project_cover_picture_name'];
		
		// Read image path, convert to base64 encoding
		//$imageData = base64_encode(file_get_contents($upgrade_cover_picture));
		// Format the image SRC:  data:{mime};base64,{data};
		//$upgrade_cover_picture = 'data: '.mime_content_type($upgrade_cover_picture).';base64,'.$imageData;
		$upgrade_cover_picture_css = "background-image:url('".$upgrade_cover_picture."');min-height: 400px;min-width: 100%;height: 35vh;max-height: 400px;"; 
	}
	?>
	<div class="uploadImg" style="<?php echo $featured_upgrade_cover_picture_show_css; ?>">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
			<div id="topContainer" class="bgposition" style="<?php echo $upgrade_cover_picture_css; ?>"></div>
			</div>
		</div>	
	</div>
	<div class="clearfix"></div>
	<!-- Upload Image Cover Section End -->		
	
	<!-- Middle Section Start -->
	<div class="row">
		<div class="col-md-2 col-sm-2 col-xs-12 pr0">				
			
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12 pojDet">
			<!-- Project Details Start -->				
			<div class="proDtls">
				<div class="pD">
					<strong>Project Details</strong>
					<?php
					if($project_expiration_date >= time()) {
					?>
					<span>Open For Bidding</span>
					<?php
					} else {
					?>
					<span>Expired</span>
					<?php		
					}
					?>
					<div class="clearfix"></div>
				</div>
				<div class="pDtls">
					<h4><?php echo $project_data[0]['project_title']; ?></h4>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-12 pDetailsL">
							<div class="pDSheduled">
								<label>
									<span>
										<i class="fa fa-clock-o" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_posted_on') ?> :
									</span>
									<small><?php echo date(DATE_TIME_FORMAT,strtotime($project_data[0]['project_posting_date']));?></small>
								</label>
							
								<label>
									<span>
										<i class="fa fa-clock-o" aria-hidden="true"></i>
										<?php
										if(strtotime($project_data[0]['project_expiration_date']) >= time()){
											echo $this->config->item('project_details_page_time_left') ?> 
										<?php
										}else{
											echo $this->config->item('project_details_page_expired_on');
										}
										?>
										:
									</span>
									<small>
									<?php
									if(strtotime($project_data[0]['project_expiration_date']) >= time()){
										echo secondsToWords(strtotime($project_data[0]['project_expiration_date']) -time());
										echo "<br/>";
										echo "<b>".$this->config->item('project_details_page_expires_on')."</b>";
									}
									echo "&nbsp;".date(DATE_TIME_FORMAT,strtotime($project_data[0]['project_expiration_date']));
									?>
									</small>
									<?php
									//}
									?>
								</label>
								<label>
									<span>
										<i class="fa fa-file-text-o" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_project_type') ?> :
									</span>
									<small>
									<?php
									//if($project_data[0]['project_type'] == 'fixed'){
										echo ucfirst($project_data[0]['project_type']). "&nbspBudget"; 
									//}
									?>		
									</small>
								</label>
								<label>
									<span>
										<i class="fa fa-credit-card" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_project_budget') ?> :
									</span>
									<small>
									<?php
									//if($project_data[0]['project_type'] == 'fixed'){
									if($project_data[0]['confidential_dropdown_option_selected'] == 'Y'){
										$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
									}else if($project_data[0]['not_sure_dropdown_option_selected'] == 'Y'){
										$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
									}else{
											if($project_data[0]['max_budget'] != 'All'){
												$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($project_data[0]['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($project_data[0]['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
												}else{
												$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($project_data[0]['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
											}
										}
									//}
									echo $budget_range;
									?>	
									</small>
								</label>
								<?php
								if(!empty($payment_method)){
								?>
								<label>
									<span>
										<i class="fa fa-credit-card" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_payment_method') ?> :
									</span>
									<small><?php echo $payment_method; ?></small>
								</label>
								<?php
								}
								?>
								<?php
								if(!empty($location)){
								?>
								<label>
									<span>
										<i class="fa fa-map-marker" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_location') ?> :
									</span>
									<small><?php echo $location; ?></small>
								</label>
								<?php
								}
								?>
								<label>
									<span>
										<i class="fa fa-bullhorn" aria-hidden="true"></i>
										<?php echo $this->config->item('project_details_page_bid_history') ?> :
									</span>
									<small>0 <?php echo $this->config->item('project_details_page_bids') ?></small>
								</label>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-12 pDetailsR">
							<div class="pProject">
								<?php
								if(!empty($project_category_data)){
									foreach($project_category_data as $category_key=>$category_value){
										if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
								?>
											<div class="clearfix">
												<small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
												<a href="#">
													<span><?php echo $category_value['category_name']; ?></span>
												</a>
											</div>
										
								<?php
										}else{
											echo '<small>'.$category_value['category_name'].'</small>'; 
										}
									}
								}
								?>
								<!--<div class="clearfix">
									<small class="pSmnu">IT, Web Design & Software Development</small>
									<a href="#">
										<span>Péče o domácnost a úklidové služby</span>
									</a>
								</div>-->
								<div class="">
									<div class="pApply">
										<?php if(!$this->session->userdata ('user')){ ?>
										<button type="button" class="btn">Apply Now</button>
										<?php }else if($this->session->userdata ('user') &&  ($user[0]->user_id != $project_data[0]['project_owner_id'])){ ?>
										<button type="button" class="btn applyNow">Apply Now</button>
										<?php 
										}
										?>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<?php
						if(strtotime($project_data[0]['project_expiration_date']) > strtotime(date('Y-m-d H:i:s'))) {
					?>
					<div class="pDBttm">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12 proDetS">
								<a href="#" class="fb_share_project" data-link="<?php echo base_url().$this->config->item('project_detail_page_url').'?id='.$project_data[0]['project_id'];?>"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter_share_project" data-link="<?php echo base_url().$this->config->item('project_detail_page_url').'?id='.$project_data[0]['project_id'];?>" data-title="<?php echo $project_data[0]['project_title'];?>" data-description="<?php echo limitString(strip_tags($project_data[0]['project_description']), $this->config->item('twitter_share_project_description_character_limit'));?>"><i class="fa fa-twitter"></i></a>
								<a href="#" class="ln_share_project" data-link="<?php echo base_url().$this->config->item('project_detail_page_url').'?id='.$project_data[0]['project_id'];?>" data-title="<?php echo $project_data[0]['project_title'];?>" data-description="<?php echo limitString(strip_tags($project_data[0]['project_description']), $this->config->item('facebook_and_linkedin_share_project_description_character_limit'));?>"><i class="fa fa-linkedin"></i></a>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-12">								
								<div class="pdButton">
									<?php
									if($project_data[0]['featured'] == 'Y'){
										echo '<button type="button" class="btn">Featured</button>';
									}if($project_data[0]['urgent'] == 'Y'){
										echo '<button type="button" class="btn urgent">Urgent</button>';
									}
									if($project_data[0]['sealed'] == 'Y'){
										echo '<button type="button" class="btn">Sealed</button>';
									}
									if($project_data[0]['hidden'] == 'Y'){
										echo '<button type="button" class="btn">Hidden</button>';
									}
									?>
								</div>
							</div>
						</div>
					</div>
					<?php
						}
					?>
				</div>
			</div>
			<!-- Project Details End -->
			
			<!-- Listing Id,History,Revisions and Report Violation Start -->
			<div class="pDLId">
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-12 lidL">
						<label>
							<b><?php echo $this->config->item('project_details_page_listing_id'); ?> :</b>
							<span><?php echo $project_data[0]['project_id']; ?></span>
						</label>
						<label>
							<b><?php echo $this->config->item('project_details_page_history'); ?> :</b>
							<span><?php echo $project_data[0]['views']; ?> <?php echo $this->config->item('project_details_page_views'); ?></span>
						</label>
						<label>
							<b><?php echo $this->config->item('project_details_page_revisions'); ?> :</b>
							<span><?php echo $project_data[0]['revisions']; ?></span>
						</label>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 lidR">
						<label><?php echo $this->config->item('project_details_page_report_violation'); ?></label>
					</div>
				</div>
			</div>
			<!-- Listing Id,History,Revisions and Report Violation End -->
			
			
			<!-- Description Start -->
			<div class="proDesc">
				<div class="pDesc">
					<strong>Description</strong>
					<div class="clearfix"></div>
				</div>
				<div class="proDn">
					<div class="proPart">
						<h6 class="line-break"><?php echo nl2br($project_data[0]['project_description']); ?></h6>
					</div>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<?php
							if(!empty($project_attachment_data)){
							?>
							<div class="pDAttach">
								<?php
								foreach($project_attachment_data as $project_attachment_key=>$project_attachment_value){
									$attachment_id = Cryptor::doEncrypt($project_attachment_value['id']);
									echo '<label><span><a href="javascript:void(0);" class="download_attachment" data-attr="'.$attachment_id.'">'.$project_attachment_value['project_attachment_name'].'</a></span></label>';
								}
								?>
							</div>
							<?php
							}
							?>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12"></div>						
						<div class="portTags">
							<div class="row">
								<div class="col-md-8 col-sm-8 col-xs-12">
									<?php
									if(!empty($project_tag_data)){
									?>
									<div class="smallTag">
										<?php
										foreach($project_tag_data as $project_tag_key=>$project_tag_value){
											echo '<p><span>#</span><small>'.$project_tag_value['project_tag_name'].'</small></p>';
										}
										?>
									</div>
									<?php
									}
									?>
								</div>
								
								<div class="col-md-4 col-sm-4 col-xs-12">										
									<div class="pApNow">
										<?php if(!$this->session->userdata ('user')){ ?>
										<button type="button" class="btn">Apply Now</button>
										<?php }else if($this->session->userdata ('user') &&  ($user[0]->user_id != $project_data[0]['project_owner_id'])){ ?>
										<button type="button" class="btn applyNow">Apply Now</button>
										<?php 
										}
										?>
									</div>
								</div>
							</div>
						</div>						
					</div>
					<?php if($this->session->userdata ('user') &&  ($user[0]->user_id != $project_data[0]['project_owner_id'])){ ?>
					<div class="row" id="onCApNw">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="bidDesc">
								<h3>There are many variations of passages of Lorem Ipsum available</h3>
								<div class="bidDays">
									<b>Bid:</b>
									<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="There are many variations of passages of Lorem Ipsum available, but the majority have suffered"></i>
									<span>123456789</span>
									<b>Kc Delivery In:</b>
									<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="There are many variations of passages of Lorem Ipsum available, but the majority have suffered"></i>
									<span>4999</span>
									<b>Days</b>
								</div>
								<div class="bidTArea">
									<b>Description:</b>
									<div class="form-group">
										<textarea class="form-control" rows="5" id="comment"></textarea>
										<small>100 characters remaining</small>
									</div>
									<div class="clearfix"></div>
									<div class="row">
										<div class="col-md-3 col-sm-3 col-xs-12">
											<div class="upBtn">
												<label for="files" class="btn">
													<i class="fa fa-cloud-upload" aria-hidden="true"></i>Upload a File
												</label>
												<input id="files" style="display: none;" type="file">
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="upValue">
												<label>business_financial.docx</label>
												<label>26kb</label>
												<label><i class="fa fa-trash-o" aria-hidden="true"></i></label>
												<div class="clearfix"></div>
											</div>
										</div>
										<div class="col-md-1 col-sm-1 col-xs-12"></div>
									</div>
								</div>
								<div class="canPB">									
									<button type="button" class="btn btn-primary cancelApply">Cancel</button>
									<button type="button" class="btn btn-secondary">Place Bid</button>
								</div>
							</div>
						</div>
					</div>	
					<?php
					}
					?>
				</div>
			</div>
			<!-- Description End -->
			
			<div class="opnPro">
				<div class="oP"><strong>Employer Details</strong></div>
			
				<div class="row">
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div class="empDtls">
							<label class="empName">
								<small>
								<?php
								$name = $project_data[0]['account_type'] == USER_PERSONAL_ACCOUNT_TYPE ? $project_data[0]['first_name'] . ' ' . $project_data[0]['last_name'] : $project_data[0]['company_name'];
								echo $name;
								?>
								<i class="fa fa-circle onLine" aria-hidden="true"></i></small>
							</label>
							<label class="empMem">
								Member Since <?php echo date(DATE_FORMAT,strtotime($project_data[0]['account_validation_date'])); ?>
							</label>
						</div>							
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 empRting">
						<div class="empDtls">
							<label>
								<span>
									<small>3.5</small>
									<i class="fa fa-star sR" aria-hidden="true"></i>
									<i class="fa fa-star sR" aria-hidden="true"></i>
									<i class="fa fa-star sR" aria-hidden="true"></i>
									<i class="fa fa-star-half-o sR" aria-hidden="true"></i>
									<i class="fa fa-star-o sR" aria-hidden="true"></i>
								</span>
							</label>
							<label>
								<b>4 Review</b>
							</label>
						</div>							
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 empSoc">
						<div class="empDtls">
							<label>
								<i class="fa fa-envelope vR" aria-hidden="true"></i>
								<i class="fa fa-phone vR" aria-hidden="true"></i>
								<i class="fa fa-facebook-official vR <?php echo (!empty($project_data[0]) && $project_data[0]['sync_facebook'] == 'n') ? 'greyCol' : ''; ?>" aria-hidden="true"></i>
								<i class="fa fa-linkedin-square vR <?php echo (!empty($project_data[0]) && $project_data[0]['sync_linkedin'] == 'n') ? 'greyCol' : ''; ?>" aria-hidden="true"></i>
							</label>
						</div>							
					</div>
				</div>
			</div>
			<?php
			if(!empty($project_data[0]['additional_information'])){
			?>
			<!-- Additional Information Start -->
			<div class="proAI">
				<div class="pDAI">
					<strong>Additional Information</strong>
					<div class="clearfix"></div>
				</div>
				<div class="pAI">
					<h6>
						<i class="fa fa-clock-o" aria-hidden="true"></i>
						Updated On :<span> <?php echo date(DATE_FORMAT,strtotime($project_data[0]['additional_information_add_date'])); ?></span>
					</h6>
					<p><?php echo $project_data[0]['additional_information']; ?></p>
				</div>
			</div>
			<!-- Additional Information End -->
			<?php
			}
			?>
			<div class="bidFreelance">
				<div class="fB"><strong>Freelancers Bidding</strong></div>
				<div class="bidFree">
					<div class="freeBid boder-color">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12 personImg">		
								<div class="imgTxtR">						
									<div class="imgSize">						
										<div class="avtOnly">
											<div id="profile-picture" class="profile-picture" style="background-image: url('<?php echo URL ?>assets/images/project_detail/1.png');">
											</div>
										</div>
										<div class="sRate">
											<small>3.5</small>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star-half-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
										</div>						
										<div class="rvw">
											<small>180 Reviews</small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-12 empRting">
								<div class="opLBttm opBg">
									<div>
										<label>SEO Expert</label>
										<label>bid amount: 300kc</label>
										<div class="clearfix"></div>
									</div>
									<p>
										<small><i class="far fa-calendar"></i> 24.07.2017 14:10:59</small>
										<small><i class="fa fa-map-marker"></i> Brno, Czech Republic</small>
									</p>
									<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't<span id="dots">...</span><span id="more"> look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum.</span><small class="rmRL" onclick="myFunction()" id="myBtn">Read more</small></p>
									<div class="acCo">
										<div class="row">
										<div class="col-sm-4">
											<small><i class="fa fa-check"></i> business-financial.docx</small>
											<small><i class="fa fa-check"></i> business-financial.docx</small>
										</div>
										<div class="col-sm-8">
											<button type="button" class="btn btn-primary">Edit Bid</button>
											<button type="button" class="btn btn-secondary">Retract Bid</button>
										</div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>									
							</div>
						</div>
					</div>
					<div class="freeBid">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12 personImg">		
								<div class="imgTxtR">						
									<div class="imgSize">						
										<div class="avtOnly">
											<div id="profile-picture" class="profile-picture" style="background-image: url('<?php echo URL ?>assets/images/project_detail/2.png');">
											</div>
										</div>
										<div class="sRate">
											<small>3.5</small>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star-half-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
										</div>						
										<div class="rvw">
											<small>180 Reviews</small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-12 empRting">
								<div class="opLBttm opBg">
									<div>
										<label>Kait</label>
										<label>bid amount: 200kc</label>
										<div class="clearfix"></div>
									</div>
									<p>
										<small><i class="far fa-calendar"></i> 24.07.2017 14:10:59</small>
										<small><i class="fa fa-map-marker"></i> Brno, Czech Republic</small>
									</p>
									<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't<span id="dots1">...</span><span id="more1"> look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum.</span><small class="rmRL" onclick="myFunction1()" id="myBtn1">Read more</small></p>
									<div class="acCo">
										<button type="button" class="btn btn-primary">Accept</button>
										<button type="button" class="btn btn-secondary">Contact</button>
									</div>
								</div>
								<div class="clearfix"></div>									
							</div>
						</div>
					</div>
					<div class="freeBid">
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-12 personImg">		
								<div class="imgTxtR">						
									<div class="imgSize">						
										<div class="avtOnly">
											<div id="profile-picture" class="profile-picture" style="background-image: url('<?php echo URL ?>assets/images/project_detail/3.png');">
											</div>
										</div>
										<div class="sRate">
											<small>3.5</small>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star-half-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
										</div>						
										<div class="rvw">
											<small>180 Reviews</small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-12 empRting">
								<div class="opLBttm opBg">
									<div>
										<label>Peter Parker</label>
										<label>bid amount: 100kc</label>
										<div class="clearfix"></div>
									</div>
									<p>
										<small><i class="far fa-calendar"></i> 24.07.2017 14:10:59</small>
										<small><i class="fa fa-map-marker"></i> Brno, Czech Republic</small>
									</p>
									<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't<span id="dots2">...</span><span id="more2"> look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum. look even slightly believable. If you are going to use a passage of Lorem Ipsum.</span><small class="rmRL" onclick="myFunction2()" id="myBtn2">Read more</small></p>
									<div class="acCo">
										<!--<button type="button" class="btn btn-primary">Accept</button>-->
										<!--<button type="button" class="btn btn-secondary">Contact</button>-->
									</div>
								</div>
								<div class="clearfix"></div>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Start Cover Image Button Start -->
		<!--<div class="col-md-2 col-sm-2 col-xs-12 pojBtn">
			<div class="projBtn">
				<button type="button" class="btn btn-primary">Reset Cover Picture</button>
				<button type="button" class="btn btn-secondary">Upload Cover Picture</button>
			</div>
		</div>-->
		<?php
		if($this->session->userdata ('user') && $user[0]->user_id == $project_data[0]['project_owner_id']){
		?>
			<div class="col-md-2 col-sm-2 col-xs-12 pojBtn" style="<?php echo $featured_upgrade_cover_picture_show_css; ?>">
				<div class="projBtn">
					<button id="reset_cover" onclick="resetCover();" style="<?php echo $upgrade_cover_picture_exist_status == true ? "display:block;" : "display:none;"?>" class="btn btn-danger">Reset Cover Picture</button>
					
					<button id="upload_cover" onclick="startUploadCover();" style="<?php echo $upgrade_cover_picture_exist_status == true ? "display:none;" : "display:block;"?>" class="btn">Upload Cover Picture</button>
					
					<button id="edit_cover" style="<?php echo $upgrade_cover_picture_exist_status == true ? "display:block;" : "display:none;"?>" class="btn">Edit Cover Picture</button>
					
					<button id="save_cover" style="display:none;" class="btn btn-success">Save Cover Picture</button>
					
					<button id="cancel_upload_cover" onclick="cancel_upload_cover();" style="display:none;" class="btn btn-danger">Cancel Upload</button>
					
					<button id="cancel_edit_cover" onclick="cancel_edit_cover();"  style="display:none;" class="btn btn-danger">Cancel Edit</button>
					
					<div id="show_instruction" class="show_text" style="display:none;">Use mouse wheel to zoom the image in/out.</div>
					<input type="file" accept=".gif,.png,.jpg,.jpeg" style="display:none;" id="images" />
					<input type="hidden" id="ww" value="0" />
					<input type="hidden" id="tw" value="" />
				</div>
			</div>
		<!-- Start Cover Image Button End -->
		<?php
		}
		?>
		
	</div>
	<?php if(!$this->session->userdata ('user')){ ?>
	<div class="regHire" id="clientsWrapper">
		<span class="closeDiv"><i class="fa fa-times" aria-hidden="true"></i></span>
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-3"></div>
			<div class="col-md-3 col-sm-3 col-xs-3 freReg">
				<button type="button" class="btn btn-primary signup">register as freelancer</button>
				<p>Start Working Right Now</p>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 freHir">
				<button type="button" class="btn btn-secondary signin">hire a freelancer</button>
				<p>Get the best freelancer in just minutes</p>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3"></div>
		</div>
	</div>
	<?php } ?>

	<!-- Middle Section End -->
</div>
	
<div class="modal alert-popup" id="error_popup" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close popup_close" data-dismiss="modal">&times;</button>
			  <div class="modal-header-inner">
				<img style="" src="<?=ASSETS?>images/post_project/alert-icon.png" alt="alert icon" class="img-fluid" />
				<h4 class="modal-title" id="error_popup_heading"></h4>
			  </div>
			</div>
			<div class="modal-body text-center">
			  <p id="error_popup_body"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger popup_close" data-dismiss="modal">Close</button>
			</div>
	    </div>
	</div>
</div>
<!-- Script Start -->
<script>
//var project_attachment_popup_error_heading = "<?php echo $this->config->item('project_attachment_popup_error_heading'); ?>";
var popup_error_heading = "<?php echo $this->config->item('popup_error_heading'); ?>";
var project_attachment_not_exist_temporary_project_preview_validation_post_project_message = "<?php echo $this->config->item('project_attachment_not_exist_temporary_project_preview_validation_post_project_message'); ?>";
var dashboard_page_url = "<?php echo $this->config->item('dashboard_page_url'); ?>";
var signup_page_url = "<?php echo $this->config->item('signup_page_url'); ?>";
var signin_page_url = "<?php echo $this->config->item('signin_page_url'); ?>";
var project_id = "<?php echo $project_id; ?>";
var project_status = "<?php echo 'open_for_bidding'; ?>";
var featured_project_cover_picture_maximum_size_validation_message = "<?php echo $this->config->item('featured_project_cover_picture_maximum_size_validation_message'); ?>";
var featured_project_cover_picture_extension_validation_message = "<?php echo $this->config->item('featured_project_cover_picture_extension_validation_message'); ?>";
var featured_project_cover_picture_size_validation_message = "<?php echo $this->config->item('featured_project_cover_picture_size_validation_message'); ?>";
var featured_project_cover_picture_loader_message = "<?php echo $this->config->item('featured_project_cover_picture_loader_message'); ?>";
var featured_project_cover_picture_zoom_in_out_message = "<?php echo $this->config->item('featured_project_cover_picture_zoom_in_out_message'); ?>";
</script>
<script src="<?php echo JS; ?>modules/project_detail.js"></script>
<?php echo $this->load->view('footer.php'); ?>
<script>

	$(".applyNow").on('click', function(){
		
		$("#onCApNw").show();
		
		$('html, body').animate({
			scrollTop: $("#onCApNw").offset().top-110
		 }, 2000);
		 
		$(".applyNow").hide();
	})
	$(".cancelApply").on('click', function(){
		$("#onCApNw").hide();
		$(".applyNow").show();
	})
	
</script>
	<!-- Bottom Div Closed Script Start -->
	<script>
		$('.closeDiv').on('click', function(){
			$(this).closest("#clientsWrapper").remove();
		});
	</script>
	<!-- Bottom Div Closed Script End -->
	<!-- Read More and Read Less Script End -->
	<script>
		function myFunction() {
		  var dots = document.getElementById("dots");
		  var moreText = document.getElementById("more");
		  var btnText = document.getElementById("myBtn");

		  if (dots.style.display === "none") {
			dots.style.display = "inline";
			btnText.innerHTML = "Read more"; 
			moreText.style.display = "none";
		  } else {
			dots.style.display = "none";
			btnText.innerHTML = "Read less"; 
			moreText.style.display = "inline";
		  }
		}
	</script>
	<script>
		function myFunction1() {
		  var dots = document.getElementById("dots1");
		  var moreText = document.getElementById("more1");
		  var btnText = document.getElementById("myBtn1");

		  if (dots.style.display === "none") {
			dots.style.display = "inline";
			btnText.innerHTML = "Read more"; 
			moreText.style.display = "none";
		  } else {
			dots.style.display = "none";
			btnText.innerHTML = "Read less"; 
			moreText.style.display = "inline";
		  }
		}
	</script>
	<script>
		function myFunction2() {
		  var dots = document.getElementById("dots2");
		  var moreText = document.getElementById("more2");
		  var btnText = document.getElementById("myBtn2");

		  if (dots.style.display === "none") {
			dots.style.display = "inline";
			btnText.innerHTML = "Read more"; 
			moreText.style.display = "none";
		  } else {
			dots.style.display = "none";
			btnText.innerHTML = "Read less"; 
			moreText.style.display = "inline";
		  }
		}
	</script>
	<!-- Read More and Read Less Script End -->
