<!-- Open Project Start -->
<?php
    $user = $this->session->userdata('user');
    if(!empty($open_bidding_project_left_side) || !empty($open_bidding_project_right_side)) {
?>
<div class="opnPro">
    <div class="oP"><strong>Latest Projects</strong></div>
    <!-- div class="oP">Latest Projects :<sup>______</sup></div -->
    <div class="row">
        <?php
            if($this->config->item('dashboard_left_projects') == 0 || $this->config->item('dashboard_right_projects') == 0 || empty($open_bidding_project_left_side) || empty($open_bidding_project_right_side)) {
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12 opL">
            <?php
                if($this->config->item('dashboard_left_projects') != 0 && !empty($open_bidding_project_left_side)) {
                    foreach($open_bidding_project_left_side as $key => $left_data) {
                        $featured_class = '';
                        if($left_data['featured'] == 'Y') {
                            $featured_class = 'opBg';
                        }
                        $location = '';
                        if(!empty($left_data['county_name'])){
                            if(!empty($left_data['locality_name'])){
                                $location .= '&nbsp'.$left_data['locality_name'];
                            }
                            if(!empty($left_data['postal_code'])){
                                $location .= ',&nbsp'.$left_data['postal_code'] .',&nbsp';
                            }else{
                                $location .= ',&nbsp';
                            }
                            $location .= $left_data['county_name'];
                        }
                        if(!empty($user) && $user[0]->user_id != $left_data['project_owner_id']) {
            ?>
            <div class="opLBttm <?=$featured_class?>">
				<div class="wiP">
                <a href="<?php echo base_url().$this->config->item('project_detail_page_url').'?id='.$left_data['project_id']?>">
                    <?=$left_data['project_title']?>
                </a>
				</div>
                <label>
                    <small><i class="far fa-clock"></i><?=date(DATE_TIME_FORMAT, strtotime($left_data['project_posting_date']))?></small>
					<small>
						<i class="far fa-credit-card"></i>
						<?php echo ucfirst($left_data['project_type']).' budget'; ?>
						<?php
						//if($left_data['project_type'] == 'fixed'){
							if($left_data['confidential_dropdown_option_selected'] == 'Y'){
								$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
							}else if($left_data['not_sure_dropdown_option_selected'] == 'Y'){
								$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
							}else{
									if($left_data['max_budget'] != 'All'){
										$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($left_data['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($left_data['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
										}else{
										$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($left_data['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}
								}
							//}
							echo $budget_range;
							if($left_data['escrow_payment_method'] == 'Y') {
								echo '- Payment Method: Safe Pay Escrow';
							}
						?>
					</small>
                    <?php
                      if(!empty($location)) :
                    ?>
                    <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                    <?php
                        endif;
                    ?>
                    <small><i class="fas fa-bullhorn"></i>0 Bid</small>
                </label>
                <div class="osu1">
                    <div class="onSlo1">
					<?php
				/* 	<?php
						$project_description_class='mpOne';
						if (strpos($left_data['project_description'], ' ') !== false) {
							$project_description_class='mpTwo';
						}
					?>
					<p class="<?php echo $project_description_class; ?>">
						<?php 
							$project_description_length = strlen($left_data['project_description']);
							$description = strip_tags($left_data['project_description']);
							echo substr(nl2br($description),0,450);
						?>
						
					</p> */
						$description = strip_tags($left_data['project_description']);
					?>
						<div class="project_description_my_project desktop-secreen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_dekstop'));
							?>
							</p>
						</div>
						<div class="project_description_my_project ipad-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_tablet'));
							?>
							</p>
						</div>
						<div class="project_description_my_project mobile-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_mobile'));
							?>
							</p>
						</div>
                    </div>
                </div>
                <div class="pProject">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 subRow">
                            <?php
                            if(!empty($left_data['categories'])) {
                                foreach($left_data['categories'] as $category_key=>$category_value){
                                    if($category_key <2){
                                        if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                        ?>
                                        <div class="clearfix">
                                            <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                            <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                        } else if(!empty($category_value['category_name'])) {
                                            echo '<small>'.$category_value['category_name'].'</small>'; 
                                        } else if(!empty($category_value['parent_category_name'])) {
                                            echo '<small>'.$category_value['parent_category_name'].'</small>'; 
                                        }
                                    }else{
                                        if($category_key == 2){
                                        ?>
                                            <div  class="collapse clearfix ldetails-<?php echo $left_data['project_id']?>">
                                    <?php } ?>
                                            <?php
                                            if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                            ?>
                                            <div class="clearfix">
                                                <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                                <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                                </a>
                                            </div>
                                            <?php
                                            } else if(!empty($category_value['category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['category_name']; ?></small>
                                            <?php
                                            } else if(!empty($category_value['parent_category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['parent_category_name']; ?></small>
                                            <?php    
                                            }
                                            ?>
                                        
                                        <?php
                                            if(count($left_data['categories']) == ($category_key+1)){
                                            ?></div>
                                            <div class="text-center">
                                            <button  type="button" class="btn opSL" data-toggle="collapse" data-target=".ldetails-<?php echo $left_data['project_id']?>">Show More <i class="fas fa-angle-down"></i></button>
                                            </div>
                                        
                                        <?php
                                            }
                                        
                                        ?>
                                    
                                    <?php
                                    
                                    }
                                }
                            ?>
                                
                                
                            <?php
                            }
                            ?>	
                            
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12"></div>
                    </div>
                </div>
                <div class="clearfix"></div>						
                <div class="pDBttm">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12 proDetS">
                            <a href="#" class="fb_share_project"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter_share_project" data-title="<?php echo $left_data['project_title']; ?>" data-description="<?php echo limitString(strip_tags($left_data['project_description']), $this->config->item('twitter_share_project_description_character_limit'));?>"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="ln_share_project" data-title="<?php echo $left_data['project_title'];?>" data-description="<?php echo limitString(strip_tags($left_data['project_description']), $this->config->item('facebook_and_linkedin_share_project_description_character_limit'))?>"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">									
                            <div class="pdButton">
                                <?php
                                    if($left_data['featured'] == 'Y') { 
                                ?>
                                <button type="button" class="btn">Featured</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($left_data['urgent'] == 'Y') {
                                ?>
                                <button type="button" class="btn urgent">Urgent</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($left_data['sealed'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Sealed</button>
                                <?php
                                    }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">	
                            <?php
                                if(!empty($user) && $user[0]->user_id != $left_data['project_owner_id']) :
                            ?>
                            <div class="fjApply">
                                <button type="button" data-url="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$left_data['project_id']; ?>" class="btn">Apply Now</button>
                            </div>
                            <?php
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
                        }
                    }
                } else {
                    foreach($open_bidding_project_right_side as $key => $right_data) {
                        $location = '';
                        $featured_class = '';
                        if($right_data['featured'] == 'Y') {
                            $featured_class = 'opBg';
                        }
                        if(!empty($right_data['county_name'])){
                            if(!empty($right_data['locality_name'])){
                                $location .= '&nbsp'.$right_data['locality_name'];
                            }
                            if(!empty($right_data['postal_code'])){
                                $location .= ',&nbsp'.$right_data['postal_code'] .',&nbsp';
                            }else{
                                $location .= ',&nbsp';
                            }
                            $location .= $right_data['county_name'];
                        }
                        if(!empty($user) && $user[0]->user_id != $right_data['project_owner_id']) {
            ?>
            <div class="opLBttm <?php echo $featured_class;?>">
				<div class="wiP">
                <a href="<?=base_url().$this->config->item('project_detail_page_url').'?id='.$right_data['project_id']?>">
                    <?=$right_data['project_title']?>
                </a>	
				</div>
                <label>
                    <small><i class="far fa-clock"></i><?=date(DATE_TIME_FORMAT, strtotime($right_data['project_posting_date']))?></small>
                   
					<small>
						<i class="far fa-credit-card"></i>
						<?php echo ucfirst($right_data['project_type']).' budget'; ?>
						<?php
						//if($right_data['project_type'] == 'fixed'){
							if($right_data['confidential_dropdown_option_selected'] == 'Y'){
								$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
							}else if($right_data['not_sure_dropdown_option_selected'] == 'Y'){
								$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
							}else{
									if($right_data['max_budget'] != 'All'){
										$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($right_data['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($right_data['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
										}else{
										$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($right_data['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}
								}
							//}
							echo $budget_range;
							if($right_data['escrow_payment_method'] == 'Y') {
								echo '- Payment Method: Safe Pay Escrow';
							}
						?>
					</small>
                    <?php
                      if(!empty($location)) :
                    ?>
                    <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                    <?php
                        endif;
                    ?>
                    <small><i class="fas fa-bullhorn"></i>0 Bid</small>
                </label>
                <?php
                  /*   $project_description_class='mpOne';
                    if (strpos($right_data['project_description'], ' ') !== false) {
                        $project_description_class='mpTwo';
                    } */
                ?>
                <div class="osu1">
                    <div class="onSlo1">
						<?php
						/* <p class="<?php echo $project_description_class; ?>">
							<?php 
								$project_description_length = strlen($right_data['project_description']);
								$description = strip_tags($right_data['project_description']);
								echo substr(nl2br($description),0,450);
							?>
							
						</p> */
						$description = strip_tags($right_data['project_description']);
						?>
						<div class="project_description_my_project desktop-secreen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_dekstop'));
							?>
							</p>
						</div>
						<div class="project_description_my_project ipad-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_tablet'));
							?>
							</p>
						</div>
						<div class="project_description_my_project mobile-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_mobile'));
							?>
							</p>
						</div>
                    </div>
                </div>
                <div class="pProject">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 subRow">
                            <?php
                            if(!empty($right_data['categories'])) {
                                foreach($right_data['categories'] as $category_key=>$category_value){
                                    if($category_key <2){
                                        if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                        ?>
                                        <div class="clearfix">
                                            <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                            <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                        } else if(!empty($category_value['category_name'])) {
                                            echo '<small>'.$category_value['category_name'].'</small>'; 
                                        } else if(!empty($category_value['parent_category_name'])) {
                                            echo '<small>'.$category_value['parent_category_name'].'</small>'; 
                                        }
                                    }else{
                                        if($category_key == 2){
                                        ?>
                                            <div  class="collapse clearfix rdetails-<?php echo $right_data['project_id']?>">
                                    <?php } ?>
                                            <?php
                                            if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                            ?>
                                            <div class="clearfix">
                                                <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                                <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                                </a>
                                            </div>
                                            <?php
                                            } else if(!empty($category_value['category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['category_name']; ?></small>
                                            <?php
                                            } else if(!empty($category_value['parent_category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['parent_category_name']; ?></small>
                                            <?php    
                                            }
                                            ?>
                                        
                                        <?php
                                            if(count($right_data['categories']) == ($category_key+1)){
                                            ?></div>
                                            <div class="text-center">
                                            <button  type="button" class="btn opSL" data-toggle="collapse" data-target=".rdetails-<?php echo $right_data['project_id']?>">Show More <i class="fas fa-angle-down"></i></button>
                                            </div>
                                        
                                        <?php
                                            }
                                        
                                        ?>
                                    
                                    <?php
                                    
                                    }
                                }
                            ?>
                                
                                
                            <?php
                            }
                            ?>	
                            
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12"></div>
                    </div>
                </div>
                <div class="clearfix"></div>						
                <div class="pDBttm">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12 proDetS">
							<div class="ftlS">
								<a href="#" class="fb_share_project"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter_share_project" data-title="<?php echo $right_data['project_title']; ?>" data-description="<?php echo limitString(strip_tags($right_data['project_description']), $this->config->item('twitter_share_project_description_character_limit'));?>"><i class="fa fa-twitter"></i></a>
								<a href="#" class="ln_share_project" data-title="<?php echo $right_data['project_title'];?>" data-description="<?php echo limitString(strip_tags($right_data['project_description']), $this->config->item('facebook_and_linkedin_share_project_description_character_limit'))?>"><i class="fa fa-linkedin"></i></a>
							</div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">									
                            <div class="pdButton">
                                <?php
                                    if($right_data['featured'] == 'Y') { 
                                ?>
                                <button type="button" class="btn">Featured</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($right_data['urgent'] == 'Y') {
                                ?>
                                <button type="button" class="btn urgent">Urgent</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($right_data['sealed'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Sealed</button>
                                <?php
                                    }
                                ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 pdApN">	
                            <?php
                                if(!empty($user) && $user[0]->user_id != $right_data['project_owner_id']) :
                            ?>
                            <div class="fjApply">
                                <button type="button" data-url="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$right_data['project_id']; ?>" class="btn">Apply Now</button>
                            </div>
                            <?php
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
                        }
                    }
                }
            ?>
        </div>
        <?php
            } else {
        ?>
        <div class="col-md-6 col-sm-6 col-xs-12 opL">
            <?php
                foreach($open_bidding_project_left_side as $key => $left_data) {
                    $featured_class = '';
                    if($left_data['featured'] == 'Y' ) {
                        $featured_class = 'opBg';
                    }
                    $location = '';
                    if(!empty($left_data['county_name'])){
                        if(!empty($left_data['locality_name'])){
                            $location .= '&nbsp'.$left_data['locality_name'];
                        }
                        if(!empty($left_data['postal_code'])){
                            $location .= ',&nbsp'.$left_data['postal_code'] .',&nbsp';
                        }else{
                            $location .= ',&nbsp';
                        }
                        $location .= $left_data['county_name'];
                    }
                    if(!empty($user) && $user[0]->user_id != $left_data['project_owner_id']) {
            ?>
            <div class="opLBttm <?=$featured_class?>">
                <a href="<?=base_url().$this->config->item('project_detail_page_url').'?id='.$left_data['project_id']?>">
                    <?=$left_data['project_title']?>
                </a>
                <!-- <span class="opOpen">Open</span> -->
                <label>
                    <small><i class="far fa-clock"></i><?=date(DATE_TIME_FORMAT, strtotime($left_data['project_posting_date']))?></small>
                    <small>
                    <i class="far fa-credit-card"></i><?=ucfirst($left_data['project_type']).'&nbsp;'.$this->config->item('post_project_budget')?>
                    <?php
                    if($left_data['max_budget'] != 'All'){
                        $budget_range = $this->config->item('post_project_budget_range_between').'&nbsp'.number_format($left_data['min_budget'], 0, '', ' '). '&nbsp'.CURRENCY.$this->config->item('post_project_budget_range_and').'&nbsp'.number_format($left_data['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
                        }else{
                        $budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($left_data['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
                    }
                    echo $budget_range;
                    ?>
                    <?php
                        if($left_data['escrow_payment_method'] == 'Y') {
                            echo '- Payment Method: Safe Pay Escrow';
                        }
                    ?>
                    </small>
                    <?php
                      if(!empty($location)) :
                    ?>
                    <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                    <?php
                        endif;
                    ?>
                    <small><i class="fas fa-bullhorn"></i>0 Bid</small>
                </label>
                <div class="osu1">
                    <div class="onSlo1">
                <?php
                   /*  $project_description_class='mpOne';
                    if (strpos($left_data['project_description'], ' ') !== false) {
                        $project_description_class='mpTwo';
                    } */
                ?>
				<?php
              /*   <p class="<?php echo $project_description_class; ?>">
                    <?php 
                        $project_description_length = strlen($left_data['project_description']);
                        $description = strip_tags($left_data['project_description']);
                        echo substr(nl2br($description),0,450);
                    ?>
                    
                </p> */
				$description = strip_tags($left_data['project_description']);
				?>
					<div class="project_description_my_project desktop-secreen">
					
						<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_dekstop'));
							?>
							</p>
						</div>
						<div class="project_description_my_project ipad-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_tablet'));
							?>
							</p>
						</div>
						<div class="project_description_my_project mobile-screen">
							<p><?php 
							echo limitString($description,$this->config->item('dashboard_latest_projects_section_project_description_character_limit_mobile'));
							?>
							</p>
						</div>
                    </div>
                </div>
                <!-- <h6>Pulished In :</h6> -->
                <div class="pProject">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 subRow">
                            <?php
                            if(!empty($left_data['categories'])) {
                                foreach($left_data['categories'] as $category_key=>$category_value){
                                    if($category_key <2){
                                        if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                        ?>
                                        <div class="clearfix">
                                            <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                            <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                        } else if(!empty($category_value['category_name'])) {
                                            echo '<small>'.$category_value['category_name'].'</small>'; 
                                        } else if(!empty($category_value['parent_category_name'])) {
                                            echo '<small>'.$category_value['parent_category_name'].'</small>'; 
                                        }
                                    }else{
                                        if($category_key == 2){
                                        ?>
                                            <div class="collapse clearfix ldetails-<?php echo $left_data['project_id']?>">
                                    <?php } ?>
                                            <?php
                                            if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                            ?>
                                            <div class="clearfix">
                                                <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                                <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                                </a>
                                            </div>
                                            <?php
                                            } else if(!empty($category_value['category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['category_name']; ?></small>
                                            <?php
                                            } else if(!empty($category_value['parent_category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['parent_category_name']; ?></small>
                                            <?php    
                                            }
                                            ?>
                                        
                                        <?php
                                            if(count($left_data['categories']) == ($category_key+1)){
                                            ?></div>
                                            <div class="text-center">
                                            <button  type="button" class="btn opSL" data-toggle="collapse" data-target=".ldetails-<?php echo $left_data['project_id']?>">Show More <i class="fas fa-angle-down"></i></button>
                                            </div>
                                        
                                        <?php
                                            }
                                        
                                        ?>
                                    
                                    <?php
                                    
                                    }
                                }
                            ?>
                                
                                
                            <?php
                            }
                            ?>	
                            
                        </div>
                    </div>
                    <!--div id="opShLsr<?=$key?>" class="collapse clearfix">
                        <small>Web Design Development</small>
                        <small>Application Software Development</small>
                    </div>
                    <div class="text-center">
                        <button id="opSL" type="button" class="btn opSL collapsed" data-toggle="collapse" data-target="#opShLsr<?=$key?>" aria-expanded="false">Show More <i class="fas fa-angle-down"></i></button>
                    </div-->
                </div>
                <div class="clearfix"></div>						
                <div class="pDBttm">
                    <div class="">
                        <div class="proDetS">
                            <a href="#" class="fb_share_project"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter_share_project" data-title="<?php echo $left_data['project_title']; ?>" data-description="<?php echo limitString(strip_tags($left_data['project_description']), $this->config->item('twitter_share_project_description_character_limit'));?>"><i class="fa fa-twitter"></i></a>
							<a href="#" class="ln_share_project" data-title="<?php echo $left_data['project_title'];?>" data-description="<?php echo limitString(strip_tags($left_data['project_description']), $this->config->item('facebook_and_linkedin_share_project_description_character_limit'))?>"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <div class="pRnone">									
                            <div class="pdButton">
                                <?php
                                    if($left_data['featured'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Featured</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($left_data['urgent'] == 'Y') {
                                ?>
                                <button type="button" class="btn urgent">Urgent</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($left_data['sealed'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Sealed</button>
                                <?php
                                    }
                                ?>
                                <!--button type="button" class="btn urgent">Full Time</button-->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="pRApply">
                            <?php
                                if(!empty($user) && $user[0]->user_id != $left_data['project_owner_id']) :
                            ?>
                            <div class="fjApply">
                                <button type="button" data-url="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$left_data['project_id']; ?>" class="btn">Apply Now</button>
                            </div>
                            <?php
                                endif;
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
                    }
                }
            ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 opR">
            <?php
                foreach($open_bidding_project_right_side as $key => $right_data) {
                    $location = '';
                    $featured_class = '';
                        if($right_data['featured'] == 'Y') {
                            $featured_class = 'opBg';
                        }
                    if(!empty($right_data['county_name'])){
                        if(!empty($right_data['locality_name'])){
                            $location .= '&nbsp'.$right_data['locality_name'];
                        }
                        if(!empty($right_data['postal_code'])){
                            $location .= ',&nbsp'.$right_data['postal_code'] .',&nbsp';
                        }else{
                            $location .= ',&nbsp';
                        }
                        $location .= $right_data['county_name'];
                    }
                    if(!empty($user) && $user[0]->user_id != $right_data['project_owner_id']) {
            ?>
            <div class="opLBttm <?php echo $featured_class; ?>">
                <a href="<?=base_url().$this->config->item('project_detail_page_url').'?id='.$right_data['project_id']?>">
                    <?=$right_data['project_title']?>
                </a>
                <!-- <span class="opOpen">Open</span> -->
                <label>
                    <small><i class="far fa-clock"></i><?=date(DATE_TIME_FORMAT, strtotime($right_data['project_posting_date']))?></small>
                    <small>
                    <i class="far fa-credit-card"></i><?=ucfirst($right_data['project_type']).'&nbsp;'.$this->config->item('post_project_budget')?>
                    <?php
                    if($right_data['max_budget'] != 'All'){
                        $budget_range = $this->config->item('post_project_budget_range_between').'&nbsp'.number_format($right_data['min_budget'], 0, '', ' '). '&nbsp'.CURRENCY.$this->config->item('post_project_budget_range_and').'&nbsp'.number_format($right_data['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
                        }else{
                        $budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($right_data['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
                    }
                    echo $budget_range;
                    ?>
                    <?php
                        if($right_data['escrow_payment_method'] == 'Y') {
                            echo '- Payment Method: Safe Pay Escrow';
                        }
                    ?>
                        
                    </small>
                    <?php
                      if(!empty($location)) :
                    ?>
                    <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                    <?php
                        endif;
                    ?>
                    <small><i class="fas fa-bullhorn"></i>0 Bid</small>
                </label>
                <div class="osu1">
                    <div class="onSlo1">
                <?php
                    $project_description_class='mpOne';
                    if (strpos($right_data['project_description'], ' ') !== false) {
                        $project_description_class='mpTwo';
                    }
                ?>
                <p class="<?php echo $project_description_class; ?>">
                    <?php 
                        $project_description_length = strlen($right_data['project_description']);
                        $description = strip_tags($right_data['project_description']);
                        echo substr(nl2br($description),0,450);
                    ?>
                    
                </p>
                    </div>
                </div>
                <!-- <h6>Pulished In :</h6> -->
                <div class="pProject">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 subRow">
                            <?php
                            if(!empty($right_data['categories'])) {
                                foreach($right_data['categories'] as $category_key=>$category_value){
                                    if($category_key <2){
                                        if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                        ?>
                                        <div class="clearfix">
                                            <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                            <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                            </a>
                                        </div>
                                        <?php
                                        } else if(!empty($category_value['category_name'])) {
                                            echo '<small>'.$category_value['category_name'].'</small>'; 
                                        } else if(!empty($category_value['parent_category_name'])) {
                                            echo '<small>'.$category_value['parent_category_name'].'</small>'; 
                                        }
                                    }else{
                                        if($category_key == 2){
                                        ?>
                                            <div  class="collapse clearfix rdetails-<?php echo $right_data['project_id']?>">
                                    <?php } ?>
                                            <?php
                                            if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                            ?>
                                            <div class="clearfix">
                                                <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                                <a href="#">
                                                <span><?php echo $category_value['category_name']; ?></span>
                                                </a>
                                            </div>
                                            <?php
                                            } else if(!empty($category_value['category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['category_name']; ?></small>
                                            <?php
                                            } else if(!empty($category_value['parent_category_name'])) {
                                            ?>
                                                <small><?php echo $category_value['parent_category_name']; ?></small>
                                            <?php    
                                            }
                                            ?>
                                        
                                        <?php
                                            if(count($right_data['categories']) == ($category_key+1)){
                                            ?></div>
                                            <div class="text-center">
                                            <button  type="button" class="btn opSL" data-toggle="collapse" data-target=".rdetails-<?php echo $right_data['project_id']?>">Show More <i class="fas fa-angle-down"></i></button>
                                            </div>
                                        
                                        <?php
                                            }
                                        
                                        ?>
                                    
                                    <?php
                                    
                                    }
                                }
                            ?>
                                
                                
                            <?php
                            }
                            ?>	
                            
                        </div>
                    </div>
                    <!--div id="opShLsr<?=$key?>" class="collapse clearfix">
                        <small>Web Design Development</small>
                        <small>Application Software Development</small>
                    </div>
                    <div class="text-center">
                        <button id="opSL" type="button" class="btn opSL collapsed" data-toggle="collapse" data-target="#opShLsr<?=$key?>" aria-expanded="false">Show More <i class="fas fa-angle-down"></i></button>
                    </div-->
                </div>
                <div class="clearfix"></div>						
                <div class="pDBttm">
                    <div class="">
                        <div class="proDetS">
                            <a href="#" class="fb_share_project"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter_share_project" data-title="<?php echo $right_data['project_title']; ?>" data-description="<?php echo limitString(strip_tags($right_data['project_description']), $this->config->item('twitter_share_project_description_character_limit'));?>"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="ln_share_project" data-title="<?php echo $right_data['project_title'];?>" data-description="<?php echo limitString(strip_tags($right_data['project_description']), $this->config->item('facebook_and_linkedin_share_project_description_character_limit'))?>"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <div class="pRnone">									
                            <div class="pdButton">
                                <?php
                                    if($right_data['featured'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Featured</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($right_data['urgent'] == 'Y') {
                                ?>
                                <button type="button" class="btn urgent">Urgent</button>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($right_data['sealed'] == 'Y') {
                                ?>
                                <button type="button" class="btn">Sealed</button>
                                <?php
                                    }
                                ?>
                                <!--button type="button" class="btn urgent">Full Time</button-->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="pRApply">
                            <?php
                                if(!empty($user) && $user[0]->user_id != $right_data['project_owner_id']) :
                            ?>
                            <div class="fjApply">
                                <button type="button" data-url="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$right_data['project_id']; ?>" class="btn">Apply Now</button>
                            </div>
                            <?php
                                endif;
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
                    }
                }
            ?>
        </div>
        <?php
            }
        ?>
    </div>
</div>

<?php
    }
?>
<!-- Open Project End -->