<?php
	$awaiting_moderation_project_button_show_status = "display:block";
if(!empty($awaiting_moderation_project_data)){
	$awaiting_moderation_project_button_show_status = "display:none";
	foreach($awaiting_moderation_project_data as $awaiting_moderation_project_key=>$awaiting_moderation_project_value){
		$featured_class = '';
		if($awaiting_moderation_project_value['featured'] == 'Y' ) {
			$featured_class = 'opBg';
		}
		$location = '';
		if(!empty($awaiting_moderation_project_value['county_name'])){
		if(!empty($awaiting_moderation_project_value['locality_name'])){
			$location .= '&nbsp'.$awaiting_moderation_project_value['locality_name'];
		}
		if(!empty($awaiting_moderation_project_value['postal_code'])){
			$location .= ',&nbsp'.$awaiting_moderation_project_value['postal_code'] .',&nbsp';
		}else{
			$location .= ',&nbsp';
		}
		$location .= $awaiting_moderation_project_value['county_name'];
		}
		
?>
	<div class="tabContent">
		<div class="opLBttm <?php echo $featured_class; ?>">
			<div class="wiP"><a href="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$awaiting_moderation_project_value['project_id']; ?>">
				<?php echo $awaiting_moderation_project_value['project_title']; ?>
			</a></div>
		
			<label>
				<small><i class="far fa-clock"></i>
					<?php 
						if(isset($project_status) && $project_status == 'cancelled' || $project_status == 'expired') {
							echo date(DATE_TIME_FORMAT,strtotime($awaiting_moderation_project_value['project_posting_date']));
						} else {
							echo date(DATE_TIME_FORMAT,strtotime($awaiting_moderation_project_value['project_submission_to_moderation_date'])) ;
						}
						
					?>
				</small>
				<small>
					<i class="far fa-credit-card"></i>
					<?php echo ucfirst($awaiting_moderation_project_value['project_type']).' budget'; ?>
					<?php
					//if($awaiting_moderation_project_value['project_type'] == 'fixed'){
						if($awaiting_moderation_project_value['confidential_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
						}else if($awaiting_moderation_project_value['not_sure_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
						}else{
								if($awaiting_moderation_project_value['max_budget'] != 'All'){
									$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($awaiting_moderation_project_value['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($awaiting_moderation_project_value['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}else{
									$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($awaiting_moderation_project_value['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
								}
							}
						//}
						echo $budget_range;
						if($awaiting_moderation_project_value['escrow_payment_method'] == 'Y') {
							echo '- Payment Method: Safe Pay Escrow';
						}
					?>
				</small>
				<?php
				if(!empty($location)){
				?>
				<small><i class="fas fa-map-marker-alt"></i><?php echo $location;?></small>
				<?php
				}
				?>
				<small><i class="fas fa-bullhorn"></i>0 Bid</small>
			</label>
			
			<div class="osu1">
				<?php
				$description = strip_tags($awaiting_moderation_project_value['project_description']);
				?>
				<div class="project_description_my_project desktop-secreen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_dekstop'));
					?>
					</p>
				</div>
				<div class="project_description_my_project ipad-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_tablet'));
					?>
					</p>
				</div>
				<div class="project_description_my_project mobile-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_mobile'));
					?>
					</p>
				</div>
			</div>
			
			<div class="clearfix"></div>
			
			<div class="pDBttm">
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-12 baDges">									
						<div class="pdButton">
							<?php
								if($awaiting_moderation_project_value['featured'] == 'Y') {
							?>
							<button type="button" class="btn">Featured</button>
							<?php
								}
							?>
							<?php
								if($awaiting_moderation_project_value['urgent'] == 'Y') {
							?>
							<button type="button" class="btn urgent">Urgent</button>
							<?php
								}
							?>
							<?php
								if($awaiting_moderation_project_value['sealed'] == 'Y') {
							?>
							<button type="button" class="btn">Sealed</button>
							<?php
								}
							?>
						</div>
					</div>
					<!--div class="col-md-3 col-sm-3 col-xs-12 actOnly">
						<div class="myAction">
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Awaiting Moderation</a>
									<a class="dropdown-item" href="#">Editeaza Draft</a>
								</div>
							</div>
						</div>
					</div-->
				</div>
			</div>
		</div>
	</div>

	
<?php
	}
}
?>

<?php
	if($awaiting_moderation_project_cnt > $this->config->item('user_dashboard_awaiting_moderation_projects_listing_limit') && $project_status != 'expired' && $project_status != 'cancelled') {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
	<a href="">view more</a>
</div>
<?php
	}
?>

<?php
	if($expired_project_cnt > $this->config->item('user_dashboard_expired_projects_listing_limit') && $project_status == 'expired') {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
	<a href="">view more</a>
</div>
<?php
	}
?>

<?php
	if($cancelled_project_cnt > $this->config->item('user_dashboard_cancelled_projects_listing_limit') && $project_status == 'cancelled') {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
	<a href="">view more</a>
</div>
<?php
	}
?>

<div class="mpNoPrj" style="<?php echo $awaiting_moderation_project_button_show_status ?>">
	<h5>
		<?php
			if(!empty($project_status) && $project_status == 'expired') {
				echo $this->config->item('no_expired_project_message');
			} else if (!empty($project_status) && $project_status == 'cancelled') {
				echo $this->config->item('no_cancelled_project_message');
			} else {
				echo $this->config->item('no_awaiting_moderation_project_message');
			}
		?>
		
	</h5>
</div>