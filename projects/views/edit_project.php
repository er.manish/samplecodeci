<?php
$CI = & get_instance ();
$CI->load->library('Cryptor');
$selected_category_array = array();

if(!empty($project_category_data)){
	foreach($project_category_data as $category_key){
		$selected_category_array[] = $category_key['project_category_id'];
	}
}
$count_project_attachments= count($project_attachment_array);
$project_upgrade_type_box_show_status = false;	
$featured_max = 0;
$urgent_max = 0;
$expiration_featured_upgrade_date_array = array();
$expiration_urgent_upgrade_date_array = array();

if(!empty($project_data[0]['featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
}
if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
}
if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
}
if(!empty($expiration_featured_upgrade_date_array)){
	$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
}

if(!empty($project_data[0]['urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['urgent_upgrade_end_date'];
}
if(!empty($project_data[0]['bounus_urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['bounus_urgent_upgrade_end_date'];
}
if(!empty($project_data[0]['membership_include_urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['membership_include_urgent_upgrade_end_date'];
}
if(!empty($expiration_urgent_upgrade_date_array)){
	$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
}



if($project_data[0]['featured'] == 'N' && $featured_max < time() ){
	$project_upgrade_type_box_show_status = true;	
}
	
if($project_data[0]['urgent'] == 'N' && $urgent_max < time() ){
	$project_upgrade_type_box_show_status = true;	
}	
	
##########





?>
<main>
  <!-- header section -->
  <div class="top-hedaer-sectn">
	<div class="container-fluid">
	  <div class="row">
		<div class="col-sm-12">
		  <figure>
			<?php 
			if($this->session->userdata ('user')){ 
				$logo_redirect_url = base_url().$this->config->item('dashboard_page_url');
			}else{ 
				$logo_redirect_url = base_url().$this->config->item('signin_page_url');
			}
			?>
			<a href="<?php echo $logo_redirect_url ; ?>"><img src="<?=ASSETS?>images/site-inner-logo.svg" alt="logo"></a>
		</figure>
		</div>
	  </div>
	</div>
  </div>
  <!-- end header section -->

  <div class="create-your-project">
	<div class="container-fluid">
	  <div class="row">
		<div class="col-xl-8">
		<?php
		 $attributes = [
			 'id' => 'edit_project_form',
			 'class' => '',
			 'role' => 'form',
			 'name' => 'edit_project_form',
			 'enctype' => 'multipart/form-data',
		 ];
		 echo form_open('', $attributes);
		 ?>
			<input type="hidden" name="project_id" value="<?php echo $project_id; ?>"/>
			<input type="hidden" name="page_type" value="form"/>
			<div class="box-overly">
			<div class="box-heading bg-primary"><h3>Edit Project</h3></div>

			<!-- Choose the most relevant categories for your position -->
			<div class="block-sectn categories-select-sectn">
			  <h4 class="inner-block-heading"><span id="project_category_section_heading">
				<?php
				if($project_data[0]['project_type'] == 'fulltime'){
					echo $this->config->item('post_fulltime_project_category_section_heading');
				}else{
					echo $this->config->item('post_project_category_section_heading');
				}
				?>
				</span></h4>
			  <div class="row">
				<div class="col-md-12" id="category_listing_block">
					<?php
					
					if(!empty($project_category_data))
					{
						$category_counter = 0;
						foreach($project_category_data as $project_category)
						{
					?>
							<div class="row category_row" id="<?php echo "project_category_row_".$project_category['id']."_".$project_category['project_id']; ?>">	
							<?php
							if(empty($project_category['project_parent_category_id'])){
								$get_project_child_categories = $CI->Post_project_model->get_project_child_categories($project_category['project_category_id']);
							}else{
								$get_project_child_categories = $CI->Post_project_model->get_project_child_categories($project_category['project_parent_category_id']);
							}
							?>
								<div class="col-sm-6">
								  <div class="form-group">
											
										<select name="project_category[<?php echo $category_counter ?>][project_parent_category]" id="<?php echo "project_parent_category_".$category_counter ?>" class="project_parent_category">
										<!--<option>Select Category</option>-->
										<?php
										
										if(!empty($project_parent_categories)){
											foreach ($project_parent_categories as $project_parent_category_row){ 
												$selected_parent_category_id = 0;
												$selected_parent_category_selected = "";
												if($project_category['project_parent_category_id'] == 0){
													$selected_parent_category_id = $project_category['project_category_id'];
												}else{
													$selected_parent_category_id = $project_category['project_parent_category_id'];
												}

												if($selected_parent_category_id == $project_parent_category_row['id']){
													$selected_parent_category_selected = "selected";
												}
												
												if(!empty($selected_category_array)){
													$array_diff = array();
													$array_diff = array_diff($selected_category_array,array($project_category['project_category_id']));
													if(in_array($project_parent_category_row['id'], $array_diff)){
														$style_display = "display:none;";
													}else{
														$style_display = "display:block;";
													}
													
												}
												
												
										?>
												<option  style="<?php echo $style_display; ?>" <?php echo $selected_parent_category_selected; ?> value="<?php echo $project_parent_category_row['id']; ?>"><?php echo $project_parent_category_row['name']."";?></option>
										<?php
											}
										}	
										?>		   
									  </select>                    
								  </div>
								</div>
								<div class="col-sm-6">
								  <div class="form-group">
									<?php
									$project_child_category_disabled = "";
									if($get_project_child_categories){
										$project_child_category_disabled = "disabled";
									}
									?>
									  <select name="project_category[<?php echo $category_counter ?>][project_child_category]"  id="<?php echo "project_child_category_".$category_counter ?>"  <?php echo $project_child_category_disabled; ?> class="project_child_category">
									  
										
										<option value="">Select Subcategory</option>
										<?php
										if(!empty($get_project_child_categories )){
											foreach ($get_project_child_categories as $project_child_category_row) {
											
											$selected_child_category_id = 0;
												$selected_child_category_selected = "";
												if($project_category['project_parent_category_id'] != 0){
													$selected_child_category_id = $project_category['project_category_id'];
												}
												if($selected_child_category_id == $project_child_category_row['id']){
													$selected_child_category_selected = "selected";
												}
										?>
												<option <?php echo $selected_child_category_selected; ?> value="<?php echo $project_child_category_row['id']; ?>"><?php echo $project_child_category_row['name'];?></option>
										<?php
											
											}
										}		
										?>			
									  </select>                    
								  </div>
								</div>
							<?php if($category_counter != 0){ ?>
							 <a data-id = "<?php echo $project_category['project_category_id'] ?>"  id="<?php echo $project_category['id']."_".$project_category['project_id'] ?>" class="delete-category-row delete_project_category_row_data"><i class="fa fa-trash"></i></a>
							 
							  <?php } ?>
							</div>
					<?php
						$category_counter++;
						}
					}?>
				</div>
				<div class="col-sm-6">
					<button type="button" class="btn btn-default" id="add_more_project_category">
					<?php
					if(empty($project_category_data)){
						echo "Add Category";
					}else{
						echo "Add Another Category";
					}
					?>	
					</button>
				</div>
				<div class="col-sm-6">
					<div class="form-group" id="project_parent_category_0"> 
						
						<div class="error_div_sectn clearfix">
						<span id="project_parent_category_0_error" class="error_msg"></span>
						</div>
					</div>
				</div>
				<div class="col-sm-6"></div>
				<div class="hover-tooltip-sectn" id="project_category_section_tooltip" style="display:none;">
				  <?php 
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_category_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_category_section');
					}
				  
				  ?>
				</div>
			  </div>
			</div>
			<!--end Choose the most relevant categories for your position -->

			<!-- check box button -->
			<div class="checkbox-btn-sectn project_post_checkbox block-sectn">
			  <div class="row">
				<div class="col-sm-6">
				  <div class="form-group">
					<div class="checkbox-btn-inner">
					  <input id="post_project" name="project_type_main" class="post_project_input" style="position: absolute;top: 0;width: 100%;" type="radio" value="post_project" disabled>
					  <div class="checkbox-inner-div">
						<label for="post_project">I'm a toggle</label>
						<div class="checkbox-content align-middle">
						  <h6>I want to post a project</h6>
						  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,</p>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
				<div class="col-sm-6">
				  <div class="form-group">
					<div class="checkbox-btn-inner">
					  <input id="post_fulltime_position" class="post_project_input" name="project_type_main" style="position: absolute;top: 0;width: 100%;" type="radio" value="post_fulltime_position" disabled>
					  <div class="checkbox-inner-div">
						<label for="post_fulltime_position">I'm a toggle</label>
						<div class="checkbox-content align-middle">
						  <h6>I want to post a fulltime position</h6>
						  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,</p>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<!-- end check box button -->

			<!-- check box button -->
			<div class="checkbox-btn-sectn block-sectn project_post_checkbox" id="project_type_block" >
			  <div class="row">
				<div class="col-sm-6">
				  <div class="form-group">
					<div class="checkbox-btn-inner">
					  <input name="project_type" class="post_project_type_input" style="position: absolute;top: 0;width: 100%;" type="radio" value="fixed" id="project_type_fixed" disabled>
					  <div class="checkbox-inner-div">
						<label for="project_type_fixed">I'm a toggle</label>
						<div class="checkbox-content align-middle">
						  <h6>Pay fixed price</h6>
						  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,</p>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				<div class="col-sm-6">
				  <div class="form-group">
					<div class="checkbox-btn-inner">
					  <input  name="project_type" style="position: absolute;top: 0;width: 100%;" type="radio" class="post_project_type_input" value="hourly" id="project_type_hourly" disabled>
					  <div class="checkbox-inner-div">
						<label for="project_type_hourly">I'm a toggle</label>
						<div class="checkbox-content align-middle">
						  <h6>Pay by the hour</h6>
						  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,</p>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<!-- end check box button -->

			<!-- What budget do you have in mind ? -->
			<div class="block-sectn select-budget project_show_block"  id="project_budget">
			  <div class="row">
				<div class="col-md-12">
				  <h4 class="inner-block-heading"><span id="project_budget_section_heading">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_salary_range_section_heading');
					}else{
						echo $this->config->item('post_project_budget_range_section_heading');
					}
					?>
					</span></h4> 
				</div>
				<div class="col-sm-6">
					<div class="form-group"> 
						<?php
							$budget_selected_value = '';
						if($project_data[0]['confidential_dropdown_option_selected'] == 'Y'){
						
							$budget_selected_value = key($this->config->item('fixed_budget_projects_confidential_dropdown_option'));
						
						}else if($project_data[0]['not_sure_dropdown_option_selected'] == 'Y'){
							$budget_selected_value = key($this->config->item('fixed_budget_projects_not_sure_dropdown_option'));
						}
						else if(!empty($project_data[0]['min_budget'])){
							$budget_selected_value = $project_data[0]['min_budget']."_".$project_data[0]['max_budget'];
						}
						?>
						<select name="fixed_budget" id="fixed_budget"  disabled>
							<option value="">Select Budget</option>
							<?php 
							if(!empty($fixed_budget_projects_budget_range))
							{
							
								foreach ($fixed_budget_projects_budget_range as $fixed_budget_projects_budget_range_row){ 
							?>
								<option 
							<?php echo isset ($budget_selected_value) && $budget_selected_value == $fixed_budget_projects_budget_range_row['fixed_budget_range_key'] ? 'selected' : ''; ?>
							value="<?php echo $fixed_budget_projects_budget_range_row['fixed_budget_range_key']; ?>"><?php echo $fixed_budget_projects_budget_range_row['fixed_budget_range_value'];?></option>
							<?php 
								} 
							}
							?>
						</select>
						<select name="project_budget" id="hourly_rate_based_budget">
							<option value="">Select Budget</option>
							<?php 
							if(!empty($hourly_rate_based_budget_projects_budget_range))
							{
								foreach ($hourly_rate_based_budget_projects_budget_range as $hourly_rate_based_budget_projects_budget_range_row){ 
							?>
								<option <?php echo isset ($budget_selected_value) && $budget_selected_value == $hourly_rate_based_budget_projects_budget_range_row['hourly_rate_based_budget_range_key'] ? 'selected' : ''; ?>
								value="<?php echo $hourly_rate_based_budget_projects_budget_range_row['hourly_rate_based_budget_range_key']; ?>"><?php echo $hourly_rate_based_budget_projects_budget_range_row['hourly_rate_based_budget_range_value'];?></option>
							<?php 
								} 
							}
							?>
						</select> 
						<select name="project_budget" id="fulltime_salary_range">
						<option value="">Select Salary</option>
						<?php 
						if(!empty($fulltime_project_salary_range))
						{
							foreach ($fulltime_project_salary_range as $fulltime_projects_salary_range_row){ 
						?>
							<option <?php echo isset ($budget_selected_value) && $budget_selected_value == $fulltime_projects_salary_range_row['fulltime_salary_range_key'] ? 'selected' : ''; ?> value="<?php echo $fulltime_projects_salary_range_row['fulltime_salary_range_key']; ?>"><?php echo $fulltime_projects_salary_range_row['fulltime_salary_range_value'];?></option>
						<?php 
							} 
						}
						?>
						</select> 
						<div class="error_div_sectn clearfix">
							<span id="project_budget_error" class="error_msg"></span>
						</div>

					</div>
				</div>
				<div class="col-sm-6"></div>
				<div class="hover-tooltip-sectn bottom-0 top-auto" style="display:none;" id="project_budget_section_tooltip">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_salary_range_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_budget_range_section');
					}
					?>
				</div>
			  </div>
			</div>
			<!--end What budget do you have in mind ? -->

			<!-- Project name -->
			<div class="block-sectn file-project-name project_show_block" >
				<div class="row">
                    <div class="col-md-12">
                      <h4 class="inner-block-heading"><span id="project_title_section_heading">
						<?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('post_fulltime_position_name_section_heading');
						}else{
							echo $this->config->item('post_project_title_section_heading');
						}
						?>
						</span></h4> 
                    </div>
                    <div class="col-md-12">
						<div class="form-group"> 
							<?php
							$project_title = $project_data[0]['project_title'];
							?>
							<input type="text" name="project_title" id="project_title" disabled   class="avoid_space" maxlength="<?php echo $this->config->item('project_title_maximum_length_character_limit_post_project'); ?>" value="<?php echo $project_title; ?>"> 
							<div class="error_div_sectn clearfix">
								<span id="project_title_error" class="error_msg"></span>
								
							</div>      
						</div>
                    </div>
                    <div class="col-sm-6"></div>
                    <div class="hover-tooltip-sectn" style="display:none;" id="project_title_section_tooltip">
						<?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_name_section');
						}else{
							echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_title_section');
						}	
						?>
                    </div>
                </div>
			</div>
			<!-- end Project name -->
			<!-- Describe your project in detail: -->
			<div class="block-sectn project-describe project_show_block">
			  <div class="row">
                    <div class="col-md-12">
						<h4 class="inner-block-heading"><span id="project_description_section_heading">
							<?php
							if($project_data[0]['project_type'] == 'fulltime'){ 
								echo $this->config->item('post_fulltime_position_description_section_heading');
							}else{
								echo $this->config->item('post_project_description_section_heading');
							}	
							?>
						</span></h4> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
						<?php
							$project_description = $project_data[0]['project_description'];
						?>
						<textarea name="project_description" disabled id="project_description" class="avoid_space_textarea" maxlength="<?php echo $this->config->item('project_description_maximum_length_character_limit_post_project'); ?>"><?php echo trim($project_description); ?></textarea>
						<div class="error_div_sectn clearfix">
						<span id="project_description_error" class="error_msg"></span>
						</div>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="hover-tooltip-sectn" style="display:none;" id="project_description_section_tooltip">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_description_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_description_section');
					}	
					?>
                    </div>
                  </div>
			</div>
			<!-- Describe your project in detail -->
			<!-- Describe additional information of project: -->
			<div class="block-sectn project-describe project_show_block">
			  <div class="row">
                    <div class="col-md-12">
						<h4 class="inner-block-heading"><span id="project_additional_information_section_heading"><?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('post_fulltime_position_additional_information_section_heading');
						}else{
							echo $this->config->item('post_project_additional_information_section_heading');
						}	
						?></span></h4> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
						<?php
							$project_additional_information = $project_data[0]['additional_information'];
							$project_additional_information_remaining_characters = $this->config->item('project_additional_information_maximum_length_character_limit_post_project') - strlen($project_additional_information);
						?>
						<textarea name="project_additional_information" id="project_additional_information" class="avoid_space_textarea" maxlength="<?php echo $this->config->item('project_additional_information_maximum_length_character_limit_post_project'); ?>"><?php echo trim($project_additional_information); ?></textarea>
						<div class="error_div_sectn clearfix">
						<span id="project_additional_information_error" class="error_msg"></span>
						<span class="content-count project_additional_information_length_count_message"><?php echo $project_additional_information_remaining_characters."&nbspcharacters remaining"; ?></span> 
						</div>
                      </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="hover-tooltip-sectn" style="display:none;">
						<?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('edit_fulltime_project_page_hover_tooltip_message_position_additional_information_section');
						}else{
							echo $this->config->item('edit_project_page_hover_tooltip_message_project_additional_information_section');
						}	
						?>
                    </div>
                  </div>
			</div>
			<!-- Describe additional information of project: -->

			<!-- Upload your document -->
			<div class="block-sectn upload-document project_show_block">
			  <div class="row">
				<div class="col-md-3">
				  <div class="upload-btn-wrapper">
					<!--<button class="btn btn-default"><i class="fa fa-cloud-upload"></i> Upload a file</button>
					<input type="file" name="myfile" />-->
					<span class="btn btn-default fileinput-button">
						<i class="fa fa-cloud-upload"></i>
						Upload a file
					</span>
				  </div>
				  <div class="upload_attachment_error"></div>
				</div>
				<div class="col-md-9">
					<?php
					
					$show_attachment_table = "";
					if($count_project_attachments == 0 ){
						$show_attachment_table = "display:none;";
					}
					?>
					<table class="table" id="project_attachment_container" style="<?php echo $show_attachment_table; ?>">
						<tr>
						  <th>file name</th>
                          <th align="center">file size</th>
                          <th align="center">remove</th>
						</tr>
						<?php
						if(!empty($project_attachment_array)){
							foreach($project_attachment_array as $project_attachment){
								$attachment_id = Cryptor::doEncrypt($project_attachment['id']);
						?>
							<tr class="project_attachment_row" id="<?php echo "project_attachment_row".$project_attachment['id']; ?>">
								<td ><a class="download_attachment" href="javascript:;" data-attr="<?php echo $attachment_id; ?>" ><?php echo $project_attachment['project_attachment_name']; ?></a></td>
								<td align="center"><?php echo $project_attachment['size']; ?></td>
								<td  align="center">
									<a href="javascript:void(0);" class="btn btn-default delete-btn project_attachment_row_delete" id="<?php echo $project_attachment['id'];?>"> <i class="fa fa-trash"></i> </a>	  
								</td>
							</tr>
						<?php
							}
						}	
						?>
					</table>
				</div>
					<div class="hover-tooltip-sectn" style="display:none;" id="project_attachment_section_tooltip">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_attachment_upload_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_attachment_upload_section');
					}	
					?>
					</div>
			  </div>
			</div>
			<!-- Upload your document -->

			<!-- tags -->
			<div class="block-sectn tags-sectn project_show_block">
				<div class="row">
					<div class="col-md-12">
					  <h4 class="inner-block-heading"><span id="project_tag_heading_section">
						<?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('post_fulltime_position_tags_section_heading');
						}else{
							echo $this->config->item('post_project_project_tags_section_heading');
						}	
						?>
						</span></h4> 
					</div>
				<div class="col-md-12">
					<input type="text" id="input_tags" name=""   placeholder="For Example: Web site design, Admin support..." class="avoid_space allow_character_number" maxlength="<?php echo $this->config->item('project_tag_maximum_length_character_limit_post_project'); ?>" >
					<div class="error_div_sectn clearfix">
						<span class="content-count project_tag_length_count_message"><?php echo $this->config->item('project_tag_maximum_length_character_limit_post_project')."&nbspcharacters remaining"; ?></span>
					</div>
					<ul id="tags-list"> 
						<?php
						if(!empty($project_tag_data)){
							$tag_counter = 0;
							foreach($project_tag_data as $project_tag){
								
						?>
							<li class="tag_name" id="<?php echo 'project_tag_'.$project_tag['id']."_".$project_tag['project_id'] ?>"> <span><?php echo $project_tag['project_tag_name']; ?><input type="hidden" name="project_tag[<?php echo $tag_counter; ?>][tag_name]" value="<?php echo $project_tag['project_tag_name']; ?>" /><i class="fa fa-times delete_project_tag_row_data" data-attr="<?php echo 'project_tag_'.$project_tag['id']."_".$project_tag['project_id'] ?>"></i> </span></li>
						
						<?php
								$tag_counter ++;
							}
						}	
						?>
					</ul>
				</div>
				<div class="hover-tooltip-sectn" style="display: none;" id="project_tag_section_tooltip">
				  <?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_tag_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_tag_section');
					}	
					?>
				</div>
				</div>
			</div>
			<!-- end tags -->

			<!-- Where do you want this done? -->
			<div class="block-sectn county-details project_show_block" id="location_block">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group" style="margin:0;">
							<div class="checkbox custom_check_box">  
								<div class="checkbox-btn-inner">
									<input id="this-check" style="position: absolute;top: 0;width: 100%;" type="checkbox" class="location_option" value="location" name="location_option">
									<div class="checkbox-inner-div d-inline-block">
									  <label for="this-check"></label>
									  Where do you want this done?
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="hover-tooltip-sectn" style="display: none;" id="project_location_section_tooltip"> 
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_location_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_location_section');
					}	
					?>
					</div>
				</div>
				<div class="location_section" style="display:none;">
				<div class="row">
					<div class="col-sm-6">
					  <div class="form-group"> 
						<select id="project_county_id" name="project_county_id">
							<option value="" >Select County</option>
							<?php foreach ($counties as $county): ?>
							<option <?php echo isset ($project_data[0]['county_id']) && $project_data[0]['county_id'] == $county['id'] ? 'selected' : ''; ?> value="<?php echo $county['id']; ?>"><?php echo $county['name'] ?></option>
							<?php endforeach; ?>
						</select> 
						<div class="error_div_sectn clearfix">
							<span id="project_county_id_error" class="error_msg"></span>
						</div>
					  </div>
					</div>
					<div class="col-sm-6">
					  <div class="form-group"> 
						<?php
						$lacality_disabled = "disabled";
						if(!empty($project_data[0]['locality_id'])){
							$lacality_disabled = "";
						}
						?>
						<select  name="project_locality_id" id="project_locality_id" <?php echo $lacality_disabled;?> >
							<option value="">Select Locality</option>
							<?php foreach ($localities as $locality): ?>
							<option <?php echo isset ($project_data[0]['locality_id']) && $project_data[0]['locality_id'] == $locality['id'] ? 'selected' : ''; ?>
							value="<?php echo $locality['id']; ?>"><?php echo $locality['name'] ?></option>
							<?php endforeach; ?>
						</select>
						<div class="error_div_sectn clearfix">
							<span id="project_locality_id_error" class="error_msg"></span>
						</div>
					  </div>
					</div>
					<div class="col-sm-6">
					  <div class="form-group"> 
						<?php
						$postal_code_disabled = "disabled";
						if(!empty($project_data[0]['postal_code_id'])){
							$postal_code_disabled = "";
						}
						?>
						<select name="project_postal_code_id" id="project_postal_code_id" <?php echo $postal_code_disabled;?>>
							<option value="">Select Postal Code</option>
							<?php foreach ($postal_codes as $postal_code): ?>
							<option <?php echo isset ($project_data[0]['postal_code_id']) && $project_data[0]['postal_code_id'] == $postal_code['id'] ? 'selected' : ''; ?>
							value="<?php echo $postal_code['id']; ?>"><?php echo $postal_code['postal_code']; ?></option>
							<?php endforeach; ?>
						</select>	
						<div class="error_div_sectn clearfix">
							<span id="project_postal_code_id_error" class="error_msg"></span>
						</div>
					  </div>
					</div>
					<div class="col-md-6"></div>
				</div>
				</div>
			</div>
			<!-- Where do you want this done? -->

			<!-- Payment Methods -->
			<div class="payment-methods block-sectn project_show_block">
			  <div class="row">
				<div class="col-md-12">
				  <h4 class="inner-block-heading"><span id="project_payment_method_section_heading">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_project_project_optional_upgrades_section_heading');
					}else{
						echo $this->config->item('post_project_payment_method_section_heading');
					}	
					?>
					</span></h4>
				</div>
				<div class="col-md-12">
				  <div class="form-group">
					<div class="checkbox custom_check_box">  
					  <div class="checkbox-btn-inner">
						<input id="payment-methods1" class="escrow_payment_method" name="escrow_payment_method" value="Y" style="position: absolute;top: 0;width: 100%;" type="checkbox">
						<div class="checkbox-inner-div">
							<label for="payment-methods1"></label>
							<span id="payment_method_escrow_text">
							<?php
							if($project_data[0]['project_type'] == 'fulltime'){ 
								echo $this->config->item('post_fulltime_project_page_step2_payment_method_section_text_escrow_payment');
							}else{
								echo $this->config->item('post_project_page_step2_payment_method_section_text_escrow_payment');
							}	
							?>
							</span>	
						</div>
					  </div>  
					</div>
				  </div>
				  <div class="form-group">
					<div class="checkbox custom_check_box">  
					  <div class="checkbox-btn-inner">
						<input id="payment-methods2" class="offline_payment_method" name="offline_payment_method" value="Y" style="position: absolute;top: 0;width: 100%;" type="checkbox">
						<div class="checkbox-inner-div">
							<label for="payment-methods2"></label>
							<span id="payment_method_offline_text">	
							<?php
							if($project_data[0]['project_type'] == 'fulltime'){ 
								echo $this->config->item('post_fulltime_project_page_step2_payment_method_section_text_offline_payment');
							}else{
								echo $this->config->item('post_project_page_step2_payment_method_section_text_offline_payment');
							}	
							?>
							</span>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				<div class="hover-tooltip-sectn" style="display:none;" id="project_payment_method_section_tooltip">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_payment_method_section');
					}else{
						echo $this->config->item('post_project_page_step2_hover_tooltip_message_payment_method_section');
					}	
					?>
				</div>
			  </div>
			</div>
			<!-- end Payment Methods -->
			
			<!-- Confidential option -->
			<div class="block-sectn confidential_after_expiration_show_block" style="display:none">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="checkbox custom_check_box">  
							  <div class="checkbox-btn-inner">
								<input id="confidential_after_expiration_option" name="confidential_after_expiration_selected" value="Y" style="position: absolute;top: 0;width: 100%;" type="checkbox">
								<div class="checkbox-inner-div">
								  <label for="confidential_after_expiration_option"></label>
								  <?php echo $this->config->item('fulltime_project_keep_posting_confidential_after_expiration'); ?>
							  </div>
							</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
			<!-- Confidential option -->

			
			
			<?php
			if($project_upgrade_type_box_show_status){
			?>
			<!-- Get most from your project! (optional) -->
				<div class="most-project block-sectn project_show_block">
				  <div class="row">
					  <div class="col-md-12">
					  <h4 class="inner-block-heading"><span id="project_optional_upgrades_section_heading">
						<?php
						if($project_data[0]['project_type'] == 'fulltime'){ 
							echo $this->config->item('post_fulltime_position_optional_upgrades_section_heading');
						}else{
							echo $this->config->item('post_project_optional_upgrades_section_heading');
						}	
						?>
					</span></h4>
						<?php
						if($project_data[0]['featured'] == 'N' && $featured_max < time() ){
						
						?>
						  <div class="form-group">
							<div class="checkbox-btn-inner">
								<input id="most-project1" style="position: absolute;top: 0;width: 100%;" type="checkbox" class="upgrade_type_featured upgrade_type" name="upgrade_type_featured" value="Y">
								<div class="checkbox-inner-div">
								  <label for="most-project1"></label>
								  <div class="row">
									<div class="checkbox-title"> <span class="bg-yellow-light">FEATURED</span></div>
									<div class="pay-sectn"><span><span id="upgrade_type_featured_amount"><?php echo number_format($this->config->item('project_upgrade_price_featured')); ?></span> Kč</span></div>  
								  </div>
								  <div class="checkbox-content">
									<p><?php echo $this->config->item('post_project_page_step2_project_upgrade_description_featured'); ?></p>
								  </div>
								</div>
							  </div>
						  </div>
						<?php
						}
						if($project_data[0]['urgent'] == 'N' && $urgent_max < time() ){
						
						?>
						 <div class="form-group">
						  <div class="checkbox-btn-inner">
							  <input id="most-project2"  class="upgrade_type_urgent upgrade_type" style="position: absolute;top: 0;width: 100%;" type="checkbox" name="upgrade_type_urgent"  value="Y">
							  <div class="checkbox-inner-div">
								<label for="most-project2"></label>
								<div class="row">
								  <div class="checkbox-title"> <span  class="bg-danger text-white">URGENT</span></div>
								  <div class="pay-sectn"> <span><span id="upgrade_type_urgent_amount"><?php echo number_format($this->config->item('project_upgrade_price_urgent')); ?></span> Kč</span></div>
								</div>
								<div class="checkbox-content">
								  <p><?php echo $this->config->item('post_project_page_step2_project_upgrade_description_urgent'); ?></p>
								</div>
							  </div>
							</div>
						  </div>
						<?php
						}
						?>
						<div class="total-price" style="display:none;">
							<p>Total:   <span id="total_upgrade_amount">0</span>  <?php echo CURRENCY; ?></p>
						</div>
						<div id="upgrade_message"></div>
							
					</div>
				  </div>
				</div>
			<!-- Get most from your project! (optional) -->
			<?php
			}
			?>

			<!-- project relative buttons -->
			<div class="project-relative-btn block-sectn project_show_block">
			  <div class="row">
				<div class="col-md-12 text-center">
				  <button type="button" class="btn btn-default" id="update_project">Update Project</button>
				  <button type="button" class="btn btn-danger" id="cancel_project">Cancel</button>
				</div>
			  </div>
			</div>
			<!-- project relative buttons -->

			<!-- Publish Project content -->
			<div class="publish-project-text project_show_block" style="display:none">
			  <div class="row">
				<div class="col-md-12 text-center">
				  <p>By clicking "Publish Project" you confirm that you accept the <a href="javascript:void(0);"> Terms and Conditions </a> and <a href="javascript:void(0);"> Policy </a></p>
				</div>
			  </div>
			</div>
			<!-- end Publish Project content -->
		  </div>
		 <?php echo form_close(); ?>  
		</div><!-- col md 8 -->
		<div class="col-md-4 hidden-mobile" style="opacity: inherit;z-index: -1;">
			<div class="right-sectn-bar">
				<div class="box-overly" id="project_major_benefits_text_container">
					<?php 
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_major_benefits_section_text'); 
					}else{
						echo $this->config->item('post_project_major_benefits_section_text');
					}
					?>
				</div>
				<div class="box-overly" id="project_how_it_works_text_container">
					<?php
					if($project_data[0]['project_type'] == 'fulltime'){ 
						echo $this->config->item('post_fulltime_project_how_it_works_section_text'); 
					}else{
						echo $this->config->item('post_project_how_it_works_section_text'); 
					}
					?>
				</div>
			</div>
		</div>
	  </div><!-- row -->
	</div><!-- container fluid -->
  </div>
 </main>
<div class="modal alert-popup" id="error_popup" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <div class="modal-header-inner">
				<img style="" src="<?=ASSETS?>images/post_project/alert-icon.png" alt="alert icon" class="img-fluid" />
				<h4 class="modal-title" id="error_popup_heading"></h4>
			  </div>
			</div>
			<div class="modal-body text-center">
			  <p id="error_popup_body"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger popup_close" data-dismiss="modal">Close</button>
			</div>
	    </div>
	</div>
</div> 
<script type="text/javascript">

var project_additional_information_maximum_length_character_limit_post_project = "<?php echo $this->config->item('project_additional_information_maximum_length_character_limit_post_project'); ?>";	
var project_additional_information_minimum_length_character_limit_post_project = "<?php echo $this->config->item('project_additional_information_minimum_length_character_limit_post_project'); ?>";	
var project_tag_maximum_length_character_limit_post_project = "<?php echo $this->config->item('project_tag_maximum_length_character_limit_post_project'); ?>";	

var number_tag_allowed_post_project = "<?php echo $this->config->item('number_tag_allowed_post_project'); ?>";	
var number_project_category_post_project = "<?php echo $this->config->item('number_project_category_post_project'); ?>";	
var project_attachment_maximum_size_limit = "<?php echo $this->config->item('project_attachment_maximum_size_limit'); ?>";
project_attachment_maximum_size_limit = project_attachment_maximum_size_limit * 1048576;
var project_attachment_maximum_size_validation_post_project_message = "<?php echo $this->config->item('project_attachment_maximum_size_validation_post_project_message'); ?>";	
var project_attachment_allowed_files_validation_post_project_message = "<?php echo $this->config->item('project_attachment_allowed_files_validation_post_project_message'); ?>";	


var project_attachment_invalid_file_extension_validation_post_project_message = "<?php echo $this->config->item('project_attachment_invalid_file_extension_validation_post_project_message'); ?>";


// config vaiables for section headings start //

var post_project_category_section_heading = "<?php echo $this->config->item('post_project_category_section_heading'); ?>";
var post_fulltime_project_category_section_heading = "<?php echo $this->config->item('post_fulltime_project_category_section_heading'); ?>";
var post_project_budget_range_section_heading = "<?php echo $this->config->item('post_project_budget_range_section_heading'); ?>";
var post_fulltime_project_salary_range_section_heading = "<?php echo $this->config->item('post_fulltime_project_salary_range_section_heading'); ?>";
var post_project_title_section_heading = "<?php echo $this->config->item('post_project_title_section_heading'); ?>";
var post_fulltime_position_name_section_heading = "<?php echo $this->config->item('post_fulltime_position_name_section_heading'); ?>";
var post_project_description_section_heading = "<?php echo $this->config->item('post_project_description_section_heading'); ?>";
var post_fulltime_position_description_section_heading = "<?php echo $this->config->item('post_fulltime_position_description_section_heading'); ?>";
var post_project_project_tags_section_heading = "<?php echo $this->config->item('post_project_project_tags_section_heading'); ?>";
var post_fulltime_position_tags_section_heading = "<?php echo $this->config->item('post_fulltime_position_tags_section_heading'); ?>";
var post_project_payment_method_section_heading = "<?php echo $this->config->item('post_project_payment_method_section_heading'); ?>";
var post_fulltime_position_payment_method_section_heading = "<?php echo $this->config->item('post_fulltime_position_payment_method_section_heading'); ?>";


var post_project_page_step2_payment_method_section_text_escrow_payment = "<?php echo $this->config->item('post_project_page_step2_payment_method_section_text_escrow_payment'); ?>";
var post_project_page_step2_payment_method_section_text_offline_payment = "<?php echo $this->config->item('post_project_page_step2_payment_method_section_text_offline_payment'); ?>";
var post_fulltime_project_page_step2_payment_method_section_text_escrow_payment = "<?php echo $this->config->item('post_fulltime_project_page_step2_payment_method_section_text_escrow_payment'); ?>";
var post_fulltime_project_page_step2_payment_method_section_text_offline_payment = "<?php echo $this->config->item('post_fulltime_project_page_step2_payment_method_section_text_offline_payment'); ?>";

var post_project_optional_upgrades_section_heading = "<?php echo $this->config->item('post_project_optional_upgrades_section_heading'); ?>";
var post_fulltime_position_optional_upgrades_section_heading = "<?php echo $this->config->item('post_fulltime_position_optional_upgrades_section_heading'); ?>";


var post_project_major_benefits_section_text = "<?php echo $this->config->item('post_project_major_benefits_section_text'); ?>";
var post_project_how_it_works_section_text = "<?php echo $this->config->item('post_project_how_it_works_section_text'); ?>";
var post_fulltime_project_major_benefits_section_text = "<?php echo $this->config->item('post_fulltime_project_major_benefits_section_text'); ?>";
var post_fulltime_project_how_it_works_section_text = "<?php echo $this->config->item('post_fulltime_project_how_it_works_section_text'); ?>";




// config vaiables for section tooltip messages start //

var post_project_page_step2_hover_tooltip_message_category_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_category_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_category_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_category_section'); ?>";

var post_project_page_step2_hover_tooltip_message_budget_range_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_budget_range_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_salary_range_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_salary_range_section'); ?>";

var post_project_page_step2_hover_tooltip_message_project_title_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_title_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_position_name_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_name_section'); ?>";

var post_project_page_step2_hover_tooltip_message_project_description_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_description_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_position_description_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_description_section'); ?>";

var post_project_page_step2_hover_tooltip_message_project_attachment_upload_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_attachment_upload_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_attachment_upload_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_attachment_upload_section'); ?>";

var post_project_page_step2_hover_tooltip_message_project_tag_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_tag_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_tag_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_tag_section'); ?>";

var post_project_page_step2_hover_tooltip_message_project_location_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_project_location_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_position_location_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_position_location_section'); ?>";

var post_project_page_step2_hover_tooltip_message_payment_method_section = "<?php echo $this->config->item('post_project_page_step2_hover_tooltip_message_payment_method_section'); ?>";

var post_fulltime_project_page_step2_hover_tooltip_message_payment_method_section = "<?php echo $this->config->item('post_fulltime_project_page_step2_hover_tooltip_message_payment_method_section'); ?>";


var popup_error_heading = "<?php echo $this->config->item('popup_error_heading'); ?>";

var maximum_allowed_number_of_attachments_on_projects = "<?php echo $this->config->item('maximum_allowed_number_of_attachments_on_projects'); ?>";
//var post_project_page_url = "<?php echo $this->config->item('post_project_page_url'); ?>";
var dashboard_page_url = "<?php echo $this->config->item('dashboard_page_url'); ?>";
//var preview_draft_project_page_url = "<?php echo $this->config->item('preview_draft_project_page_url'); ?>";


var category_options = "";
var	project_id = '<?php echo $project_id ?>';
var project_status = "<?php echo 'open_for_bidding'; ?>";
var show_project_upgrade_amount_staus = false;
$("#hourly_rate_based_budget").attr('disabled','disabled');	
$("#hourly_rate_based_budget").attr('disabled','disabled');	

	
<?php 
	
	if(!empty($project_parent_categories))
	{
		foreach ($project_parent_categories as $project_parent_category_row){ 
?>	
		category_options += '<option value="'+<?php echo $project_parent_category_row['id']; ?>+'">'+'<?php echo $project_parent_category_row['name']; ?>'+'</option>';
<?php 
		} 
	}
?>
<?php
	if($project_data[0]['project_type'] == 'fulltime'){
?>	
	$("#fulltime_salary_range").css('display','block');	
	$("#fulltime_salary_range").removeAttr('disabled');
	$("#hourly_rate_based_budget").css('display','none');	
	$("#hourly_rate_based_budget").attr('disabled','disabled');	
	$("#fixed_budget").attr('disabled','disabled');	
	$("#fixed_budget").css('display','none');
	$("#project_type_block").css('display','none');
	$("#project_budget").css('display','block');
	$("#post_fulltime_position").prop("checked", true);
	$("#project_type_fixed").prop("checked", false);
	$("#post_project").prop("checked", false);
	$("#project_type_hourly").prop("checked", false);
	$(".confidential_after_expiration_show_block").css("display","block");
	<?php
	if($project_data[0]['confidential_after_expiration_selected'] == 'Y'){
	?>
		$("#confidential_after_expiration_option").prop("checked", true);
	<?php
	}else{
	?>
		$("#confidential_after_expiration_option").prop("checked", false);
	<?php
	}
	?>
<?php
	}else{
?>
		$(".confidential_after_expiration_show_block").css("display","none");
		$("#confidential_after_expiration_option").prop("checked", false);
		
		$("#fulltime_salary_range").css('display','none');	
		$("#fulltime_salary_range").attr('disabled','disabled');
		$("#post_fulltime_position").prop("checked", false);
<?php
		if($project_data[0]['project_type'] == 'fixed'){
?>	
		$("#project_type_fixed").prop("checked", true);
		$("#post_project").prop("checked", true);
		$("#hourly_rate_based_budget").css('display','none');	
		$("#hourly_rate_based_budget").attr('disabled','disabled');			
<?php
		}else if($project_data[0]['project_type'] == 'hourly'){
?>
		$("#project_type_hourly").prop("checked", true);
		$("#post_project").prop("checked", true);
		$("#fixed_budget").css('display','none');	
		$("#fixed_budget").attr('disabled','disabled');
<?php
		}
	
	}
?>	
<?php
	if(!empty($project_data[0]['county_id'])){
?>
		$(".location_option").prop("checked", true);
		$(".location_section").show();
	<?php
	}else{
	?>
		//$("#project_county_id").hide();
		//$("#project_locality_id").hide();
		//$("#project_postal_code_id").hide();
		$(".location_section").hide();
		$(".county-details #project_county_id").prop('selectedIndex',0);
		$(".county-details #project_locality_id").prop('selectedIndex',0);
		$(".county-details #project_locality_id").attr('disabled','disabled');
		$(".county-details #project_locality_id").html('<option value="">Select Locality</option>');
		$(".county-details #project_postal_code_id").prop('selectedIndex',0);
		$(".county-details #project_postal_code_id").attr('disabled','disabled');
		$(".county-details #project_postal_code_id").html('<option value="">Select Postal Code</option>');
		
	<?php
	} 
	if($project_data[0]['escrow_payment_method'] == 'Y'){
	?>
		$(".escrow_payment_method").prop("checked", true);	
	<?php
	}if($project_data[0]['offline_payment_method'] == 'Y'){
	?>
		$(".offline_payment_method").prop("checked", true);	
	<?php
	}
		
	?>
	
	<?php
	
	if( $count_project_categories >= $this->config->item('number_project_category_post_project')){
	?>
		$("#add_more_project_category").hide(); // check number of category block if there are more less then 5 it will show the add more category option
		$("#add_more_project_category").attr('disabled','disabled');// 
	<?php
	}if( $count_project_attachments >= $this->config->item('maximum_allowed_number_of_attachments_on_projects')){
	?>
	$(".upload-btn-wrapper").hide();
	<?php
	}if($count_project_tags >= $this->config->item('number_tag_allowed_post_project')){
	?>
		$("#input_tags").hide();
		$("#input_tags").next().hide();
	<?php
	}
	if($count_project_postal_codes  == '1'){
	?>
		$('.county-details #project_postal_code_id option[value=""]').hide();	
	<?php
	}
	if(!empty($project_data[0]['postal_code_id'])){
	?>
		$('#project_postal_code_id option[value=""]').hide();
	<?php
	}
	if(!empty($project_data[0]['county_id'])){
	?>
	$('#project_county_id option[value=""]').hide();
	<?php
	}
	if($project_data[0]['locality_id']){
	?>
	$('#project_locality_id option[value=""]').hide();
	<?php
	}
	?>
	/* if(show_project_upgrade_amount_staus){
		$(".total-price").css("display","block");
		$(".disclaimer").css("display","block");
		$(".bonus_balance").css("display","block");
	} */
</script>
<script src="<?=ASSETS?>js/dropzone.js"></script>
<script src="<?php echo JS; ?>modules/edit_project.js"></script>



