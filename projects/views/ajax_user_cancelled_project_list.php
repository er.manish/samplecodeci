<?php
    $cancelled_project_button_show_status = "display:block";
    
    if(!empty($cancelled_project_data)){
        $cancelled_project_button_show_status = "display:none";
        foreach($cancelled_project_data as $cancelled_project_key => $cancelled_project_value){
        
            // $featured_upgrade_end_date = $expired_project_value['featured_upgrade_end_date'] != NULL ? strtotime ($open_bidding_project_value['featured_upgrade_end_date']) : 0; // latest featured upgrade date
            
            // $urgent_upgrade_end_date = $expired_project_value['urgent_upgrade_end_date'] != NULL ? strtotime ($open_bidding_project_value['urgent_upgrade_end_date']) : 0;
            // latest featured upgrade date
            $featured_class = '';
            if($cancelled_project_value['featured'] == 'Y') {
                $featured_class = 'opBg';
            }
            $location = '';
            if(!empty($cancelled_project_value['county_name'])){
            if(!empty($cancelled_project_value['locality_name'])){
                $location .= '&nbsp;'.$cancelled_project_value['locality_name'];
            }
            if(!empty($cancelled_project_value['postal_code'])){
                $location .= ',&nbsp;'.$cancelled_project_value['postal_code'] .',&nbsp;';
            }else{
                $location .= ',&nbsp';
            }
            $location .= $cancelled_project_value['county_name'];
            }
        
?>
    <div class="tabContent">
        <div class="opLBttm <?php echo $featured_class?>" id="<?php echo "open_for_bidding_project_".$cancelled_project_value['project_id'] ?>">
            <a href="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$cancelled_project_value['project_id']; ?>">
                <?=$cancelled_project_value['project_title']?>
            </a>
            <label>
                <small><i class="far fa-clock"></i><?php echo date(DATE_TIME_FORMAT,strtotime($cancelled_project_value['project_posting_date'])) ?></small>
                <small>
					<i class="far fa-credit-card"></i>
					<?php echo ucfirst($cancelled_project_value['project_type']).' budget'; ?>
					<?php
					//if($cancelled_project_value['project_type'] == 'fixed'){
						if($cancelled_project_value['confidential_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
						}else if($cancelled_project_value['not_sure_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
						}else{
								if($cancelled_project_value['max_budget'] != 'All'){
									$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($cancelled_project_value['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($expired_project_value['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}else{
									$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($cancelled_project_value['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
								}
							}
						//}
						echo $budget_range;
						if($cancelled_project_value['escrow_payment_method'] == 'Y') {
							echo '- Payment Method: Safe Pay Escrow';
						}
					?>
				</small>
                <?php
                    if(!empty($location)):
                ?>
                <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                <?php
                    endif;
                ?>
                <small><i class="fas fa-bullhorn"></i>0 Bid</small>
            </label>
            <div class="osu1">
                <?php
                $description = strip_tags($cancelled_project_value['project_description']);
                ?>
                <div class="project_description_my_project desktop-secreen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_dekstop'));
                    ?>
                    </p>
                </div>
                <div class="project_description_my_project ipad-screen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_tablet'));
                    ?>
                    </p>
                </div>
                <div class="project_description_my_project mobile-screen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_mobile'));
                    ?>
                    </p>
                </div>
            </div>
            <div class="pProject">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 subRow">
                    
                        <?php
                        if(!empty($cancelled_project_value['categories'])) {
                            foreach($cancelled_project_value['categories'] as $category_key=>$category_value){
                                if($category_key <2){
                                    if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                    ?>
                                    <div class="clearfix">
                                    <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                    <a href="#">
                                        <span><?php echo $category_value['category_name']; ?></span>
                                    </a>
                                    </div>
                                    <?php
                                    }else{
                                        echo '<small>'.$category_value['category_name'].'</small>'; 
                                    }
                                }else{
                                    if($category_key == 2){
                                    ?>
                                        <div  class="collapse clearfix details-<?php echo $cancelled_project_value['project_id']; ?>" >
                                <?php } ?>
                                        <?php
                                        if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
                                        ?>
                                        <div class="clearfix">
                                        <small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
                                        <a href="#">
                                        <span><?php echo $category_value['category_name']; ?></span>
                                        </a>
                                        </div>
                                        <?php
                                        }else{
                                        ?>
                                            <small><?php echo $category_value['category_name']; ?></small>
                                        <?php
                                        }
                                        ?> 
                                    <?php
                                        if(count($cancelled_project_value['categories']) == ($category_key+1)){
                                        ?></div>
                                        <div class="text-center">
                                        <button type="button" class="btn opSL opShLsbtn"data-toggle="collapse" data-target=".details-<?php echo $cancelled_project_value['project_id']; ?>" data-text-alt="Show less <i class='fas fa-angle-up'></i>">Show More <i class="fas fa-angle-down"></i></button>
                                        </div>
                                    
                                    <?php
                                        }
                                    
                                    ?>
                                
                                <?php
                                
                                }
                            }
                        ?>
                            
                            
                        <?php
                        }
                        ?>	
                        
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="pDBttm">
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-12 baDges">									
                        <div class="pdButton" id="<?php echo "open_for_bidding_project_upgrade_badges_".$cancelled_project_value['project_id'] ?>">
                            <?php
                            if($cancelled_project_value['featured'] == 'Y' ){
                            ?>
                            <button type="button" class="btn">Featured</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($cancelled_project_value['urgent'] == 'Y' ){
                            ?>
                            <button type="button" class="btn urgent">Urgent</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($cancelled_project_value['sealed'] == 'Y'){
                            ?>
                            <button type="button" class="btn">Sealed</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($cancelled_project_value['delete_by_admin'] == 'Y'){
                            ?>
                            <button type="button" class="btn urgent">Admin</button>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 actOnly">
                        <div class="myAction">
                            <div class="dropdown">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>										
    
<?php
    }
}
?>

<?php
    if($cancelled_project_cnt > $this->config->item('user_dashboard_cancelled_projects_listing_limit')) {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
    <a href="">view more</a>
</div>
<?php
    }
?>

<div class="mpNoPrj" style="<?php echo $cancelled_project_button_show_status ?>">
    <h5><?=$this->config->item('no_cancelled_project_message')?></h5>
</div>