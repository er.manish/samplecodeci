<?php

$featured_max = 0;
$urgent_max = 0;
$expiration_featured_upgrade_date_array = array();
$expiration_urgent_upgrade_date_array = array();

if(!empty($project_data[0]['featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
}
if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
}
if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
	$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
}
if(!empty($expiration_featured_upgrade_date_array)){
	$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
}

##########

if(!empty($project_data[0]['urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['urgent_upgrade_end_date'];
}
if(!empty($project_data[0]['bounus_urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['bounus_urgent_upgrade_end_date'];
}
if(!empty($project_data[0]['membership_include_urgent_upgrade_end_date'])){
	$expiration_urgent_upgrade_date_array[] = $project_data[0]['membership_include_urgent_upgrade_end_date'];
}
if(!empty($expiration_urgent_upgrade_date_array)){
	$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
}
																



// latest featured upgrade date	
?>	
<div class="row">
	<div class="col-md-12">
		<div id="error_user_no_balance" class="alert alert-danger" style="display:none;"></div>
		<?php
		$attributes = [
		 'id' => 'project_upgrade_form',
		 'class' => '',
		 'role' => 'form',
		 'name' => 'project_upgrade_form',
		 'enctype' => 'multipart/form-data',
		];
		echo form_open('', $attributes);
		?>
		<input name="project_id" type="hidden" value="<?php echo $project_data[0]['project_id'] ?>" />
		<?php
		if($action_type == 'upgrade_project' || $action_type == 'prolong_availability_featured' || $action_type == 'upgrade_as_featured_project'){
		
			if($project_data[0]['featured'] == 'Y' && $action_type == 'prolong_availability_featured'){
				$time_arr = explode(':', $this->config->item('project_upgrade_availability_featured'));
				$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
			?>
				<h3><?php echo "current upgrade availability expires on ".date(DATE_TIME_FORMAT,$featured_max).". By choosing to prolong today, its availability will be extended to ".date(DATE_TIME_FORMAT,strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',$featured_max));?></h3>
			
			<?php
			}
		
		?>
			<div class="form-group">
				<div class="checkbox-btn-inner">
					<input id="most-project1" style="position: absolute;top: 0;width: 100%;" type="checkbox" class="upgrade_type_featured upgrade_type" name="upgrade_type_featured" value="Y">
					<div class="checkbox-inner-div">
						<label for="most-project1"></label>
						<div class="row">
							<div class="checkbox-title"> <span class="bg-yellow-light">FEATURED</span></div>
							<div class="pay-sectn"><span><span id="upgrade_type_featured_amount"><?php echo number_format($this->config->item('project_upgrade_price_featured')); ?></span> Kc</span></div>  
						</div>
						<div class="checkbox-content">
							<p><?php echo $this->config->item('post_project_page_step2_project_upgrade_description_featured'); ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php	
		}
		if($action_type == 'upgrade_project' || $action_type == 'prolong_availability_urgent' || $action_type == 'upgrade_as_urgent_project'){
			if($project_data[0]['urgent'] == 'Y' && $action_type == 'prolong_availability_urgent'){
				$time_arr = explode(':', $this->config->item('project_upgrade_availability_urgent'));
				$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
			?>
				<h3><?php echo "current upgrade availability expires on ".date(DATE_TIME_FORMAT,$urgent_max).". By choosing to prolong today, its availability will be extended to ".date(DATE_TIME_FORMAT,strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds',$urgent_max));?></h3>
			
			<?php
			}
			?>
			
			<div class="form-group">
				<div class="checkbox-btn-inner">
					<input id="most-project2"  class="upgrade_type_urgent upgrade_type" style="position: absolute;top: 0;width: 100%;" type="checkbox" name="upgrade_type_urgent"  value="Y">
					<div class="checkbox-inner-div">
						<label for="most-project2"></label>
						<div class="row">
							<div class="checkbox-title"> <span  class="bg-danger text-white">URGENT</span></div>
							<div class="pay-sectn"> <span><span id="upgrade_type_urgent_amount"><?php echo number_format($this->config->item('project_upgrade_price_urgent')); ?></span> Kc</span></div>
						</div>
						<div class="checkbox-content">
						<p><?php echo $this->config->item('post_project_page_step2_project_upgrade_description_urgent'); ?></p>
						</div>
					</div>
				</div>
			</div>
			
		<?php	
		}
		?>
		
		<div class="total-price" style="display:none;">
			<p>Total:   <span id="total_upgrade_amount">0</span>  <?php echo CURRENCY; ?></p>
		</div>
		<div id="upgrade_message"></div>
		
		
		
		<?php echo form_close(); ?>  
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btnCancel" data-dismiss="modal">Cancel</button>
	<button type="button" class="btn btnSave update_project_upgrade_button" style="opacity: .6;" disabled >Save</button>
</div>