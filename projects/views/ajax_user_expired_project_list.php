<?php
    $expired_project_button_show_status = "display:block";
    
    if(!empty($expired_project_data)){
        $expired_project_button_show_status = "display:none";
        foreach($expired_project_data as $expired_project_key => $expired_project_value){
        
            $featured_upgrade_end_date = $expired_project_value['featured_upgrade_end_date'] != NULL ? strtotime ($open_bidding_project_value['featured_upgrade_end_date']) : 0; // latest featured upgrade date
            
            $urgent_upgrade_end_date = $expired_project_value['urgent_upgrade_end_date'] != NULL ? strtotime ($open_bidding_project_value['urgent_upgrade_end_date']) : 0;
            // latest featured upgrade date
            $featured_class = '';
            if($expired_project_value['featured'] == 'Y') {
                $featured_class = 'opBg';
            }
            $location = '';
            if(!empty($expired_project_value['county_name'])){
            if(!empty($expired_project_value['locality_name'])){
                $location .= '&nbsp;'.$expired_project_value['locality_name'];
            }
            if(!empty($expired_project_value['postal_code'])){
                $location .= ',&nbsp;'.$expired_project_value['postal_code'] .',&nbsp;';
            }else{
                $location .= ',&nbsp';
            }
            $location .= $expired_project_value['county_name'];
            }
            
        
?>
    <div class="tabContent">
        <div class="opLBttm <?php echo $featured_class?>" id="<?php echo "open_for_bidding_project_".$expired_project_value['project_id'] ?>">
            <a href="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$expired_project_value['project_id']; ?>">
                <?=$expired_project_value['project_title']?>
            </a>
            <label>
                <small><i class="far fa-clock"></i><?php echo date(DATE_TIME_FORMAT,strtotime($expired_project_value['project_posting_date'])) ?></small>
                <small>
					<i class="far fa-credit-card"></i>
					<?php echo ucfirst($expired_project_value['project_type']).' budget'; ?>
					<?php
					//if($expired_project_value['project_type'] == 'fixed'){
						if($expired_project_value['confidential_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
						}else if($expired_project_value['not_sure_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
						}else{
								if($expired_project_value['max_budget'] != 'All'){
									$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($expired_project_value['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($expired_project_value['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}else{
									$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($expired_project_value['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
								}
							}
						//}
						echo $budget_range;
						if($expired_project_value['escrow_payment_method'] == 'Y') {
							echo '- Payment Method: Safe Pay Escrow';
						}
					?>
				</small>
                <?php
                    if(!empty($location)):
                ?>
                <small><i class="fas fa-map-marker-alt"></i><?=$location?></small>
                <?php
                    endif;
                ?>
                <small><i class="fas fa-bullhorn"></i>0 Bid</small>
            </label>
            <div class="osu1">
                <?php
                $description = strip_tags($expired_project_value['project_description']);
                ?>
                <div class="project_description_my_project desktop-secreen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_dekstop'));
                    ?>
                    </p>
                </div>
                <div class="project_description_my_project ipad-screen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_tablet'));
                    ?>
                    </p>
                </div>
                <div class="project_description_my_project mobile-screen">
                    <p><?php 
                    echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_mobile'));
                    ?>
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="pDSheduled">
            <ul id="<?php echo "open_for_bidding_project_upgrade_prolong_availability_".$expired_project_value['project_id'] ?>">
                <?php
                if($expired_project_value['featured'] == 'Y' && $featured_upgrade_end_date > time() ){
                ?>
                <li>
                    <label>
                        <span>
                        Featured Upgrade :
                        </span>
                        <small>
                        <?php echo "Expired on ".date(DATE_TIME_FORMAT,$featured_upgrade_end_date); ?>
                        </small>
                    </label>
                    <button type="button" data-attr= "<?php echo $expired_project_value['project_id']  ?>" class="btn upgrade_project" data-action-type="prolong_availability_featured">Prolong Availability</button>
                </li>
                <?php
                }
                if($open_bidding_project_value['urgent'] == 'Y' && $urgent_upgrade_end_date > time()){
                ?>
                <li>
                    <label>
                        <span>
                        Urgent Ugrade :
                        </span>
                        <small>
                        <?php echo "Expired on ".date(DATE_TIME_FORMAT,$urgent_upgrade_end_date); ?>
                        </small>
                    </label>
                    <button type="button" class="btn upgrade_project" data-attr= "<?php echo $expired_project_value['project_id']  ?>" data-action-type="prolong_availability_urgent">Prolong Availability</button>
                </li>
                <?php
                }
                ?>
            </ul>
            </div>
            <div class="pDBttm">
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-12 baDges">									
                        <div class="pdButton" id="<?php echo "open_for_bidding_project_upgrade_badges_".$open_bidding_project_value['project_id'] ?>">
                            <?php
                            if($expired_project_value['featured'] == 'Y' && $featured_upgrade_end_date > time() ){
                            ?>
                            <button type="button" class="btn">Featured</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($expired_project_value['urgent'] == 'Y' && $urgent_upgrade_end_date > time() ){
                            ?>
                            <button type="button" class="btn urgent">Urgent</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($expired_project_value['sealed'] == 'Y'){
                            ?>
                            <button type="button" class="btn">Sealed</button>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 actOnly">
                        <div class="myAction">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    Action
								</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  id="<?php echo "open_for_bidding_project_drop_down_action_".$expired_project_value['project_id'] ?>">
                                    <a class="dropdown-item cancel_project" data-project-status="expired" data-attr= "<?php echo $expired_project_value['project_id']  ?>" style="cursor:pointer">Cancel Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>										
    
<?php
    }
}
?>
<?php
    if($expired_project_cnt > $this->config->item('user_dashboard_expired_projects_listing_limit')) {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
    <a href="">view more</a>
</div>
<?php
    }
?>


<div class="mpNoPrj" style="<?php echo $expired_project_button_show_status ?>">
    <h5><?=$this->config->item('no_expired_project_message')?></h5>
</div>