<?php
	$open_bidding_project_button_show_status = "display:block";
	
if(!empty($open_bidding_project_data)){
	$open_bidding_project_button_show_status = "display:none";
	foreach($open_bidding_project_data as $open_bidding_project_key => $open_bidding_project_value){
	
		
		$featured_max = 0;
		$urgent_max = 0;
		$expiration_featured_upgrade_date_array = array();
		$expiration_urgent_upgrade_date_array = array();
		if(!empty($open_bidding_project_value['featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['featured_upgrade_end_date'];
		}
		if(!empty($open_bidding_project_value['bounus_featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['bounus_featured_upgrade_end_date'];
		}
		if(!empty($open_bidding_project_value['membership_include_featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['membership_include_featured_upgrade_end_date'];
		}
		if(!empty($expiration_featured_upgrade_date_array)){
			$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
		}
		
		##########
		
		if(!empty($open_bidding_project_value['urgent_upgrade_end_date'])){
			$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['urgent_upgrade_end_date'];
		}
		if(!empty($open_bidding_project_value['bonus_urgent_upgrade_end_date'])){
			$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['bonus_urgent_upgrade_end_date'];
		}
		if(!empty($open_bidding_project_value['membership_include_urgent_upgrade_end_date'])){
			$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['membership_include_urgent_upgrade_end_date'];
		}
		if(!empty($expiration_urgent_upgrade_date_array)){
			$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
		}
		
		$featured_class = '';
		if($open_bidding_project_value['featured'] == 'Y' && $featured_max >= time()){
			$featured_class = 'opBg';
		}
		
		
		if($open_bidding_project_value['escrow_payment_method'] == 'Y'){
			$payment_method = 'via Escrow system';
			}
		if($open_bidding_project_value['offline_payment_method'] == 'Y'){
			$payment_method = 'via Offline system';
		}
		$location = '';
		if(!empty($open_bidding_project_value['county_name'])){
		if(!empty($open_bidding_project_value['locality_name'])){
			$location .= '&nbsp;'.$open_bidding_project_value['locality_name'];
		}
		if(!empty($open_bidding_project_value['postal_code'])){
			$location .= ',&nbsp;'.$open_bidding_project_value['postal_code'] .',&nbsp;';
		}else{
			$location .= ',&nbsp';
		}
		$location .= $open_bidding_project_value['county_name'];
		}
		
?>
	<div class="tabContent">
		<div class="opLBttm <?php echo $featured_class?>" id="<?php echo "open_for_bidding_project_".$open_bidding_project_value['project_id'] ?>">
			<div class="wiP">
			<a href="<?php echo base_url().$this->config->item('project_detail_page_url')."?id=".$open_bidding_project_value['project_id']; ?>">
				<?=$open_bidding_project_value['project_title']?>
			</a>
			</div>
			<label>
				<small><i class="far fa-clock"></i><?php echo date(DATE_TIME_FORMAT,strtotime($open_bidding_project_value['project_posting_date'])) ?></small>
				<small>
					<i class="far fa-credit-card"></i>
					<?php echo ucfirst($open_bidding_project_value['project_type']).' budget'; ?>
					<?php
					//if($open_bidding_project_value['project_type'] == 'fixed'){
						if($open_bidding_project_value['confidential_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
						}else if($open_bidding_project_value['not_sure_dropdown_option_selected'] == 'Y'){
							$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
						}else{
								if($open_bidding_project_value['max_budget'] != 'All'){
									$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($open_bidding_project_value['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($open_bidding_project_value['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
									}else{
									$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($open_bidding_project_value['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
								}
							}
						//}
						echo $budget_range;
						if($open_bidding_project_value['escrow_payment_method'] == 'Y') {
							echo '- Payment Method: Safe Pay Escrow';
						}
					?>
				</small>
				<?php
				if(!empty($location)){
				?>
				<small><i class="fas fa-map-marker-alt"></i><?php echo $location;?></small>
				<?php
				}
				?>
				<small><i class="fas fa-bullhorn"></i>0 Bid</small>
			</label>
			<div class="osu1">
				<?php
				$description = strip_tags($open_bidding_project_value['project_description']);
				?>
				<div class="project_description_my_project desktop-secreen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_dekstop'));
					?>
					</p>
				</div>
				<div class="project_description_my_project ipad-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_tablet'));
					?>
					</p>
				</div>
				<div class="project_description_my_project mobile-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_mobile'));
					?>
					</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="pDSheduled project_upgrade_prolong">
			<ul id="<?php echo "open_for_bidding_project_upgrade_prolong_availability_".$open_bidding_project_value['project_id'] ?>">
				<?php
				
				if($open_bidding_project_value['featured'] == 'Y'&& $featured_max != 0 && $featured_max > time()){
				
				?>
				<li>
					<label>
						<span>
						Featured Upgrade :
						</span> <small> <?php echo $this->config->item('project_details_page_expires_on')."&nbsp;".date(DATE_TIME_FORMAT,$featured_max); ?>
						</small>
					</label>
					<button type="button" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" class="btn upgrade_project" data-action-type="prolong_availability_featured">Prolong Availability</button>
				</li>
				<?php
				}
				
				if($open_bidding_project_value['urgent'] == 'Y'&& $urgent_max != 0 && $urgent_max > time()){
				?>
				<li>
					<label>
						<span>
						Urgent Ugrade :
						</span>
						<small>
						<?php echo $this->config->item('project_details_page_expires_on')."&nbsp;".date(DATE_TIME_FORMAT,$urgent_max); ?>
						</small>
					</label>
					<button type="button" class="btn upgrade_project" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" data-action-type="prolong_availability_urgent">Prolong Availability</button>
				</li>
				<?php
				}
				?>
			</ul>
			</div>
			<div class="pDBttm">
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-12 baDges">									
						<div class="pdButton" id="<?php echo "open_for_bidding_project_upgrade_badges_".$open_bidding_project_value['project_id'] ?>">
							<?php
							if($open_bidding_project_value['featured'] == 'Y'&& $featured_max != 0 && $featured_max > time()){
							?>
							<button type="button" class="btn">Featured</button>
							<?php
							}
							?>
							<?php
							if($open_bidding_project_value['urgent'] == 'Y'&& $urgent_max != 0 && $urgent_max > time()){
							?>
							<button type="button" class="btn urgent">Urgent</button>
							<?php
							}
							?>
							<?php
							if($open_bidding_project_value['sealed'] == 'Y'){
							?>
							<button type="button" class="btn">Sealed</button>
							<?php
							}
							?>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 actOnly">
						<div class="myAction">
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  id="<?php echo "open_for_bidding_project_drop_down_action_".$open_bidding_project_value['project_id'] ?>">
									<a class="dropdown-item edit_project" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" style="cursor:pointer">Edit Project</a>
									<a class="dropdown-item cancel_project" data-project-status="open" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" style="cursor:pointer">Cancel Project</a>
									
									<?php
									if($open_bidding_project_value['featured'] == 'N' && $open_bidding_project_value['urgent'] == 'N' && empty($featured_max) && empty($urgent_max)){
									?>
										<a id="<?php echo "upgrade_project_".$open_bidding_project_value['project_id'] ?>" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" data-action-type="upgrade_project">Upgrade Project</a>
									<?php
									}
									elseif($open_bidding_project_value['featured'] == 'N' && $open_bidding_project_value['urgent'] == 'Y' && empty($featured_max) ){
									?>
									
									<a id="<?php echo "upgrade_project_".$open_bidding_project_value['project_id'] ?>" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" data-action-type="upgrade_as_featured_project">Upgrade As Featured Project</a>
									<?php
									
									}elseif($open_bidding_project_value['featured'] == 'Y' && $open_bidding_project_value['urgent'] == 'N' && empty($urgent_max)){
									?>
									<a id="<?php echo "upgrade_project_".$open_bidding_project_value['project_id'] ?>" class="dropdown-item upgrade_project" style="cursor:pointer" data-attr= "<?php echo $open_bidding_project_value['project_id']  ?>" data-action-type="upgrade_as_urgent_project">Upgrade As Urgent Project</a>
									<?php
									}
									?>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>										
	
<?php
	}
}
?>
<?php
	if($open_bidding_project_cnt > $this->config->item('user_dashboard_open_bidding_projects_listing_limit')) {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
	<a href="">view more</a>
</div>
<?php
	}
?>
<div class="mpNoPrj" style="<?php echo $open_bidding_project_button_show_status ?>">
	<h5><?=$this->config->item('no_open_bidding_project_message')?></h5>
</div>