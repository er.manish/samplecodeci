<?php echo $this->load->view('header.php');

$CI = & get_instance ();
$CI->load->library('Cryptor');
$payment_method = '';
$location = '';
if($project_data[0]['escrow_payment_method'] == 'Y'){
	$payment_method = 'via Escrow system';
	}
if($project_data[0]['offline_payment_method'] == 'Y'){
	$payment_method = 'via Offline system';
}
if(!empty($project_data[0]['county_name'])){
	if(!empty($project_data[0]['locality_name'])){
		$location .= '&nbsp'.$project_data[0]['locality_name'];
	}
	if(!empty($project_data[0]['postal_code'])){
	$location .= '&nbsp'.$project_data[0]['postal_code'] .',&nbsp';
	}else{
		$location .= ',&nbsp';
	}
	$location .= $project_data[0]['county_name'];
}	
?>
<div class="dashTop">
	<!-- Upload Image Cover Section Start -->
	<!-- <div class="uploadImg">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
				<div id="topContainer" class="bgposition" style="background-image: url('<?php echo URL ?>assets/images/post_project/banner.jpg');"></div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div> -->
	<!-- Upload Image Cover Section End -->		
	<!-- Middle Section Start -->
	<div class="row">
		<div class="col-md-2 col-sm-2 col-xs-12 pr0">				
			
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12 pojDet">
			<!-- Project Details Start -->				
			<div class="proDtls mb25">
				<div class="pD">
					<strong>Project Details</strong>
					<span>Awaiting Moderation</span>
					<div class="clearfix"></div>
				</div>
				<div class="pDtls">
					<h4><?php echo $project_data[0]['project_title']; ?></h4>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 pDetailsL">
							<div class="pDSheduled">
								<label>
									<span>
										<i class="fa fa-file-text-o" aria-hidden="true"></i>
										Project type :
									</span>
									<small>
									<?php
									//if($project_data[0]['project_type'] == 'fixed'){
										echo ucfirst($project_data[0]['project_type']). "&nbspBudget"; 
									//}
									?>
									</small>
								</label>
								<label>
									<span>
										<i class="fa fa-credit-card" aria-hidden="true"></i>
										Project Budget :
									</span>
									<small>
									<?php
									//if($project_data[0]['project_type'] == 'fixed'){
									if($project_data[0]['confidential_dropdown_option_selected'] == 'Y'){
										$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
									}else if($project_data[0]['not_sure_dropdown_option_selected'] == 'Y'){
										$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
									}else{
											if($project_data[0]['max_budget'] != 'All'){
												$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($project_data[0]['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($project_data[0]['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
												}else{
												$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($project_data[0]['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
											}
										}
									//}
									echo $budget_range;
									?>	
									</small>
								</label>
								<?php
								if(!empty($payment_method)){
								?>
								<label>
									<span>
										<i class="fa fa-credit-card" aria-hidden="true"></i>
										Payment Method :
									</span>
									<small><?php echo $payment_method; ?></small>
								</label>
								<?php
								}
								if(!empty($location)){
								?>
								<label>
									<span>
										<i class="fa fa-map-marker" aria-hidden="true"></i>
										Location :
									</span>
									<small><?php echo $location; ?></small>
								</label>
								<?php
								}
								?>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 pDetailsR">
							<div class="pProject">
								<?php
								if(!empty($project_category_data)){
									foreach($project_category_data as $category_key=>$category_value){
										if(!empty($category_value['parent_category_name']) && !empty($category_value['category_name'])){
								?>
										<div class="clearfix">
											<small class="pSmnu"><?php echo $category_value['parent_category_name']; ?></small>
											<a href="#">
												<span><?php echo $category_value['category_name']; ?></span>
											</a>
										</div>
								<?php
										}else{
											echo '<small>'.$category_value['category_name'].'</small>'; 
										}
									}
								}
								?>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="pDBttm">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">									
								<div class="pdButton">
									<?php
									if($project_data[0]['featured'] == 'Y'){
										echo '<button type="button" class="btn">Featured</button>';
									}if($project_data[0]['urgent'] == 'Y'){
										echo '<button type="button" class="btn urgent">Urgent</button>';
									}
									if($project_data[0]['sealed'] == 'Y'){
										echo '<button type="button" class="btn">Sealed</button>';
									}
									if($project_data[0]['hidden'] == 'Y'){
										echo '<button type="button" class="btn">Hidden</button>';
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Project Details End -->
			<!-- Description Start -->
			<div class="proDesc">
				<div class="pDesc">
					<strong>Description</strong>
					<div class="clearfix"></div>
				</div>
				<div class="proDn">
					<div class="proPart">
						<h6 class="line-break"><?php echo nl2br($project_data[0]['project_description']); ?></h6>
					</div>
					
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<?php
							if(!empty($project_attachment_data)){
							?>
							<div class="pDAttach">
								<?php
								foreach($project_attachment_data as $project_attachment_key=>$project_attachment_value){
									$attachment_id = Cryptor::doEncrypt($project_attachment_value['id']);
									echo '<label><span><a href="javascript:void(0);" class="download_attachment" data-attr="'.$attachment_id.'">'.$project_attachment_value['project_attachment_name'].'</a></span></label>';
								}
								?>
							</div>
							<?php
							}
							?>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12"></div>						
						<div class="portTags">
							<div class="row">
								<div class="col-md-8 col-sm-8 col-xs-12">
									<?php
									if(!empty($project_tag_data)){
									?>
									<div class="smallTag">
										<?php
										foreach($project_tag_data as $project_tag_key=>$project_tag_value){
											echo '<p><span>#</span><small>'.$project_tag_value['awaiting_moderation_project_tag_name'].'</small></p>';
										}
										?>
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Description End -->
			
		</div>
	</div>
	<!-- Middle Section End -->
</div>

<!-- Script Start -->

<!-- Script End -->
<div class="modal alert-popup" id="error_popup" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <div class="modal-header-inner">
				<img style="" src="<?=ASSETS?>images/post_project/alert-icon.png" alt="alert icon" class="img-fluid" />
				<h4 class="modal-title" id="error_popup_heading"></h4>
			  </div>
			</div>
			<div class="modal-body text-center">
			  <p id="error_popup_body"></p>
			</div>
	    </div>
	</div>
</div>
<script>
//var project_attachment_popup_error_heading = "<?php echo $this->config->item('project_attachment_popup_error_heading'); ?>";
var popup_error_heading = "<?php echo $this->config->item('popup_error_heading'); ?>";
var project_attachment_not_exist_temporary_project_preview_validation_post_project_message = "<?php echo $this->config->item('project_attachment_not_exist_temporary_project_preview_validation_post_project_message'); ?>";
var dashboard_page_url = "<?php echo $this->config->item('dashboard_page_url'); ?>";
var project_id = "<?php echo $project_id; ?>";
var project_status = "<?php echo 'awaiting_moderation'; ?>";
</script>
<script src="<?php echo JS; ?>modules/project_detail.js"></script>
<?php echo $this->load->view('footer.php'); ?>