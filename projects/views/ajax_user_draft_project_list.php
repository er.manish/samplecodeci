<?php
	$post_draft_project_button_show_status = "display:block";
if(!empty($draft_project_data)){
	$post_draft_project_button_show_status = "display:none;";
	foreach($draft_project_data as $draft_project_key=>$draft_project_value){
		$featured_class = '';
		if($draft_project_value['featured'] == 'Y') {
			$featured_class = 'opBg';
		}
		$location = '';
		if(!empty($draft_project_value['county_name'])){
		if(!empty($draft_project_value['locality_name'])){
			$location .= '&nbsp'.$draft_project_value['locality_name'];
		}
		if(!empty($draft_project_value['postal_code'])){
			$location .= ',&nbsp'.$draft_project_value['postal_code'] .',&nbsp';
		}else{
			$location .= ',&nbsp';
		}
		$location .= $draft_project_value['county_name'];
		}
		
		
		
?>
	<div class="tabContent" id="<?php echo 'draft_project_'.$draft_project_value['project_id']; ?>">
		<div class="opLBttm <?php echo $featured_class; ?>">
			<h4>
				<?php echo $draft_project_value['project_title'] ?>
			</h4>
			<!-- <span class="opOpen">Open</span> -->
			<label>
				<small><i class="far fa-clock"></i><?php echo date(DATE_TIME_FORMAT,strtotime($draft_project_value['project_save_date'])) ?></small>
				<small>
				<i class="far fa-credit-card"></i>
				<?php echo ucfirst($draft_project_value['project_type']).' budget'; ?>
				<?php
				//if($draft_project_value['project_type'] == 'fixed'){
					if($draft_project_value['confidential_dropdown_option_selected'] == 'Y'){
						$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_confidential_option_selected');
					}else if($draft_project_value['not_sure_dropdown_option_selected'] == 'Y'){
						$budget_range = $this->config->item('displayed_text_fixed_budget_project_details_page_budget_not_sure_option_selected');
					}else{
							if($draft_project_value['max_budget'] != 'All'){
								$budget_range = $this->config->item('post_project_budget_range_between').'&nbsp;'.number_format($draft_project_value['min_budget'], 0, '', ' '). '&nbsp;'.CURRENCY .'&nbsp;'. $this->config->item('post_project_budget_range_and').'&nbsp;'.number_format($draft_project_value['max_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
								}else{
								$budget_range = $this->config->item('post_project_budget_range_more_then').'&nbsp'.number_format($draft_project_value['min_budget'], 0, '', ' ').'&nbsp'.CURRENCY;
							}
						}
					//}
					echo $budget_range;
					if($draft_project_value['escrow_payment_method'] == 'Y') {
						echo '- Payment Method: Safe Pay Escrow';
					}
				?>
			</small>
			<?php
			if(!empty($location)){
			?>
			<small><i class="fas fa-map-marker-alt"></i><?php $location;?></small>
			<?php } ?>
			</label>
			<div class="osu1">
				<?php
				$description = strip_tags($draft_project_value['project_description']);
				?>
				<div class="project_description_my_project desktop-secreen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_dekstop'));
					?>
					</p>
				</div>
				<div class="project_description_my_project ipad-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_tablet'));
					?>
					</p>
				</div>
				<div class="project_description_my_project mobile-screen">
					<p><?php 
					echo limitString($description,$this->config->item('dashboard_my_projects_section_project_description_character_limit_mobile'));
					?>
					</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="pDBttm">
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-12 baDges">									
						<div class="pdButton">
							<?php
								if($draft_project_value['featured'] == 'Y') {
							?>
							<button type="button" class="btn">Featured</button>
							<?php
								}
							?>
							<?php
								if($draft_project_value['urgent'] == 'Y') {
							?>
							<button type="button" class="btn urgent">Urgent</button>
							<?php
								}
							?>
							<?php
								if($draft_project_value['sealed'] == 'Y') {
							?>
							<button type="button" class="btn">Sealed</button>
							<?php
								}
							?>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 actOnly">
						<div class="myAction">
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item edit_draft" data-attr= "<?php echo $draft_project_value['project_id']  ?>" style="cursor:pointer">Edit Draft</a>
									<a class="dropdown-item remove_draft"style="cursor:pointer" data-attr= "<?php echo $draft_project_value['project_id']  ?>">Remove Draft</a>
									<a class="dropdown-item publish_project" style="cursor:pointer" data-attr= "<?php echo $draft_project_value['project_id']  ?>">Publish Project</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
<?php
	}
}
?>
<?php
	if($draft_project_cnt > $this->config->item('user_dashboard_draft_projects_listing_limit')) {
?>
<div class="col-sm-12 col-lg-12 col-xs-12">
	<a href="">view more</a>
</div>
<?php
	}
?>
<div class="mpNoPrj" style="<?php echo $post_draft_project_button_show_status ?>">
	<h5>You do not currently have any project saved as Draft.</h5>
	<p>Go ahead and post a project NOW!</p>
	<div class="crPro">
		<button type="button" class="btn create_project">Create Project</button>
	</div>
</div>