<?php
if ( ! defined ('BASEPATH'))
{
    exit ('No direct script access allowed');
}
class Projects_model extends BaseModel
{
    public function __construct ()
    {
        return parent::__construct ();
    }
	
	
	/**
	* This function is used to count the total open projects of user(included open for biddding project and expired project) .
	*/
	public function get_user_open_projects_count($user_id){
	
		$user_total_open_projects_count = 0;
		$user_open_bidding_project_count = $this->db->where(['project_owner_id' => $user_id])->from('projects_open_bidding')->count_all_results();
		$user_expired_project_count = $this->db->where(['project_owner_id' => $user_id])->from('fixed_budget_projects_expired')->count_all_results();
		$user_total_open_projects_count = $user_open_bidding_project_count + $user_expired_project_count;
		return $user_total_open_projects_count;
		
	}
	
	
	/**
	* This function is used to return the table name and project status .
	*/
	public function get_project_status_table_name($project_id){
		if(!empty($project_id)){
			$project_status_table_name_array = array('project_status'=>'','table_name'=>'');
			$status = '';
			$count_awaiting_moderation_project = $this->db 
			->select ('id')
			->from ('projects_awaiting_moderation')
			->where('project_id',$project_id)
			->get ()->num_rows ();
			if($count_awaiting_moderation_project > 0){
				$table_name = 'projects_awaiting_moderation';
				$project_status = 'awaiting_moderation';
				$project_status_table_name_array = array('project_status'=>$project_status,'table_name'=>$table_name);
			}else{
				// count the project from projects_open_bidding table
				$count_open_bidding_project = $this->db 
				->select ('id')
				->from ('projects_open_bidding')
				->where('project_id',$project_id)
				//->where('project_owner_id',$project_owner_id)
				->get ()->num_rows ();
				if($count_open_bidding_project > 0){
					$table_name = 'projects_open_bidding';
					$project_status = 'open_for_bidding';
					$project_status_table_name_array = array('project_status'=>$project_status,'table_name'=>$table_name);
				}else{
				
					// count the project from fixed_budget_projects_expired table
					$count_fixed_budget_expired_project = $this->db 
					->select ('id')
					->from ('fixed_budget_projects_expired')
					->where('project_id',$project_id)
					//->where('project_owner_id',$project_owner_id)
					->get ()->num_rows ();
					if($count_fixed_budget_expired_project > 0){
					
						$table_name = 'fixed_budget_projects_expired';
						$project_status = 'fixed_budget_expired';
						$project_status_table_name_array = array('project_status'=>$project_status,'table_name'=>$table_name);
					
					}
				}
			
			}
			return $project_status_table_name_array;
		}else{
			show_404();
		}
	
	} 
	
	
	/**
	* This function is used to return the list of tag of projects from tables .
	*/
	public function get_project_tags($project_id,$project_status){
		if(!empty($project_id) && !empty($project_status)){	
			########## fetch the draft project attachments ###
			if($project_status == 'awaiting_moderation')
			{
				$this->db->select('awaiting_moderation_project_tag_name');
				$this->db->from('awaiting_moderation_projects_tags');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_tag_result = $this->db->get();
				$project_tag_data = $project_tag_result->result_array();
			
			}elseif($project_status == 'open_for_bidding'){
				$this->db->select('project_tag_name');
				$this->db->from('projects_tags');
				$this->db->where('project_id',$project_id);
				$this->db->order_by('id',"asc");
				$project_tag_result = $this->db->get();
				$project_tag_data = $project_tag_result->result_array();
			}
			return $project_tag_data;
		}else{
			show_404();
		}
	}
	
	/**
	*	This function is used to return the list of categories of projects from tables .
	*/
	public function get_project_categories($project_id,$project_status){
		
		if(!empty($project_id) && !empty($project_status)){
			
			########## fetch the project categories(awaiting moderation,open for bidding etc) ###
			if($project_status == 'draft')
			{
				$this->db->select('category_project.name as category_name,parent_category_project.name as parent_category_name');
				$this->db->from('draft_projects_categories_listing_tracking as category_tracking');
				$this->db->join('categories_projects as category_project', 'category_project.id = category_tracking.draft_project_category_id AND category_project.status = "Y"', 'left');
				$this->db->join('categories_projects as parent_category_project', 'parent_category_project.id = category_tracking.draft_project_parent_category_id AND parent_category_project.status = "Y"', 'left');
				$this->db->where('category_tracking.project_id',$project_id);
				$this->db->where('category_project.status', 'Y');
				$this->db->order_by('category_project.name',"asc");
				$category_result = $this->db->get();
				$category_data = $category_result->result_array();
			
			}
			if($project_status == 'awaiting_moderation')
			{
				$this->db->select('category_project.name as category_name,parent_category_project.name as parent_category_name');
				$this->db->from('awaiting_moderation_projects_categories_listing_tracking as category_tracking');
				$this->db->join('categories_projects as category_project', 'category_project.id = category_tracking.awaiting_moderation_project_category_id AND category_project.status = "Y"', 'left');
				$this->db->join('categories_projects as parent_category_project', 'parent_category_project.id = category_tracking.awaiting_moderation_project_parent_category_id AND parent_category_project.status = "Y"', 'left');
				$this->db->where('category_tracking.project_id',$project_id);
				$this->db->order_by('category_project.name',"asc");
				$category_result = $this->db->get();
				$category_data = $category_result->result_array();
			
			
			} elseif($project_status == 'open_for_bidding' || $project_status == 'expired' || $project_status == 'cancelled'){
				$this->db->select('category_project.name as category_name,parent_category_project.name as parent_category_name');
				$this->db->from('projects_categories_listing_tracking as category_tracking');
				$this->db->join('categories_projects as category_project', 'category_project.id = category_tracking.project_category_id AND category_project.status = "Y"', 'left');
				$this->db->join('categories_projects as parent_category_project', 'parent_category_project.id = category_tracking.project_parent_category_id AND parent_category_project.status = "Y"', 'left');
				$this->db->where('category_tracking.project_id',$project_id);
				$this->db->order_by('category_project.name',"asc");
				$category_result = $this->db->get();
				$category_data = $category_result->result_array();
			}
			if(!empty($category_data)){
				foreach($category_data as $key=>$value){
					if(!empty($value['parent_category_name'])){
						$category_data[$key]['category_order_value'] =  $value['parent_category_name'];
					}else{
						$category_data[$key]['category_order_value'] =  $value['category_name'];
					}
				
				}
				sortArrayBySpecificKeyValue('category_order_value',$category_data, 'asc');// sort category array by key:category_order_value value
			}
			
			return $category_data;
		}else{
			show_404();
		}
	}
   
	/**
     * Manage standard project refresh called from projects/ajax_update_latest_project_dashboard_view
     */
	public function manage_standard_project_refresh($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('standard_project_refresh_sequence'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
		if(!empty($valid_time_arr)) { // standard refresh sequence only exectue if standard project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code,u.profile_name');
			$this->db->from('projects_open_bidding op');
			$this->db->join('counties', 'counties.id = op.county_id', 'left');
			$this->db->join('localities', 'localities.id = op.locality_id', 'left');
			$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
			$this->db->where('op.project_expiration_date >= NOW()');
			$this->db->where('(op.featured = "N" AND op.urgent = "N" AND op.sealed = "Y" AND op.hidden = "N")');
			$this->db->join('users u', 'op.project_owner_id = u.user_id', 'left');
			$this->db->where('op.project_owner_id !=', $user[0]->user_id);
			$this->db->order_by('op.id','desc');
			$standard_projects = $this->db->get()->result_array();

			if(!empty($standard_projects)) {
				foreach($standard_projects as $standard_project) {
					$this->db->select('*');
					$this->db->from('projects_refresh_sequence_tracking');
					$this->db->where('project_id', $standard_project['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$standard_refresh = $this->db->get()->row_array();
					if(!empty($standard_refresh)) {
						if(strtotime($standard_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$index = array_search($standard_refresh['project_id'], $left_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$index];
									unset($data['open_bidding_project_left_side'][$index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $standard_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($standard_refresh['project_next_refresh_time'], $time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($standard_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($standard_project['project_expiration_date'])) {
									$next_refresh_date = null;
								}
									$next_refresh_time[$standard_project['project_id']] = [
										'project_title' => $standard_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'standard',
										'profile' => $standard_project['profile_name']
                                    ];
                                    
								$refresh_tracking = [
									'project_id' => $standard_refresh['project_id'],
									'project_upgrade_purchase_tracking_id' => null,
									'project_last_refresh_time' => $standard_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('projects_refresh_sequence_tracking', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('projects_refresh_sequence_tracking', $insert_id, $standard_refresh['project_id'], $standard_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$standard_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}

						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time];
	}
	/**
	 * get immediate next refresh time for refreshed project
	 */
	public function get_immediate_next_refresh_time_for_refreshed_project($table, $id, $project_id, $last_refresh_time) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('project_id', $project_id);
		$this->db->where('id != ', $id);
		$this->db->where('project_next_refresh_time >', $last_refresh_time);
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
    /**
     * manage featured project refresh for real payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_featured_project_refresh_for_real_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_featured'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // featured refresh sequence only exectue if featured project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('upt.*, u.profile_name');
			$this->db->from('projects_upgrades_purchase_tracking upt');
			$this->db->where('upt.project_upgrade_type', 'featured');
			$this->db->join('users u', 'upt.project_owner_id = u.user_id', 'left');
			$this->db->where('upt.project_upgrade_end_date >= NOW()');
			$this->db->where('upt.project_owner_id !=', $user[0]->user_id);
            $featured_purchase = $this->db->get()->result_array();
            
			if(!empty($featured_purchase)) {
				
				foreach($featured_purchase as $featured) {
					$this->db->select('*');
					$this->db->from('projects_refresh_sequence_tracking');
					$this->db->where('project_id', $featured['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $featured['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$featured_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $featured['project_id']);
                    $featured_project = $this->db->get()->row_array();
					if(!empty($featured_refresh) && !empty($featured_project)) {
						if(strtotime($featured_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($featured_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $featured_project);
									array_pop($data['open_bidding_project_right_side']);
								}
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($featured_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $featured_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
                                $next_refresh_date = get_next_refresh_time($featured_refresh['project_next_refresh_time'], $time_arr);
                                
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($featured_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($featured_project['project_expiration_date'])) {
									$next_refresh_date = null;
								}
									$next_refresh_time[$featured_project['project_id']] = [
										'project_title' => $featured_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $featured['profile_name']
									];
								
								$refresh_tracking = [
									'project_id' => $featured['project_id'],
									'project_upgrade_purchase_tracking_id' => $featured['id'],
									'project_last_refresh_time' => $featured_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('projects_refresh_sequence_tracking', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('projects_refresh_sequence_tracking', $insert_id, $featured['project_id'], $featured_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$featured_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage featured project refresh for membership included payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_featured_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_featured'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // featured refresh sequence only exectue if featured project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('mupt.*, u.profile_name');
			$this->db->from('proj_membership_included_upgrades_purchase_tracking mupt');
			$this->db->join('users u', 'mupt.project_owner_id = u.user_id', 'left');
			$this->db->where('mupt.project_upgrade_type', 'featured');
			$this->db->where('mupt.project_upgrade_end_date >= NOW()');
			$this->db->where('mupt.project_owner_id !=', $user[0]->user_id);
            $featured_purchase = $this->db->get()->result_array();
            
			if(!empty($featured_purchase)) {
				
				foreach($featured_purchase as $featured) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_tracking_membership_included_upgrades');
					$this->db->where('project_id', $featured['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $featured['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$featured_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $featured['project_id']);
                    $featured_project = $this->db->get()->row_array();
					if(!empty($featured_refresh) && !empty($featured_project)) {
						if(strtotime($featured_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($featured_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $featured_project);
									array_pop($data['open_bidding_project_right_side']);
								}
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($featured_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $featured_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
                                $next_refresh_date = get_next_refresh_time($featured_refresh['project_next_refresh_time'], $time_arr);
                                
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($featured_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($featured_project['project_expiration_date'])) {
									$next_refresh_date = null;
								}
									$next_refresh_time[$featured_project['project_id']] = [
										'project_title' => $featured_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $featured['profile_name']
									];
								
								$refresh_tracking = [
									'project_id' => $featured['project_id'],
									'project_upgrade_purchase_tracking_id' => $featured['id'],
									'project_last_refresh_time' => $featured_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_tracking_membership_included_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_tracking_membership_included_upgrades', $insert_id, $featured['project_id'], $featured_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$featured_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage featured project refresh for bonus based payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_featured_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_featured'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // featured refresh sequence only exectue if featured project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('bput.*, u.profile_name');
			$this->db->from('proj_bonus_based_purchased_upgrades_tracking bput');
			$this->db->join('users u', 'bput.project_owner_id = u.user_id', 'left');
			$this->db->where('bput.project_upgrade_type', 'featured');
			$this->db->where('bput.project_upgrade_end_date >= NOW()');
			$this->db->where('bput.project_owner_id !=', $user[0]->user_id);
            $featured_purchase = $this->db->get()->result_array();
            
			if(!empty($featured_purchase)) {
				
				foreach($featured_purchase as $featured) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_track_bonus_based_purchased_upgrades');
					$this->db->where('project_id', $featured['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $featured['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$featured_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $featured['project_id']);
                    $featured_project = $this->db->get()->row_array();
					if(!empty($featured_refresh) && !empty($featured_project)) {
						if(strtotime($featured_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($featured_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $featured_project);
									array_pop($data['open_bidding_project_right_side']);
								}
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($featured_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $featured_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
                                $next_refresh_date = get_next_refresh_time($featured_refresh['project_next_refresh_time'], $time_arr);
                                
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($featured_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($featured_project['project_expiration_date'])) {
									$next_refresh_date = null;
								}
									$next_refresh_time[$featured_project['project_id']] = [
										'project_title' => $featured_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $featured['profile_name'] 
									];
								
								$refresh_tracking = [
									'project_id' => $featured['project_id'],
									'project_upgrade_purchase_tracking_id' => $featured['id'],
									'project_last_refresh_time' => $featured_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $insert_id, $featured['project_id'], $featured_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$featured_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								if(count($featured_purchase) == 1 && $next_refresh_date == null ) {
									// update featured column state in projects_open_bidding table
									$update_data = [
										'featured' => 'N'
									];
									$this->db->update('projects_open_bidding', $update_data, ['project_id' => $featured['project_id']]);
								}
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage urgent project refresh called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_urgent_project_refresh_for_real_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_urgent'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // urgent refresh sequence only exectue if urgent project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('upt.*, u.profile_name');
			$this->db->from('projects_upgrades_purchase_tracking upt');
			$this->db->join('users u', 'upt.project_owner_id = u.user_id', 'left');
			$this->db->where('upt.project_upgrade_type', 'urgent');
			$this->db->where('upt.project_upgrade_end_date >= NOW()');
			$this->db->where('upt.project_owner_id !=', $user[0]->user_id);
			$urgent_purchase = $this->db->get()->result_array();
			if(!empty($urgent_purchase)) {
				
				foreach($urgent_purchase as $urgent) {
					$this->db->select('*');
					$this->db->from('projects_refresh_sequence_tracking');
					$this->db->where('project_id', $urgent['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $urgent['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$urgent_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $urgent['project_id']);
					$urgent_project = $this->db->get()->row_array();
					if(!empty($urgent_refresh) && !empty($urgent_project)) {
						if(strtotime($urgent_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
                                $index = array_search($urgent_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $urgent_project);
									array_pop($data['open_bidding_project_right_side']);
                                }
                                
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($urgent_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $urgent_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($urgent_refresh['project_next_refresh_time'],$time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($urgent_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
								
								if(array_key_exists($urgent_project['project_id'], $next_refresh_time)) {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $urgent['profile_name']
									];
								} else {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'profile' => $urgent['profile_name']
									];
								}
									
								
								$refresh_tracking = [
									'project_id' => $urgent['project_id'],
									'project_upgrade_purchase_tracking_id' => $urgent['id'],
									'project_last_refresh_time' => $urgent_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('projects_refresh_sequence_tracking', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('projects_refresh_sequence_tracking', $insert_id, $urgent['project_id'], $urgent_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$urgent_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage urgent project refresh membership included payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_urgent_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_urgent'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // urgent refresh sequence only exectue if urgent project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('mupt.*, u.profile_name');
			$this->db->from('proj_membership_included_upgrades_purchase_tracking mupt');
			$this->db->join('users u', 'mupt.project_owner_id = u.user_id', 'left');
			$this->db->where('mupt.project_upgrade_type', 'urgent');
			$this->db->where('mupt.project_upgrade_end_date >= NOW()');
			$this->db->where('mupt.project_owner_id !=', $user[0]->user_id);
			$urgent_purchase = $this->db->get()->result_array();
			if(!empty($urgent_purchase)) {
				
				foreach($urgent_purchase as $urgent) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_tracking_membership_included_upgrades');
					$this->db->where('project_id', $urgent['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $urgent['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$urgent_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $urgent['project_id']);
					$urgent_project = $this->db->get()->row_array();
					if(!empty($urgent_refresh) && !empty($urgent_project)) {
						if(strtotime($urgent_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
                                $index = array_search($urgent_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $urgent_project);
									array_pop($data['open_bidding_project_right_side']);
                                }
                                
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($urgent_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $urgent_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($urgent_refresh['project_next_refresh_time'],$time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($urgent_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
								if(array_key_exists($urgent_project['project_id'], $next_refresh_time)) {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $urgent['profile_name']
									];
								} else {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'urgent',
										'profile' => $urgent['profile_name']
									];
								}
								$refresh_tracking = [
									'project_id' => $urgent['project_id'],
									'project_upgrade_purchase_tracking_id' => $urgent['id'],
									'project_last_refresh_time' => $urgent_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_tracking_membership_included_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_tracking_membership_included_upgrades', $insert_id, $urgent['project_id'], $urgent_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$urgent_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage urgent project refresh bonus based payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_urgent_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_urgent'));
		$check_valid_arr = array_map('getInt', $time_arr); 
        $valid_time_arr = array_filter($check_valid_arr);
        
		if(!empty($valid_time_arr)) { // urgent refresh sequence only exectue if urgent project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('bupt.*, u.profile_name');
			$this->db->from('proj_bonus_based_purchased_upgrades_tracking bupt');
			$this->db->join('users u', 'bupt.project_owner_id = u.user_id', 'left');
			$this->db->where('bupt.project_upgrade_type', 'urgent');
			$this->db->where('bupt.project_upgrade_end_date >= NOW()');
			$this->db->where('bupt.project_owner_id !=', $user[0]->user_id);
			$urgent_purchase = $this->db->get()->result_array();
			if(!empty($urgent_purchase)) {
				
				foreach($urgent_purchase as $urgent) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_track_bonus_based_purchased_upgrades');
					$this->db->where('project_id', $urgent['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $urgent['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$urgent_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $urgent['project_id']);
					$urgent_project = $this->db->get()->row_array();
					if(!empty($urgent_refresh) && !empty($urgent_project)) {
						if(strtotime($urgent_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
                                
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
                                $index = array_search($urgent_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $urgent_project);
									array_pop($data['open_bidding_project_right_side']);
                                }
                                
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($urgent_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $urgent_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($urgent_refresh['project_next_refresh_time'],$time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($urgent_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
								if(array_key_exists($urgent_project['project_id'], $next_refresh_time)) {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'featured',
										'profile' => $urgent['profile_name']
									];
								} else {
									$next_refresh_time[$urgent_project['project_id']] = [
										'project_title' => $urgent_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'urgent',
										'profile' => $urgent['profile_name']
									];
								}
								$refresh_tracking = [
									'project_id' => $urgent['project_id'],
									'project_upgrade_purchase_tracking_id' => $urgent['id'],
									'project_last_refresh_time' => $urgent_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $insert_id, $urgent['project_id'], $urgent_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$urgent_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time ];
    }
    /**
     * manage sealed project refresh called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_sealed_project_refresh_for_real_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_sealed'));
		$check_valid_arr = array_map('getInt', $time_arr); 
		$valid_time_arr = array_filter($check_valid_arr);
		if(!empty($valid_time_arr)) { // sealed refresh sequence only exectue if sealed project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('upt.*, u.profile_name');
			$this->db->from('projects_upgrades_purchase_tracking upt');
			$this->db->join('users u', 'upt.project_owner_id = u.user_id', 'left');
			$this->db->where('upt.project_upgrade_type', 'sealed');
			$this->db->where('upt.project_upgrade_end_date >= NOW()');
			$this->db->where('upt.project_owner_id !=', $user[0]->user_id);
			$sealed_purchase = $this->db->get()->result_array();

			if(!empty($sealed_purchase)) {
				
				foreach($sealed_purchase as $sealed) {
					$this->db->select('*');
					$this->db->from('projects_refresh_sequence_tracking');
					$this->db->where('project_id', $sealed['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $sealed['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$sealed_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $sealed['project_id']);
					$sealed_project = $this->db->get()->row_array();
					if(!empty($sealed_refresh) && !empty($sealed_project)) {
						if(strtotime($sealed_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($sealed_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $sealed_project);
									array_pop($data['open_bidding_project_right_side']);
								}
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($sealed_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $sealed_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($sealed_refresh['project_next_refresh_time'], $time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($sealed_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
								if(!array_key_exists($sealed_project['project_id'], $next_refresh_time)) {
									$next_refresh_time[$sealed_project['project_id']] = [
										'project_title' => $sealed_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'sealed',
										'profile' => $sealed['profile_name'] 
									];
								}

								$refresh_tracking = [
									'project_id' => $sealed['project_id'],
									'project_upgrade_purchase_tracking_id' => $sealed['id'],
									'project_last_refresh_time' => $sealed_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('projects_refresh_sequence_tracking', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('projects_refresh_sequence_tracking', $insert_id, $sealed['project_id'], $sealed_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$sealed_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time];
    }
    /**
     * manage sealed project refresh membership included payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_sealed_project_refresh_for_membership_included_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_sealed'));
		$check_valid_arr = array_map('getInt', $time_arr); 
		$valid_time_arr = array_filter($check_valid_arr);
		if(!empty($valid_time_arr)) { // sealed refresh sequence only exectue if sealed project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('mupt.*, u.profile_name');
			$this->db->from('proj_membership_included_upgrades_purchase_tracking mupt');
			$this->db->join('users u', 'mupt.project_owner_id = u.user_id', 'left');
			$this->db->where('mupt.project_upgrade_type', 'sealed');
			$this->db->where('mupt.project_upgrade_end_date >= NOW()');
			$this->db->where('mupt.project_owner_id !=', $user[0]->user_id);
			$sealed_purchase = $this->db->get()->result_array();

			if(!empty($sealed_purchase)) {
				
				foreach($sealed_purchase as $sealed) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_tracking_membership_included_upgrades');
					$this->db->where('project_id', $sealed['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $sealed['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$sealed_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $sealed['project_id']);
					$sealed_project = $this->db->get()->row_array();
					if(!empty($sealed_refresh) && !empty($sealed_project)) {
						if(strtotime($sealed_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($sealed_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $sealed_project);
									array_pop($data['open_bidding_project_right_side']);
								}
								
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($sealed_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $sealed_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($sealed_refresh['project_next_refresh_time'], $time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($sealed_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
									$next_refresh_time[$sealed_project['project_id']] = [
										'project_title' => $sealed_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'sealed',
										'profile' => $sealed['profile_name']
									];

								$refresh_tracking = [
									'project_id' => $sealed['project_id'],
									'project_upgrade_purchase_tracking_id' => $sealed['id'],
									'project_last_refresh_time' => $sealed_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_tracking_membership_included_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_tracking_membership_included_upgrades', $insert_id, $sealed['project_id'], $sealed_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$sealed_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time];
    }
    /**
     * manage sealed project refresh bonus based payment called from projects/ajax_update_latest_project_dashboard_view
     */
    public function manage_sealed_project_refresh_for_bonus_based_payment($data, $next_refresh_time, $user) {
        $time_arr = explode(":", $this->config->item('project_upgrade_refresh_sequence_sealed'));
		$check_valid_arr = array_map('getInt', $time_arr); 
		$valid_time_arr = array_filter($check_valid_arr);
		if(!empty($valid_time_arr)) { // sealed refresh sequence only exectue if sealed project refresh sequence time not set to disabled like "00:00:00"
			$this->db->select('bupt.*, u.profile_name');
			$this->db->from('proj_bonus_based_purchased_upgrades_tracking bupt');
			$this->db->join('users u', 'bupt.project_owner_id = u.user_id', 'left');
			$this->db->where('bupt.project_upgrade_type', 'sealed');
			$this->db->where('bupt.project_upgrade_end_date >= NOW()');
			$this->db->where('bupt.project_owner_id !=', $user[0]->user_id);
			$sealed_purchase = $this->db->get()->result_array();

			if(!empty($sealed_purchase)) {
				
				foreach($sealed_purchase as $sealed) {
					$this->db->select('*');
					$this->db->from('proj_refresh_sequence_track_bonus_based_purchased_upgrades');
					$this->db->where('project_id', $sealed['project_id']);
					$this->db->where('project_next_refresh_time is NOT NULL');
					$this->db->where('project_upgrade_purchase_tracking_id', $sealed['id']);
					$this->db->order_by('id', 'desc');
					$this->db->limit(1);
					$sealed_refresh = $this->db->get()->row_array();
					// get open bidding project details by project id
					$this->db->select('op.project_id,op.project_owner_id,op.project_title,op.project_description,op.project_type,op.min_budget,op.max_budget,op.featured,op.urgent,op.sealed,op.hidden,op.project_posting_date,op.project_expiration_date,op.escrow_payment_method,counties.name as county_name,localities.name as locality_name,postal_codes.postal_code');
					$this->db->from('projects_open_bidding op');
					$this->db->join('counties', 'counties.id = op.county_id', 'left');
					$this->db->join('localities', 'localities.id = op.locality_id', 'left');
					$this->db->join('postal_codes', 'postal_codes.id = op.postal_code_id', 'left');
					$this->db->where('op.project_expiration_date >= NOW()');
					$this->db->order_by('op.id','desc');
					$this->db->where("op.project_id", $sealed['project_id']);
					$sealed_project = $this->db->get()->row_array();
					if(!empty($sealed_refresh) && !empty($sealed_project)) {
						if(strtotime($sealed_refresh['project_next_refresh_time']) <= strtotime(date('Y-m-d H:i:s'))) {
							
								$right_side = array_column($data['open_bidding_project_right_side'], 'project_id');
								$index = array_search($sealed_refresh['project_id'], $right_side);
								if(isset($index) && array_key_exists($index, $data['open_bidding_project_right_side'])) {
									$tmp = $data['open_bidding_project_right_side'][$index];
									unset($data['open_bidding_project_right_side'][$index]);
									array_unshift($data['open_bidding_project_right_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_right_side'], $sealed_project);
									array_pop($data['open_bidding_project_right_side']);
								}
																
								$left_side = array_column($data['open_bidding_project_left_side'], 'project_id');
								$left_index = array_search($sealed_refresh['project_id'], $left_side);
								if(isset($left_index) && array_key_exists($left_index, $data['open_bidding_project_left_side'])) {
									$tmp = $data['open_bidding_project_left_side'][$left_index];
									unset($data['open_bidding_project_left_side'][$left_index]);
									array_unshift($data['open_bidding_project_left_side'], $tmp);
								} else {
									array_unshift($data['open_bidding_project_left_side'], $sealed_project);
									array_pop($data['open_bidding_project_left_side']);
								}
								// insert into project refresh sequence tracking table
								$next_refresh_date = get_next_refresh_time($sealed_refresh['project_next_refresh_time'], $time_arr);
								// $next_refresh_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds', strtotime($sealed_refresh['project_next_refresh_time'])));
								if(strtotime($next_refresh_date) > strtotime($urgent_project['project_expiration_date'])) {
									$next_refresh_date = null;
								} 
									$next_refresh_time[$sealed_project['project_id']] = [
										'project_title' => $sealed_project['project_title'],
										'next_refresh_time' => $next_refresh_date,
										'type' => 'sealed',
										'profile' => $sealed['profile_name']
									];

								$refresh_tracking = [
									'project_id' => $sealed['project_id'],
									'project_upgrade_purchase_tracking_id' => $sealed['id'],
									'project_last_refresh_time' => $sealed_refresh['project_next_refresh_time'],
									'project_next_refresh_time' => $next_refresh_date
								];
								$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_tracking);
								$insert_id = $this->db->insert_id();
								$result = $this->get_immediate_next_refresh_time_for_refreshed_project('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $insert_id, $sealed['project_id'], $sealed_refresh['project_next_refresh_time']);
								if(!empty($result)) {
									$next_refresh_time[$sealed_project['project_id']]['next_refresh_time'] = $result['project_next_refresh_time'];
								}
								
						}
					}
				}
			}	
        }
        return ['data' => $data, 'next_refresh_time' => $next_refresh_time];

    }

	/**
	*	This function is used to save user upgrade purchase tracking  and refrence sequences and deduct the bonus balance and account balance .
	*/
	public function user_project_upgrade_purchase_refresh_sequence_tracking_membership_exclude_save($project_data,$user_id){
	
		$user_selected_upgrades = array();
		$upgrade_counter = 0;
		if(!empty($project_data['featured']) && $project_data['featured'] == 'Y' ){
			//$upgrade_type_featured = 'Y';
			$user_selected_upgrades[$upgrade_counter][] = 'featured';
			$user_selected_upgrades[$upgrade_counter][] = $project_data['featured_upgrade_end_date'];
			$upgrade_counter++;
		}
		if(!empty($project_data['urgent']) && $project_data['urgent'] == 'Y'){
			//$upgrade_type_urgent = 'Y';
			//$user_selected_upgrades[] = 'urgent';
			$user_selected_upgrades[$upgrade_counter][] = 'urgent';
			$user_selected_upgrades[$upgrade_counter][] = $project_data['urgent_upgrade_end_date'];
			$upgrade_counter++;
			
		}
		if(!empty($project_data['sealed']) && $project_data['sealed'] == 'Y'){
			//$upgrade_type_sealed = 'Y';
			$user_selected_upgrades[$upgrade_counter][] = 'sealed';
			$upgrade_counter++;
		}
		if(!empty($project_data['hidden']) && $project_data['hidden'] == 'Y'){
			//$upgrade_type_hidden = 'Y';
			$user_selected_upgrades[$upgrade_counter][] = 'hidden';
			$upgrade_counter++;
		}
		
		$project_id = $project_data['project_id'];
		
	
		if(!empty($user_selected_upgrades)){
			foreach( $user_selected_upgrades as $value){
			
				$user_detail = $this->db->get_where('user_details', ['user_id' => $user_id])->row_array();
				$user_membership_plan_detail = $this->db->get_where('membership_plans', ['id' => $user_detail['current_membership_plan_id']])->row_array();
				
				
				$project_upgrade_type =  $value[0];
				$project_upgrade_price =  $this->config->item('project_upgrade_price_'.$value[0]);
				$project_upgrade_availability =  $this->config->item('project_upgrade_availability_'.$value[0]);
				$project_upgrade_refresh_sequence =  $this->config->item('project_upgrade_refresh_sequence_'.$value[0]);
				$project_upgrade_end =  $this->config->item('project_upgrade_availability_'.$value[0]);
				if(empty($value[1])){
				
					$time_arr = explode(':', $project_upgrade_end);
					$value[1] = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
				}
				
				
				$upgrades_purchase_data = [
					'project_id' => $project_id,
					'project_owner_id' => $user_id,
					'project_owner_membership_plan_id' => $user_detail['current_membership_plan_id'],
					'project_upgrade_purchase_date' => date('Y-m-d H:i:s'),
					'project_upgrade_type' => $project_upgrade_type,
					'project_upgrade_availability_length' => $project_upgrade_availability,
					'project_upgrade_start_date' => date('Y-m-d H:i:s'),
					'project_upgrade_end_date' => $value[1]
				];
				
				
				$time_arr = explode(':',$project_upgrade_refresh_sequence);
				// insert into projects_refresh_sequence_tracking table
				
				
				$refresh_sequence_data = [
					'project_id' => $project_id,
					'project_last_refresh_time' => null,
					'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
				];
				
					
				$remaining_upgrade_amount_after_deduction = 0;
				$deduction_amount = 0;
				$remaining_signup_bonus_balance =0;
				$remaining_bonus_balance =0;
				$remaining_user_account_balance =0;
				
				if( $user_detail['signup_bonus_balance'] >= $project_upgrade_price ){
					$deduction_amount = $project_upgrade_price;
					$remaining_signup_bonus_balance =  $user_detail['signup_bonus_balance'] - $deduction_amount;
				} else{
					$remaining_upgrade_amount_after_deduction = $project_upgrade_price - $user_detail['signup_bonus_balance'];
					$deduction_amount = $user_detail['signup_bonus_balance'];
					$remaining_signup_bonus_balance = 0;
				}
				$upgrades_purchase_data['project_upgrade_purchase_value'] = $deduction_amount;
				if(floatval($user_detail['signup_bonus_balance']) != 0){
					$this->db->insert('proj_bonus_based_purchased_upgrades_tracking', $upgrades_purchase_data);
					$last_insert_id = $this->db->insert_id();
					$refresh_sequence_data['project_upgrade_purchase_tracking_id'] =  $last_insert_id;
					if($value[0] != 'hidden'){
						$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_sequence_data);
					}
					$this->db->update('user_details', ['signup_bonus_balance' => $remaining_signup_bonus_balance], ['id' => $user_detail['id']]);
				}
				if($remaining_upgrade_amount_after_deduction != 0  ){
					if( $user_detail['bonus_balance'] >= $remaining_upgrade_amount_after_deduction ){
						$deduction_amount = $remaining_upgrade_amount_after_deduction;
						$remaining_bonus_balance = $user_detail['bonus_balance'] -$remaining_upgrade_amount_after_deduction;
						$remaining_upgrade_amount_after_deduction = 0;
					} else{
						$deduction_amount =  $user_detail['bonus_balance'];
						$remaining_upgrade_amount_after_deduction = $remaining_upgrade_amount_after_deduction - $user_detail['bonus_balance'];
						$remaining_bonus_balance = 0;
					}
					if(floatval($user_detail['bonus_balance']) != 0){
						$upgrades_purchase_data['project_upgrade_purchase_value'] = $deduction_amount;
						$this->db->insert('proj_bonus_based_purchased_upgrades_tracking', $upgrades_purchase_data);
						$last_insert_id = $this->db->insert_id();
						$refresh_sequence_data['project_upgrade_purchase_tracking_id'] =  $last_insert_id;
						if($value[0] != 'hidden'){
							$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_sequence_data);
						}
						
						$this->db->update('user_details', ['bonus_balance' => $remaining_bonus_balance], ['id' => $user_detail['id']]);
					}	
					if($remaining_upgrade_amount_after_deduction != 0){
						if( $user_detail['user_account_balance'] >= $remaining_upgrade_amount_after_deduction ){
							$deduction_amount = $remaining_upgrade_amount_after_deduction;
							$remaining_user_account_balance = $user_detail['user_account_balance'] - $deduction_amount;
							
						} 
						$upgrades_purchase_data['project_upgrade_purchase_value'] = $deduction_amount;
						$this->db->insert('projects_upgrades_purchase_tracking', $upgrades_purchase_data);
						$last_insert_id = $this->db->insert_id();
						$refresh_sequence_data['project_upgrade_purchase_tracking_id'] =  $last_insert_id;
						if($value[0] != 'hidden'){
							$this->db->insert('projects_refresh_sequence_tracking', $refresh_sequence_data);
						}
						$amount = $user_detail['signup_bonus_balance'] - $project_upgrade_price;
						$this->db->update('user_details', ['user_account_balance' => $remaining_user_account_balance], ['id' => $user_detail['id']]);
					}
				}
			}	
		}
	}

	/**
	 * This method is used to update memebership included purchase tracking, bonus included purchase tracking, upgrade purchase tracking tables project_upgrade_start_date and project_upgrade_end_date column
	 * also make entry into there respected refresh sequence table
	 */
	public function update_upgrade_purchase_tracking_with_related_refresh_sequence_tracking($project_id) {
		// Get membership included upgrade purchase tracking data
		$membership_included_upgrade_data = $this->db->get_where('proj_membership_included_upgrades_purchase_tracking', ['project_id' => $project_id ])->result_array();
		if(!empty($membership_included_upgrade_data)) {
			foreach($membership_included_upgrade_data as $val) {
				$time_arr = explode(':', $val['project_upgrade_availability_length']);
				$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
				$upgrades_purchase_data = [
					'project_upgrade_start_date' => date('Y-m-d H:i:s'),
					'project_upgrade_end_date' => $upgrade_end_date
				];
				$this->db->update('proj_membership_included_upgrades_purchase_tracking', $upgrades_purchase_data, ['id'=> $val['id']]);
				if($val['project_upgrade_type'] == 'featured') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_featured'));
				} else if ($val['project_upgrade_type'] == 'urgent') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_urgent'));
				} else if ($val['project_upgrade_type'] == 'sealed') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_sealed'));
				} else {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_hidden'));
				}
				// insert into refresh sequence tracking membership included upgrades
				$refresh_sequence_data = [
					'project_id' => $project_id,
					'project_upgrade_purchase_tracking_id' => $val['id'],
					'project_last_refresh_time' => null,
					'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
				];
				$this->db->insert('proj_refresh_sequence_tracking_membership_included_upgrades', $refresh_sequence_data);
			}
		}
		// get bonus based upgrade purchased tracking data
		$bonus_based_upgrade_data = $this->db->get_where('proj_bonus_based_purchased_upgrades_tracking', ['project_id' => $project_id])->result_array();
		if(!empty($bonus_based_upgrade_data)) {
			foreach($bonus_based_upgrade_data as $val) {
				$time_arr = explode(':', $val['project_upgrade_availability_length']);
				$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
				$upgrades_purchase_data = [
					'project_upgrade_start_date' => date('Y-m-d H:i:s'),
					'project_upgrade_end_date' => $upgrade_end_date
				];
				$this->db->update('proj_bonus_based_purchased_upgrades_tracking', $upgrades_purchase_data, ['id'=> $val['id']]);
				if($val['project_upgrade_type'] == 'featured') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_featured'));
				} else if ($val['project_upgrade_type'] == 'urgent') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_urgent'));
				} else if ($val['project_upgrade_type'] == 'sealed') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_sealed'));
				} else {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_hidden'));
				}
				// insert into refresh sequence tracking membership included upgrades
				$refresh_sequence_data = [
					'project_id' => $project_id,
					'project_upgrade_purchase_tracking_id' => $val['id'],
					'project_last_refresh_time' => null,
					'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
				];
				$this->db->insert('proj_refresh_sequence_track_bonus_based_purchased_upgrades', $refresh_sequence_data);
			}
		}
		// get real payment upgrade data
		$upgrade_purchase_data = $this->db->get_where('projects_upgrades_purchase_tracking', ['project_id' => $project_id])->result_array();
		if(!empty($upgrade_purchase_data)) {
			foreach($upgrade_purchase_data as $val) {
				$time_arr = explode(':', $val['project_upgrade_availability_length']);
				$upgrade_end_date = date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'));
				$upgrades_purchase_data = [
					'project_upgrade_start_date' => date('Y-m-d H:i:s'),
					'project_upgrade_end_date' => $upgrade_end_date
				];
				$this->db->update('projects_upgrades_purchase_tracking', $upgrades_purchase_data, ['id'=> $val['id']]);
				if($val['project_upgrade_type'] == 'featured') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_featured'));
				} else if ($val['project_upgrade_type'] == 'urgent') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_urgent'));
				} else if ($val['project_upgrade_type'] == 'sealed') {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_sealed'));
				} else {
					$time_arr = explode(':', $this->config->item('project_upgrade_refresh_sequence_hidden'));
				}
				// insert into refresh sequence tracking membership included upgrades
				$refresh_sequence_data = [
					'project_id' => $project_id,
					'project_upgrade_purchase_tracking_id' => $val['id'],
					'project_last_refresh_time' => null,
					'project_next_refresh_time' => date('Y-m-d H:i:s', strtotime('+'.(int)$time_arr[0].' hour +'.(int)$time_arr[1].' minutes +'.(int)$time_arr[2].' seconds'))
				];
				$this->db->insert('projects_refresh_sequence_tracking', $refresh_sequence_data);
			}
		}
	}
	/**
	 * This method is used to update upgrade column [featured / urgent] status in open bidding table 
	*/
	public function update_expired_upgrade_status_open_bidding() {
		$this->db->select('op.project_id,op.featured,op.urgent,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,urgent_purchasing_tracking.urgent_upgrade_end_date,bonus_urgent_purchasing_tracking.bonus_urgent_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,membership_include_urgent_purchasing_tracking.membership_include_urgent_upgrade_end_date');
		$this->db->from('projects_open_bidding op');
		$this->db->where('op.project_expiration_date >= NOW()');
		$this->db->where('op.featured = "Y" OR op.urgent = "Y"');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured"  group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured"  group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_urgent_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "urgent"  group by project_id ) as membership_include_urgent_purchasing_tracking', 'membership_include_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bonus_urgent_upgrade_end_date from '.$this->db->dbprefix.'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "urgent"  group by project_id ) as bonus_urgent_purchasing_tracking', 'bonus_urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->join('(select project_id, max(project_upgrade_end_date) as urgent_upgrade_end_date from '.$this->db->dbprefix.'projects_upgrades_purchase_tracking where project_upgrade_type = "urgent"  group by project_id ) as urgent_purchasing_tracking', 'urgent_purchasing_tracking.project_id = op.project_id', 'left');
		$this->db->order_by('op.id','desc');
		$open_bidding_project_result = $this->db->get();
		$open_bidding_project_data = $open_bidding_project_result->result_array();
		if(!empty($open_bidding_project_data)) {
			
			foreach($open_bidding_project_data as $open_bidding_project_key => $open_bidding_project_value){
				$featured_max = 0;
				$urgent_max = 0;
				$expiration_featured_upgrade_date_array = array();
				$expiration_urgent_upgrade_date_array = array();
				
				if(!empty($open_bidding_project_value['featured_upgrade_end_date'])){
					$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['featured_upgrade_end_date'];
				}
				if(!empty($open_bidding_project_value['bounus_featured_upgrade_end_date'])){
					$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['bounus_featured_upgrade_end_date'];
				}
				if(!empty($open_bidding_project_value['membership_include_featured_upgrade_end_date'])){
					$expiration_featured_upgrade_date_array[] = $open_bidding_project_value['membership_include_featured_upgrade_end_date'];
				}
				if(!empty($expiration_featured_upgrade_date_array)){
					$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
				}
				
				##########
				if(!empty($open_bidding_project_value['urgent_upgrade_end_date'])){
					$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['urgent_upgrade_end_date'];
				}
				if(!empty($open_bidding_project_value['bonus_urgent_upgrade_end_date'])){
					$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['bonus_urgent_upgrade_end_date'];
				}
				if(!empty($open_bidding_project_value['membership_include_urgent_upgrade_end_date'])){
					$expiration_urgent_upgrade_date_array[] = $open_bidding_project_value['membership_include_urgent_upgrade_end_date'];
				}
				if(!empty($expiration_urgent_upgrade_date_array)){
					$urgent_max = max(array_map('strtotime', $expiration_urgent_upgrade_date_array));
				}
				$updated_open_bidding_data = [];
				if($featured_max != 0 && $featured_max < time()) {
					$updated_open_bidding_data['featured'] = 'N';
				}
				if($urgent_max != 0 && $urgent_max < time()) {
					$updated_open_bidding_data['urgent'] = 'N';
				}
				if(!empty($updated_open_bidding_data)) {
					$this->db->update('projects_open_bidding', $updated_open_bidding_data, ['project_id' => $open_bidding_project_value['project_id']]);
					$standard_proj_flag = false;
					if($open_bidding_project_value['featured'] == 'Y' && $open_bidding_project_value['urgent'] == 'Y') {
						if(!empty($updated_open_bidding_data['featured']) && !empty($updated_open_bidding_data['urgent'])) {
							$standard_proj_flag = true;
						}
					} else if ($open_bidding_project_value['featured'] == 'Y' && !empty($updated_open_bidding_data['featured'])) {
						$standard_proj_flag = true;
					} else if ($open_bidding_project_value['urgent'] == 'Y' && !empty($updated_open_bidding_data['urgent'])) {
						$standard_proj_flag = true;
					}
					if($standard_proj_flag) {
						$refresh_tracking = [
							'project_id' => $open_bidding_project_value['project_id'],
							'project_upgrade_purchase_tracking_id' => null,
							'project_last_refresh_time' => null,
							'project_next_refresh_time' => date('Y-m-d H:i:s')
						];
						$this->db->insert('projects_refresh_sequence_tracking', $refresh_tracking);
					}
				}
			}
		}
	}

	public function get_featured_project_upgrade_expiration_status($project_id)
	{
		
		$this->db->select('projects_open_bidding.project_id,projects_open_bidding.project_owner_id,projects_open_bidding.project_expiration_date,projects_open_bidding.featured,featured_purchasing_tracking.featured_upgrade_end_date,bonus_featured_purchasing_tracking.bounus_featured_upgrade_end_date,membership_include_featured_purchasing_tracking.membership_include_featured_upgrade_end_date,users.profile_name');
		$this->db->from('projects_open_bidding');
		$this->db->join('users', 'users.user_id = projects_open_bidding.project_owner_id', 'left');
		
		$this->db->join('(select project_id, max(project_upgrade_end_date) as membership_include_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_membership_included_upgrades_purchase_tracking where project_upgrade_type = "featured"  and project_id = "'.$project_id.'" group by project_id ) as membership_include_featured_purchasing_tracking', 'membership_include_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
	
		$this->db->join('(select project_id, max(project_upgrade_end_date) as bounus_featured_upgrade_end_date from '.$this->db->dbprefix .'proj_bonus_based_purchased_upgrades_tracking where project_upgrade_type = "featured" and project_id = "'.$project_id.'" group by project_id ) as bonus_featured_purchasing_tracking', 'bonus_featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
		
		$this->db->join('(select project_id, max(project_upgrade_end_date) as featured_upgrade_end_date from '.$this->db->dbprefix .'projects_upgrades_purchase_tracking where project_upgrade_type = "featured"  and project_id = "'.$project_id.'" group by project_id ) as featured_purchasing_tracking', 'featured_purchasing_tracking.project_id = projects_open_bidding.project_id', 'left');
		$this->db->where('projects_open_bidding.project_id',$project_id);
		
		
		$project_result = $this->db->get();
		$project_data = $project_result->result_array();
		 $featured_max = 0;
		if(!empty($project_data[0]['featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $project_data[0]['featured_upgrade_end_date'];
		}
		if(!empty($project_data[0]['bounus_featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $project_data[0]['bounus_featured_upgrade_end_date'];
		}
		if(!empty($project_data[0]['membership_include_featured_upgrade_end_date'])){
			$expiration_featured_upgrade_date_array[] = $project_data[0]['membership_include_featured_upgrade_end_date'];
		}
		if(!empty($expiration_featured_upgrade_date_array)){
			$featured_max = max(array_map('strtotime', $expiration_featured_upgrade_date_array));
		}
		$profile_folder     = $project_data[0]['profile_name'];
		
		if($project_data[0]['featured'] == 'Y' && $featured_max >= time()){
			$project_featured_upgrade_expiration_status = true;
			}else{
			$this->delete_featured_project_upgrade_record_cover_picture($profile_folder,$project_id);
			$project_featured_upgrade_expiration_status = false;
		}
		
		
		################# remove the obsolete entry start ###########
		$count_featured_project_cover_picture = $this->db // count the project featured cover picture record
				->select ('id')
				->from ('featured_projects_users_upload_cover_pictures_tracking')
				->where('project_id',$project_id)
				->get ()->num_rows ();
		if($count_featured_project_cover_picture == 0){
			$this->delete_featured_project_upgrade_record_cover_picture($profile_folder,$project_id);
		}else{
		
			$project_featured_cover_picture_detail = $this->db->get_where('featured_projects_users_upload_cover_pictures_tracking', ['project_id' => $project_id])->row_array();
			if(!empty($project_featured_cover_picture_detail['project_cover_picture_name'])){
			
				$this->load->library('ftp');
				$config['ftp_hostname'] = $this->config->item('ftp_hostname');
				$config['ftp_username'] = $this->config->item('ftp_username');
				$config['ftp_password'] = $this->config->item('ftp_password');
				$config['ftp_port'] 	= $this->config->item('ftp_port');
				$config['debug']    = TRUE;
				$this->ftp->connect($config);
			
				$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
				$projects_ftp_dir = $this->config->item('projects_ftp_dir');
				$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
				$featured_upgrade_cover_picture = $this->config->item('featured_upgrade_cover_picture');
				$profile_folder     = $project_data[0]['profile_name'];
				$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$featured_upgrade_cover_picture.$project_featured_cover_picture_detail['project_cover_picture_name'];
				$file_size = $this->ftp->get_filesize($source_path);
				if($file_size == '-1')
				{
					
					$this->delete_featured_project_upgrade_record_cover_picture($profile_folder,$project_id);
				}else{
					$this->ftp->close();
				}
			}
		}
		################# remove the obsolete entry end ###########
		return $project_featured_upgrade_expiration_status; 
	}
	
	/**
	 * This method is used to delete the featured upgrade project cover picture from folder as well as from database  
	 */
	public function delete_featured_project_upgrade_record_cover_picture($profile_folder,$project_id){
	
		$this->db->delete('featured_projects_users_upload_cover_pictures_tracking', array('project_id' => $project_id));
		$this->load->library('ftp');
		$config['ftp_hostname'] = $this->config->item('ftp_hostname');
		$config['ftp_username'] = $this->config->item('ftp_username');
		$config['ftp_password'] = $this->config->item('ftp_password');
		$config['ftp_port'] 	= $this->config->item('ftp_port');
		$config['debug']    = TRUE;
		$this->ftp->connect($config); 
		######## connectivity of remote server end #######
		$users_ftp_dir 	= $this->config->item('users_ftp_dir'); 
		$projects_ftp_dir = $this->config->item('projects_ftp_dir');
		$project_open_for_bidding_dir = $this->config->item('project_open_for_bidding_dir');
		$featured_upgrade_cover_picture = $this->config->item('featured_upgrade_cover_picture');
		$source_path = $users_ftp_dir.$profile_folder.$projects_ftp_dir.$project_open_for_bidding_dir.$project_id.$featured_upgrade_cover_picture;
		if(!empty($this->ftp->check_ftp_directory_exist($source_path)))
		{
			$this->ftp->delete_dir($source_path);// delete cover picture directory 
		}
		$this->ftp->close();
	
	}
	
	/**
	* This method is used to check valid combination of locality_id,county_id,postal_code_id If the combination is not valid it will update locality_id,county_id,postal_code_id  to 0.
	 */
	public function check_update_invalid_combination_project_location($table_name,$field_name,$project_id){
		
		$check_valid_location = true;
		$this->db->select('locality_id,county_id,postal_code_id');
		$this->db->from($table_name);
		$this->db->where($field_name,$project_id);
		$project_result = $this->db->get();
		$project_data = $project_result->result_array();
		
		
		$locality_id = $project_data[0]['locality_id']!= NULL ? $project_data[0]['locality_id'] : 0;
		$county_id = $project_data[0]['county_id']!= NULL ? $project_data[0]['county_id'] : 0;
		$postal_code_id = $project_data[0]['postal_code_id']!= NULL ? $project_data[0]['postal_code_id'] : 0;
		$location_addition = $locality_id + $county_id + $postal_code_id;
		if(!empty($location_addition)){
			$county_detail = $this->db->get_where('counties', ['id' => $project_data[0]['county_id']])->row_array();
			if(!empty($county_detail)){
				$locality_detail = $this->db->get_where('localities', ['id'=>$project_data[0]['locality_id'],'county_id'=>$project_data[0]['county_id']])->row_array();
				if(!empty($locality_detail)){
						
					$postal_code_detail = $this->db->get_where('postal_codes', ['id'=>$project_data[0]['postal_code_id'],'locality_id'=>$project_data[0]['locality_id']])->row_array();
					if(empty($postal_code_detail)){
						$check_valid_location = false;
					}
				}else{
					$check_valid_location = false;
				}
			}else{
				$check_valid_location = false;
			}
			if(!$check_valid_location){
			$this->db->update($table_name, ['locality_id' => 0,'county_id' => 0,'postal_code_id' => 0], [$field_name => $project_id]);
			}
		}
	}
	
}